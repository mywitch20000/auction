package com.doollee.auction.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.doollee.auction.R;
import com.doollee.auction.common.PreferenceManager;
import com.doollee.auction.utils.JsonUtils;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 *
 * Class: ScheduleAdapter
 * Created by lim on 2020.12.04
 *
 * Description: 경매일정 Adapter
 */

public class ScheduleAdapter extends RecyclerView.Adapter<ScheduleAdapter.ScheduleViewHolder> {
    private Context                 context         ;
    private ArrayList               listScheduleInfo;
    private int                     selectPos  = 0  ;
    private onListener              listener        ;

    private int                     initPos    = -1 ; //초기선택행 설정시 사용

    public ScheduleAdapter(Context context, ArrayList listInfo, int inInitPos, onListener listener) {
        this.context          = context ;
        this.listScheduleInfo = listInfo;
        this.listener         = listener;
        if (inInitPos > 0) {
            this.selectPos = inInitPos;
        }
    }

    public class ScheduleViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public LinearLayout    layoutScheduleItem      ; // 경매일정 RecyclerView Layout ID
        public RelativeLayout  layoutBorderLayout  ; // 외곽선 layout
        public TextView        textAuctionNumber   ; // 경매번호
        public TextView        textProducer        ; // 출하처
        public TextView        textWrsNm           ; // 상품명
        public TextView        textDetailWrsInfo   ; // 상품정보

        public TextView        textAuctionState    ; // 경매진행상태 ( 대기, 진행 등 )
        public TextView        textAuctioneer      ; // 경매사
        public TextView        textAuctionTime     ; // 경매시간
        public CheckBox        ckboxSelectRowLayout; // 레이아웃선택표시 체크박스
        public ImageView       imgFaveCheck        ; // 즐겨찾기항목표시 이미지

        public ScheduleViewHolder(View view) {
            super(view);

            layoutScheduleItem = (LinearLayout) view.findViewById(R.id.layout_schedule_item);
            layoutScheduleItem.setOnClickListener(this);
            layoutBorderLayout = (RelativeLayout) view.findViewById(R.id.border_schedule_item);
            textAuctionNumber = (TextView) view.findViewById(R.id.text_auction_number);
            textProducer = (TextView) view.findViewById(R.id.text_producer);
            textWrsNm = (TextView) view.findViewById(R.id.text_wrs_nm);
            textDetailWrsInfo = (TextView) view.findViewById(R.id.text_detail_wrs_info);
            textAuctionState = (TextView) view.findViewById(R.id.text_auction_state);
            textAuctioneer = (TextView) view.findViewById(R.id.text_auctioneer);
            textAuctionTime = (TextView) view.findViewById(R.id.text_auction_time);
            ckboxSelectRowLayout = (CheckBox) view.findViewById(R.id.checkbox_schedule_item);
            imgFaveCheck = (ImageView)view.findViewById(R.id.image_check_favo);
        }

        @Override
        public void onClick(View view) {
            int position = (int) view.getTag();

            if (view == layoutScheduleItem) {
                selectPos = position;

                notifyDataSetChanged();

                if (listener != null)
                    listener.onClickListener(position);
            }
        }
    }

    @Override
    public ScheduleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_schedule_item, parent, false);

        ScheduleViewHolder vh = new ScheduleViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(ScheduleViewHolder viewHolder, final int position) {
        viewHolder.layoutScheduleItem.setTag(position);

        Map dataInfo = (Map) listScheduleInfo.get(position);
        if (dataInfo == null)
            return;

        final String sdfNow  = new SimpleDateFormat("yyyy-MM-dd(EE)" , Locale.getDefault()).format(new Date(System.currentTimeMillis()));

        // 경매번호 화면출력
        final String auction_number = (String) dataInfo.get("AUC_NO");
        if (TextUtils.isEmpty(auction_number) || "null".equals(auction_number)) {
            viewHolder.textAuctionNumber.setText("");
        } else {
            viewHolder.textAuctionNumber.setText(auction_number);
        }

        // 출하처(생산자) 화면출력
        String producer = (String) dataInfo.get("SENDER");
        if (TextUtils.isEmpty(producer) || "null".equals(producer)) {
            viewHolder.textProducer.setText("");
        } else {
            viewHolder.textProducer.setText(producer);
        }

        // 상품명 화면출력
        String wrs_nm = (String) dataInfo.get("WRS_NM");
        if (TextUtils.isEmpty(wrs_nm) || "null".equals(wrs_nm)) {
            viewHolder.textWrsNm.setText("");
        } else {
            viewHolder.textWrsNm.setText(wrs_nm);
        }

        // 상품정보 화면출력
        String detail_wrs_info =
                  (String) dataInfo.get("WEIGHT") + (String) dataInfo.get("UNIT") + " "
                + (String) dataInfo.get("GRADE" ) + " "
                + (String) dataInfo.get("SIZE"  ) + " "
                + (String) dataInfo.get("QT"    ) + "개"
                ;
        if (TextUtils.isEmpty(detail_wrs_info)) {
            viewHolder.textDetailWrsInfo.setText("");
        } else {
            viewHolder.textDetailWrsInfo.setText(detail_wrs_info);
        }

        // 경매진행상태 화면출력
        String auction_state = (String) dataInfo.get("AUC_ST");
        if (!TextUtils.isEmpty(auction_state)) {
            viewHolder.textAuctionState.setText(auction_state);
            if ("진행".equals(auction_state)) {
                viewHolder.textAuctionState.setTextColor(context.getResources().getColor(R.color.colorFE2E2E));
                viewHolder.layoutBorderLayout.setBackgroundColor(context.getResources().getColor(R.color.colorFE2E2E));
            } else if ("대기".equals(auction_state)) {
                //viewHolder.textAuctionState.setTextColor(context.getResources().getColor(R.color.color0C6EEC));
                //viewHolder.layoutBorderLayout.setBackgroundColor(context.getResources().getColor(R.color.color0C6EEC));
                viewHolder.textAuctionState.setTextColor(context.getResources().getColor(R.color.colorFE9A2E));
                viewHolder.layoutBorderLayout.setBackgroundColor(context.getResources().getColor(R.color.colorFE9A2E));
            } else {
                viewHolder.textAuctionState.setTextColor(context.getResources().getColor(R.color.color088A08));
                viewHolder.layoutBorderLayout.setBackgroundColor(context.getResources().getColor(R.color.color088A08));
            }
        } else {
            viewHolder.textAuctionState.setTextColor(context.getResources().getColor(R.color.color088A08));
            viewHolder.layoutBorderLayout.setBackgroundColor(context.getResources().getColor(R.color.color088A08));
        }

        // 경매사 화면출력
        String auctioneer = (String) dataInfo.get("AUCTIONEER");
        if (TextUtils.isEmpty(auctioneer)) {
            viewHolder.textAuctioneer.setText("");
        } else {
            viewHolder.textAuctioneer.setText(auctioneer);
        }

        // 경매시간 화면출력
        String auction_time = (String) dataInfo.get("AUC_TM");
        if (TextUtils.isEmpty(auction_time)) {
            viewHolder.textAuctionTime.setText("");

            if ("불매".equals(auction_state)) {
                //경매시간(종료시간)이 존재하는 내역(종료된내역)만 레이아웃 형태변경
                viewHolder.textAuctionState.setTextColor(context.getResources().getColor(R.color.color333333));
                viewHolder.layoutBorderLayout.setBackgroundColor(context.getResources().getColor(R.color.color333333));
                viewHolder.layoutBorderLayout.setAlpha(0.3f);
            } else {
                viewHolder.layoutBorderLayout.setAlpha(1f);
            }

        } else {
            viewHolder.textAuctionTime.setText(auction_time.substring(0,2 ) +":"+ auction_time.substring(2));

            //경매시간(종료시간)이 존재하는 내역(종료된내역)만 레이아웃 형태변경
            viewHolder.textAuctionState.setTextColor(context.getResources().getColor(R.color.color333333));
            viewHolder.layoutBorderLayout.setBackgroundColor(context.getResources().getColor(R.color.color333333));
            viewHolder.layoutBorderLayout.setAlpha(0.3f);
        }

        //즐겨찾기 이미지 화면적용
        //String favoCheck = "";//(String) dataInfo.get("favoCheck");
        //if (!TextUtils.isEmpty(favoCheck))
        //    viewHolder.imgFaveCheck.setBackground(Drawable.createFromPath("@drawable/btn_star_on_disabled_holo_light"));
        //else

        viewHolder.imgFaveCheck.setImageResource(R.drawable.btn_star_off_disabled_holo_light);
        viewHolder.imgFaveCheck.setContentDescription("none");
        Log.d("", PreferenceManager.getString(context, "ScheduleFavoData"));
        if (!"".equals(PreferenceManager.getString(context, "ScheduleFavoData"))) {
            try {
                Map favoMap = JsonUtils.jsonStringToMap((String) PreferenceManager.getString(context, "ScheduleFavoData"));
                Log.d("", favoMap.toString());
                if( favoMap.containsKey("insDay") ) {
                    if( sdfNow.equals(favoMap.get("insDay")) ) {
                        if( favoMap.containsKey(auction_number) ) {
                            viewHolder.imgFaveCheck.setImageResource(R.drawable.btn_star_on_disabled_holo_light);
                            viewHolder.imgFaveCheck.setContentDescription("checked");
                        }
                        Log.d("", PreferenceManager.getString(context, "ScheduleFavoData"));
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        viewHolder.imgFaveCheck.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( "".equals(PreferenceManager.getString(context, "ScheduleFavoData")) ) {
                    //기 데이터가 없는 경우 새로 생성
                    Map favoMap = new ArrayMap();
                    favoMap.put("insDay", sdfNow);
                    favoMap.put(auction_number, "");
                    PreferenceManager.setString(context, "ScheduleFavoData", JsonUtils.mapToJson(favoMap).toString());
                } else {
                    try {
                        Map favoMap = JsonUtils.jsonStringToMap((String) PreferenceManager.getString(context, "ScheduleFavoData"));
                        boolean gbn = "none".equals( ((ImageView) v).getContentDescription() ) ? false : true ;
                        if (sdfNow.equals(favoMap.get("insDay"))) {
                            //같은 일자로 기 데이터가 있는 경우
                            if(gbn) {
                                favoMap.remove(auction_number);
                            } else {
                                favoMap.put(auction_number, "");
                            }
                        } else {
                            //다른 일자로 기 데이터가 있는 경우
                            favoMap = new ArrayMap();
                            favoMap.put("insDay", sdfNow);
                            favoMap.put(auction_number, "");
                        }
                        PreferenceManager.setString(context, "ScheduleFavoData", JsonUtils.mapToJson(favoMap).toString());
                        ((ImageView) v).setImageResource(gbn ? R.drawable.btn_star_off_disabled_holo_light : R.drawable.btn_star_on_disabled_holo_light);
                        ((ImageView) v).setContentDescription(gbn ? "none" : "checked");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                Log.d("", PreferenceManager.getString(context, "ScheduleFavoData"));
            }
        });

        // 선택 중 행만 체크박스 표시되게 하기
        if (selectPos == position) {
            viewHolder.ckboxSelectRowLayout.setChecked(true);
        } else {
            viewHolder.ckboxSelectRowLayout.setChecked(false);
        }
    }

    /**
     * 데이터 개수 반환
     *
     * @return 데이터 개수
     */
    @Override
    public int getItemCount() {
        return listScheduleInfo.size();
    }

    /**
     * 선택행 초기화
     */
    public void clearClickPos() {
        selectPos = initPos;
        notifyDataSetChanged();
    }

    /**
     * 선택된 첫번째 depth 메뉴 위치 반환
     *
     * @return 선택된 메뉴 위치
     */
    public int getClickPos() {
        return selectPos;
    }

    /**
     * recyclerView 이벤트 listener
     */
    public interface onListener {
        void onClickListener(int position);
    }
}
