package com.doollee.auction.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.doollee.auction.R;
import com.doollee.auction.datatype.CategoryInfo;

import java.util.ArrayList;

/**
 *
 * MainClassAdapter
 * Created by wizard on 2021.02.04
 *
 * Description: 부류 Adapter
 */

public class MainCategoryAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<CategoryInfo> listMainCategoryInfo;

    public MainCategoryAdapter(Context context, ArrayList<CategoryInfo> listMainCategoryInfo) {
        this.context = context;
        this.listMainCategoryInfo = listMainCategoryInfo;
    }

    @Override
    public int getCount() {
        return listMainCategoryInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return listMainCategoryInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_main_class_item, parent, false);

            viewHolder.nameItem = (TextView) convertView.findViewById(R.id.text_main_class_name);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        CategoryInfo categoryInfo = listMainCategoryInfo.get(position);
        String mainClassName = categoryInfo.getMainCategoryName();
        if (!TextUtils.isEmpty(mainClassName))
            viewHolder.nameItem.setText(mainClassName);

        return convertView;
    }

    static  class ViewHolder {
        TextView nameItem;
    }
}