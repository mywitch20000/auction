package com.doollee.auction.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.doollee.auction.R;
import com.doollee.auction.datatype.PriceBidInfo;

import java.text.DecimalFormat;
import java.util.ArrayList;

/**
 *
 * Class: PriceBidAdapter
 * Created by wizard on 2021.02.05
 *
 * Description: 경매가격 Adapter
 */

public class PriceBidAdapter extends RecyclerView.Adapter<PriceBidAdapter.PriceBidViewHolder> {
    private Context context;

    private ArrayList<PriceBidInfo>   listPriceBidInfo;    // 경매 리스트

    private int        selectPos = -1;                      // 선택된 경매 위치

    private onListener listener;                           // recyclerView 이벤트 listener

    public PriceBidAdapter(Context context, ArrayList<PriceBidInfo> listPriceBidInfo, onListener listener) {
        this.context = context;
        this.listPriceBidInfo = listPriceBidInfo;
        this.listener = listener;
    }

    public class PriceBidViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public RelativeLayout layoutPriceBidItem;
        public TextView textTime;
        public TextView textCategory;
        public TextView textArea;
        public TextView textAmount;
        public TextView textStandard;
        public TextView textPrice;

        public PriceBidViewHolder(View view) {
            super(view);

            layoutPriceBidItem = (RelativeLayout) view.findViewById(R.id.layout_price_bid_item);
            layoutPriceBidItem.setOnClickListener(this);
            textTime = (TextView) view.findViewById(R.id.text_time_bid);
            textCategory = (TextView) view.findViewById(R.id.text_category_item);
            textArea = (TextView) view.findViewById(R.id.text_area_item);
            textAmount = (TextView) view.findViewById(R.id.text_amount_item);
            textStandard = (TextView) view.findViewById(R.id.text_standard_item);
            textPrice = (TextView) view.findViewById(R.id.text_price_item);
        }

        @Override
        public void onClick(View view) {
            int position = (int) view.getTag();

            if (view == layoutPriceBidItem) {
                selectPos = position;
                notifyDataSetChanged();

                if (listener != null)
                    listener.onClickListener(position);
            }
        }
    }

    @Override
    public PriceBidAdapter.PriceBidViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_price_bid_item, parent, false);

        PriceBidViewHolder vh = new PriceBidViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(PriceBidViewHolder viewHolder, final int position) {
        viewHolder.layoutPriceBidItem.setTag(position);

        PriceBidInfo priceBidInfo = listPriceBidInfo.get(position);
        if (priceBidInfo == null)
            return;

        String sbidTime = priceBidInfo.getSbidTime();
        if (!TextUtils.isEmpty(sbidTime))
            viewHolder.textTime.setText(sbidTime);

        String stdPrdlstNewNm = priceBidInfo.getStdPrdlstNewNm();
        if (!TextUtils.isEmpty(stdPrdlstNewNm))
            viewHolder.textCategory.setText(stdPrdlstNewNm);

        String cprMtcNm = priceBidInfo.getCprMtcNm();
        if (!TextUtils.isEmpty(cprMtcNm))
            viewHolder.textArea.setText(cprMtcNm);

        String delngQy = priceBidInfo.getDelngQy();
        if (!TextUtils.isEmpty(delngQy))
            viewHolder.textAmount.setText(delngQy);

        String stdFrmlcNewNm = priceBidInfo.getStdFrmlcNewNm();
        if (!TextUtils.isEmpty(stdFrmlcNewNm))
            viewHolder.textStandard.setText(stdFrmlcNewNm);

        String sbidPric = priceBidInfo.getSbidPric();
        if (!TextUtils.isEmpty(sbidPric)) {
            double amount = Double.parseDouble(sbidPric);
            DecimalFormat formatter = new DecimalFormat("#,###");
            String formatted = formatter.format(amount);
            viewHolder.textPrice.setText(formatted + "원");
        }

        /*if (position == selectPos) {
            viewHolder.layoutPriceBidItem.setBackground(context.getResources().getDrawable(R.drawable.selector_price_bid_sel));
            viewHolder.textTime.setTextColor(context.getResources().getColor(R.color.selector_price_bid_text_sel));
        } else {
            viewHolder.layoutPriceBidItem.setBackground(context.getResources().getDrawable(R.drawable.selector_price_bid_unsel));
            viewHolder.textTime.setTextColor(context.getResources().getColor(R.color.selector_price_bid_text_unsel));
        }*/
    }

    @Override
    public int getItemCount() {
        return listPriceBidInfo.size();
    }

    public void setPriceBid(ArrayList<PriceBidInfo> listPriceBidInfo) {
        this.listPriceBidInfo = listPriceBidInfo;
        notifyDataSetChanged();
    }

    /**
     * recyclerView 이벤트 listener
     */
    public interface onListener {
        void onClickListener(int position);
    }
}