package com.doollee.auction.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.doollee.auction.R;
import com.doollee.auction.datatype.AreaInfo;
import com.doollee.auction.datatype.MarketInfo;

import java.util.ArrayList;

/**
 *
 * AreaAdapter
 * Created by wizard on 2021.02.23
 *
 * Description: 지역 Adapter
 */

public class AreaAdapter extends BaseAdapter {
    private Context              context;
    private ArrayList<AreaInfo>  listAreaInfo;
    private int                  step;

    public AreaAdapter(Context context, ArrayList<AreaInfo> listAreaInfo, int step) {
        this.context = context;
        this.listAreaInfo = listAreaInfo;
        this.step = step;
    }

    @Override
    public int getCount() {
        return listAreaInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return listAreaInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_area_item, parent, false);

            viewHolder.nameItem = (TextView) convertView.findViewById(R.id.text_area_name);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        AreaInfo areaInfo = listAreaInfo.get(position);
        String name = "";
        if (step == 1)
            name = areaInfo.getFirstStep();
        else if (step == 2)
            name = areaInfo.getSecondStep();
        else if (step == 3)
            name = areaInfo.getThirdStep();

        if (!TextUtils.isEmpty(name))
            viewHolder.nameItem.setText(name);

        return convertView;
    }

    static  class ViewHolder {
        TextView nameItem;
    }
}