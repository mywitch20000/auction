package com.doollee.auction.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.doollee.auction.R;

import java.util.List;

/**
 *
 * Class: SpinnerAdapter
 * Created by wizard on 2020.01.29
 *
 * Description: spinner Adapter
 */

public class SpinnerAdapter extends BaseAdapter {
    Context        context;
    List<String>   name;                // 명(화면표시) 데이터
    List<String>   code;                // 코드 데이터
    LayoutInflater inflater;
    int            SelectPosition = -1; // 현재선택 position값 저장

    public SpinnerAdapter(Context context, List<String> code, List<String> name){
        this.context = context;
        this.code = code;
        this.name = name;
    }

    @Override
    public int getCount() {
        if (name != null)
            return name.size();
        else
            return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.layout_spinner_normal, parent, false);
        }

        ((TextView) convertView.findViewById(R.id.text_spinner)).setText(name.get(position));

        return convertView;
    }

    /**
     * position(순서값)으로 값 찾기
     * @param position
     * @return
     */
    @Override
    public Object getItem(int position) {
        return code.get(position);
    }

    /**
     * 코드로 값 선택하기
     * @return
     */
    public void setValuetoCode(Object inCode) {
    }

    /**
     *
     * @param position
     * @return
     */
    @Override
    public long getItemId(int position) {
        return position;
    }
}