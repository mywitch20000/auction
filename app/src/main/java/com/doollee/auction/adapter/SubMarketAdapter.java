package com.doollee.auction.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.doollee.auction.R;
import com.doollee.auction.datatype.MarketInfo;

import java.util.ArrayList;

/**
 *
 * SubMarketAdapter
 * Created by wizard on 2021.02.18
 *
 * Description: 법인 Adapter
 */

public class SubMarketAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<MarketInfo> listMarketInfo;

    public SubMarketAdapter(Context context, ArrayList<MarketInfo> listMarketInfo) {
        this.context = context;
        this.listMarketInfo = listMarketInfo;
    }

    @Override
    public int getCount() {
        return listMarketInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return listMarketInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_market_item, parent, false);

            viewHolder.nameItem = (TextView) convertView.findViewById(R.id.text_market_name);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        MarketInfo marketInfo = listMarketInfo.get(position);
        String cprOldNM = marketInfo.getCprOldNM();
        if (!TextUtils.isEmpty(cprOldNM))
            viewHolder.nameItem.setText(cprOldNM);

        return convertView;
    }

    static  class ViewHolder {
        TextView nameItem;
    }
}
