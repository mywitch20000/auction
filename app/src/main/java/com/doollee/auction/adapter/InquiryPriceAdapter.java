package com.doollee.auction.adapter;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import java.util.ArrayList;

/**
 *
 * InquiryPriceAdapter
 * Created by wizard on 2021.02.17
 *
 * Description: Fragment Adapter
 */

public class InquiryPriceAdapter extends FragmentPagerAdapter {
    private ArrayList<Fragment> fragments = new ArrayList<>();

    public InquiryPriceAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    public InquiryPriceAdapter(@NonNull FragmentManager fm, int behavior) {
        super(fm, behavior);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);
    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public void addItem(Fragment fragment) {
        fragments.add(fragment);
    }
}
