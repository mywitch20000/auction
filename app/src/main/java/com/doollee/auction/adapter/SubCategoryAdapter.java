package com.doollee.auction.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.doollee.auction.R;
import com.doollee.auction.datatype.CategoryInfo;

import java.util.ArrayList;

/**
 *
 * SubCategoryAdapter
 * Created by wizard on 2021.02.17
 *
 * Description: 부류 Adapter
 */

public class SubCategoryAdapter extends BaseAdapter {
    private Context context;
    private ArrayList<CategoryInfo> listSubCategoryInfo;

    public SubCategoryAdapter(Context context, ArrayList<CategoryInfo> listSubCategoryInfo) {
        this.context = context;
        this.listSubCategoryInfo = listSubCategoryInfo;
    }

    @Override
    public int getCount() {
        return listSubCategoryInfo.size();
    }

    @Override
    public Object getItem(int position) {
        return listSubCategoryInfo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            convertView = LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_sub_class_item, parent, false);

            viewHolder.nameItem = (TextView) convertView.findViewById(R.id.text_sub_class_name);

            convertView.setTag(viewHolder);

        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        CategoryInfo categoryInfo = listSubCategoryInfo.get(position);
        String subClassName = categoryInfo.getSubCategoryName();
        if (!TextUtils.isEmpty(subClassName))
            viewHolder.nameItem.setText(subClassName);

        return convertView;
    }

    static  class ViewHolder {
        TextView nameItem;
    }
}
