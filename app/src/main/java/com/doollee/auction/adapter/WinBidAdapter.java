package com.doollee.auction.adapter;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.doollee.auction.R;
import com.doollee.auction.ui.BaseActivity;
import com.doollee.auction.utils.ComUtils;

import java.util.ArrayList;
import java.util.Map;

/**
 *
 * Class: WinBidAdapter
 * Created by lim on 2020.12.08
 *
 * Description: 낙찰내역 Adapter
 */

public class WinBidAdapter extends RecyclerView.Adapter<WinBidAdapter.AuctionViewHolder> {
    private Context                 context         ;
    private ArrayList               listSuccessInfo ;
    private int                     selectPos  = 0  ;
    private onListener              listener        ;
    private BaseActivity            baseActivity    ;

    public WinBidAdapter(Context context, ArrayList listInfo, onListener listener) {
        this.context          = context ;
        this.listSuccessInfo  = listInfo;
        this.listener         = listener;
        this.baseActivity     = new BaseActivity();
    }

    public class AuctionViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public LinearLayout    layoutSuccessItem   ; // 낙찰내역 RecyclerView Layout ID
        public TextView        textAucNo           ; // 경매번호
        public TextView        textWrsNm           ; // 상품명
        public TextView        textSender          ; // 출하자
        public TextView        textBidAmt          ; // 낙찰가
        public TextView        textAuctioneer      ; // 경매사
        public TextView        textBidTm           ; // 경매시간
        public TextView        textDetailWrsInf    ; // 상품정보

        public AuctionViewHolder(View view) {
            super(view);

            layoutSuccessItem = (LinearLayout) view.findViewById(R.id.layout_success_item);
            layoutSuccessItem.setOnClickListener(this);

            textAucNo            = (TextView) view.findViewById(R.id.text_auc_no          );
            textAucNo            = (TextView) view.findViewById(R.id.text_auc_no          );
            textWrsNm            = (TextView) view.findViewById(R.id.text_wrs_nm          );
            textSender           = (TextView) view.findViewById(R.id.text_sender          );
            textBidAmt           = (TextView) view.findViewById(R.id.text_bid_amt         );
            textAuctioneer       = (TextView) view.findViewById(R.id.text_auctioneer      );
            textBidTm            = (TextView) view.findViewById(R.id.text_bid_tm          );
            textDetailWrsInf     = (TextView) view.findViewById(R.id.text_detail_wrs_info );
        }

        @Override
        public void onClick(View view) {
            int position = (int) view.getTag();

            if (view == layoutSuccessItem) {
                selectPos = position;

                notifyDataSetChanged();
                //notifyItemChanged(selectPos);

                if (listener != null)
                    listener.onClickListener(position);
            }
        }
    }

    @Override
    public AuctionViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = (View) LayoutInflater.from(parent.getContext()).inflate(R.layout.layout_win_bid_item, parent, false);

        AuctionViewHolder vh = new AuctionViewHolder(view);

        return vh;
    }

    @Override
    public void onBindViewHolder(AuctionViewHolder viewHolder, final int position) {
        viewHolder.layoutSuccessItem.setTag(position);

        Map dataInfo = (Map) listSuccessInfo.get(position);
        if (dataInfo == null)
            return;

        // 경매번호 화면출력
        String textAucNo = (String) dataInfo.get("AUC_NO");
        if (TextUtils.isEmpty(textAucNo) || "null".equals(textAucNo))
            viewHolder.textAucNo.setText("");
        else
            viewHolder.textAucNo.setText(textAucNo);

        // 출하자 화면출력
        String textSender = (String) dataInfo.get("SENDER");
        if (!TextUtils.isEmpty(textSender))
            viewHolder.textSender.setText(textSender);

        // 품목명 화면출력
        String textWrsNm = (String) dataInfo.get("WRS_NM");
        if (!TextUtils.isEmpty(textWrsNm))
            viewHolder.textWrsNm.setText(textWrsNm);

        // 품목정보 화면출력
        String textDetailWrsInf =
                (String) dataInfo.get("PRODUCT_AREA") + " "
                        + (String) dataInfo.get("WEIGHT") + " "
                        + (String) dataInfo.get("UNIT") + " "
                        + (String) dataInfo.get("GRADE") + " "
                        + (String) dataInfo.get("SIZE") + " "
                        + (String) dataInfo.get("QT") + "개";
        viewHolder.textDetailWrsInf.setText(textDetailWrsInf);

        // 낙찰가 화면출력
        String textBidAmt = (String) dataInfo.get("BID_AMT");
        if (TextUtils.isEmpty(textBidAmt))
            viewHolder.textBidAmt.setText("0" + " 낙찰");
        else
            viewHolder.textBidAmt.setText(ComUtils.setComma(textBidAmt) + " 낙찰");

        // 경매사 화면출력
        String textAuctioneer = (String) dataInfo.get("AUCTIONEER");
        if (!TextUtils.isEmpty(textAuctioneer))
            viewHolder.textAuctioneer.setText(textAuctioneer);

        // 응찰시간 화면출력
        String textBidTm = (String) dataInfo.get("BID_TM");
        if (!TextUtils.isEmpty(textBidTm)) {
            viewHolder.textBidTm.setText(
                    textBidTm.substring(0, 2) + ":" +
                            textBidTm.substring(2, 4) + ":" +
                            textBidTm.substring(4, 6) + "." +
                            textBidTm.substring(6));
        }
    }

    /**
     * 데이터 개수 반환
     *
     * @return 데이터 개수
     */
    @Override
    public int getItemCount() {
        return listSuccessInfo.size();
    }

    /**
     * 선택행 초기화
     */
    public void clearClickPos() {
        selectPos = 0;
        notifyDataSetChanged();
    }

    /**
     * 선택된 첫번째 depth 메뉴 위치 반환
     *
     * @return 선택된 메뉴 위치
     */
    public int getClickPos() {
        return selectPos;
    }

    /**
     * recyclerView 이벤트 listener
     */
    public interface onListener {
        void onClickListener(int position);
    }
}