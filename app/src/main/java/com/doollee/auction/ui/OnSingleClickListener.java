package com.doollee.auction.ui;

import android.os.SystemClock;
import android.view.View;

/**
 *
 * Class: OnSingleClickListener
 * Created by wizard on 2020.10.05
 *
 * Description: 버튼 중복클릭 방지용 클릭리스너
 */

public abstract class OnSingleClickListener implements View.OnClickListener {

    private static final long MIN_CLICK_INTERVAL = 600;    // ms
    private long mLastClickTime;

    public abstract void onSingleClick(View v);

    @Override
    public void onClick(View view) {
        long currentClickTime = SystemClock.uptimeMillis();
        long elapsedTime = currentClickTime - mLastClickTime;
        mLastClickTime = currentClickTime;

        if (elapsedTime <= MIN_CLICK_INTERVAL) {
            // 중복클릭 방지
            return;
        }

        onSingleClick(view);
    }
}