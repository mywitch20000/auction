package com.doollee.auction.ui.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;

import androidx.annotation.NonNull;

import com.doollee.auction.R;

/**
 * Class: CallInfoDialog
 * Created by wizard on 2020.10.05
 *
 * Description: 전화 설정 알림 다이얼로그창
 */

public class CallSettingsDialog extends Dialog implements View.OnClickListener {
    private Context   mContext;
    public Button    mBtnNoGoSetting;
    public Button    mBtnGoSetting;

    public View.OnClickListener mNListener;
    public View.OnClickListener mPListener;
    private OnDismissListener   onDismissListener;

    public CallSettingsDialog(@NonNull Context context) {
        super(context);

        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.dialog_call_info);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        setCancelable(false);

        mBtnNoGoSetting = (Button) findViewById(R.id.btn_no_go_setting);
        mBtnGoSetting = (Button) findViewById(R.id.btn_go_setting);
        mBtnNoGoSetting.setOnClickListener(this);
        mBtnGoSetting.setOnClickListener(this);

        if (mNListener == null) {
            mBtnNoGoSetting.setVisibility(View.GONE);
            mBtnGoSetting.setBackgroundResource(R.drawable.selector_radius_btnok);
        } else {
            mBtnNoGoSetting.setBackgroundResource(R.drawable.selector_radius_left_btncancel);
            mBtnGoSetting.setBackgroundResource(R.drawable.selector_radius_right_btnok);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_no_go_setting:
                if (mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    if (onDismissListener != null)
                        onDismissListener.onDismiss();
                    dismiss();
                }

                if (mNListener != null) {
                    mNListener.onClick(view);
                }
                break;

            case R.id.btn_go_setting:
                if (mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    if (onDismissListener != null)
                        onDismissListener.onDismiss();
                    dismiss();
                }

                if (mPListener != null) {
                    mPListener.onClick(view);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (onDismissListener != null)
            onDismissListener.onDismiss();
    }

    public void setOnDismissListener(OnDismissListener listener) {
        onDismissListener = listener;
    }

    public interface OnDismissListener {
        void onDismiss();
    }
}
