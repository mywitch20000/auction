package com.doollee.auction.ui;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.doollee.auction.BuildConfig;
import com.doollee.auction.R;
import com.doollee.auction.common.Constants;
import com.doollee.auction.common.net.HttpUtils;
import com.doollee.auction.datatype.LoginUserInfo;
import com.doollee.auction.utils.ComUtils;
import com.doollee.auction.utils.DialogUtils;
import com.doollee.auction.utils.Logs;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

/**
 *
 * Class: BaseActivity
 * Created by wizard on 2020.10.05
 *
 * Description: 기본 activity
 */

public class BaseActivity extends AppCompatActivity {
    private ProgressExDialog  mProgressExDialog;       // 진행바 표시창

    public boolean            isForeground  = false;   // wifi 체크실행여부

    BroadcastReceiver         airplaneReceiver;
    ConnectivityManager       connectivityManager;
    ConnectivityManager.NetworkCallback networkCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        String userAgent = HttpUtils.getUserAgent();
        if (TextUtils.isEmpty(userAgent))
            HttpUtils.makeUserAgent(this);

        if (BuildConfig.CHECK_AIRPLANE_MODE)
            registerAirplaneReceiver();

        //if (checkWifiYn)
        //    registerWifiReceiver();
    }

    @Override
    public void onResume() {
        super.onResume();

        isForeground = true;

        if (BuildConfig.CHECK_AIRPLANE_MODE) {
            if (ComUtils.checkAirplane(this)) {
                if (BuildConfig.CHECK_WIFI) {
                    if (!ComUtils.checkWifi(this)) {
                        goSetWifi();
                    }
                }
            } else {
                goAirplaneMode();
            }
        } else {
            if (BuildConfig.CHECK_WIFI) {
                if (!ComUtils.checkWifi(this)) {
                    goSetWifi();
                }
            }
        }
    }

    @Override
    public void onPause() {
        super.onPause();

        isForeground = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        unregisterAirplaneReceiver();

        //if (checkWifiYn)
            //unregisterWifiReceiver();
    }

    /**
     * 진행바 출력
     */
    public void showProgressDialog() {
        dismissProgressDialog();
        if (isFinishing())
            return;

        if (mProgressExDialog == null) {
            mProgressExDialog = new ProgressExDialog(this);
            mProgressExDialog.show();
        }
    }

    /**
     * 진행바 종료
     */
    public void dismissProgressDialog() {
        if (mProgressExDialog != null && mProgressExDialog.isShowing()) {
            mProgressExDialog.dismiss();
            mProgressExDialog = null;
        }
    }

    public void goAirplaneMode() {
        DialogUtils.alert(
                this
                , getString(R.string.confirm)   //오른쪽버튼명
                , getString(R.string.finish)    //왼쪽버튼명
                , getString(R.string.go_set_airplane) + "\n"
                        + "확인 버튼을 누른 후 표시되는 화면에서\n"
                        + "비행기 모드 사용을 실행해 주세요" //표시메세지
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        // 비행기모드가 실행되어 있지 않으면 설정 화면 실행
                        Intent airpIntent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
                        startActivity(airpIntent);
                    }
                }
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finishAffinity();
                    }
                });
    }

    /**
     * server 송신 데이터 생성
     * @param interfaceNo 인터페이스번호
     * @return Map
     */
    public Map setSendMsgHeader(String interfaceNo) {
        Map map = new ArrayMap();

        String biderId = LoginUserInfo.getInstance().getBiderId();
        String validateBidNo = LoginUserInfo.getInstance().getValidateBiderNo();
        String phoneNum = ComUtils.getPhoneNumber(BaseActivity.this);

        // 현재일시
        Date date     = new Date(System.currentTimeMillis());
        String sdfNow = new SimpleDateFormat("yyyyMMddHHmmssSS", Locale.getDefault()).format(date);

        map.put("SEND_GBN"       , "M" + interfaceNo);
        map.put("BIDER_ID"       , biderId         );
        map.put("BIDER_HP"       , phoneNum         );
        map.put("VALIDATE_BID_NO", validateBidNo   );
        map.put("REQ_TIMESTAMP"  , sdfNow           );
        return map;
    }

    private void goSetWifi() {
        DialogUtils.alert(
                BaseActivity.this
                , getString(R.string.confirm)
                , getString(R.string.finish)
                , getString(R.string.no_connect_ap) + "\n"
                        + "확인 버튼을 누른 후 표시되는 목록에서\n"
                        + " [ " + Constants.WIFI_SSID + " ] 으로 접속해 주세요." //표시메세지
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent wifiIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                        startActivity(wifiIntent); // wifi설정페이지 실행
                    }
                }
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        finishAffinity();
                    }
                }
        );
    }

    /**
     * Toast 실행
     */
    public void showCustomToast(String Massage) {
        if (TextUtils.isEmpty(Massage))
            return;

        int time = Toast.LENGTH_LONG;
        // 디자인 뷰 가져오기
        LayoutInflater inflater = getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_toast, (ViewGroup) findViewById(R.id.rlayout_toast));
        TextView txtView = view.findViewById(R.id.text_toast);
        txtView.setText(Massage);

        // Toast실행/설정
        Toast toast = new Toast(this);
        toast.setDuration(time);
        toast.setMargin(0, 0);
        toast.setGravity(Gravity.FILL, 0, 0);
        toast.setView(view);
        toast.show();
    }

    public void registerAirplaneReceiver() {
        airplaneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                if (!isForeground)
                    return;

                if (!ComUtils.checkAirplane(BaseActivity.this)) {
                    goAirplaneMode();
                }
            }
        };

        IntentFilter intentFilter = new IntentFilter("android.intent.action.AIRPLANE_MODE");
        registerReceiver(airplaneReceiver, intentFilter);
    }

    public void unregisterAirplaneReceiver() {
        if (airplaneReceiver == null)
            return;

        try {
            unregisterReceiver(airplaneReceiver);
        } catch (Exception e) {
            Logs.d(e.getMessage());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    public void registerWifiReceiver() {
        connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkRequest networkRequest =
                new NetworkRequest.Builder()                                        // addTransportType : 주어진 전송 요구 사항을 빌더에 추가
                        .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)       // TRANSPORT_WIFI : 이 네트워크가 Wi-Fi 전송을 사용함을 나타냅니다.
                        .build();

        networkCallback = new ConnectivityManager.NetworkCallback() {
            @Override
            public void onAvailable(Network network) {
                Logs.e("The default network is now: " + network);
            }

            @Override
            public void onLost(Network network) {
                Logs.e("The application no longer has a default network. The last default network was " + network);
            }

            @Override
            public void onCapabilitiesChanged(Network network, NetworkCapabilities networkCapabilities) {
                Logs.e("The default network changed capabilities: " + networkCapabilities);
            }

            @Override
            public void onLinkPropertiesChanged(Network network, LinkProperties linkProperties) {
                Logs.e("The default network changed link properties: " + linkProperties);
            }
        };
        //connectivityManager.registerDefaultNetworkCallback(networkRequest, networkCallback);
        connectivityManager.registerNetworkCallback(networkRequest, networkCallback);
    }

    public void unregisterWifiReceiver() {
        if (networkCallback == null)
            return;

         connectivityManager.unregisterNetworkCallback(networkCallback);
    }
}
