package com.doollee.auction.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.Display;
import android.view.Window;
import android.view.WindowManager.LayoutParams;

import androidx.annotation.NonNull;

/**
 * Class: BaseDialog
 * Created by wizard on 2020.10.05
 *
 * Description: 기본 다이얼로그창
 */

public class BaseDialog extends Dialog {
    public BaseDialog(@NonNull Context context) {
        super(context);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        requestWindowFeature(Window.FEATURE_NO_TITLE);

        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    }

    /**
     * 다이얼로그창 넓이 설정
     *
     * @eturn
     */
    protected void setDialogWidth() {
        Display display = getWindow().getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        // 다이얼로그의 넓이를 윈도우 넓이 만큼 늘린다.
        LayoutParams lp = getWindow().getAttributes();
        lp.width = size.x;
        getWindow().setAttributes(lp) ;
    }
}
