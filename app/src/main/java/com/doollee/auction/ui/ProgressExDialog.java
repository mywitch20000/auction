package com.doollee.auction.ui;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.view.WindowManager;
import android.widget.ImageView;

import androidx.annotation.Nullable;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.load.resource.gif.GifDrawable;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.RequestOptions;
import com.bumptech.glide.request.target.Target;
import com.doollee.auction.R;

/**
 *
 * Class: ProgressExDialog
 * Created by wizard on 2020.10.05
 *
 * Description: 진행바 화면
 */

public class ProgressExDialog extends Dialog {
    private Context  context;

    private ImageView imageProgress;

    public ProgressExDialog(Context context) {
        super(context);

        this.context = context;

        requestWindowFeature(android.view.Window.FEATURE_NO_TITLE);
        getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        setCancelable(false);
        getWindow().clearFlags(WindowManager.LayoutParams.FLAG_DIM_BEHIND);
        //getWindow().setDimAmount(0.1f);
        setContentView(R.layout.dialog_progress);

        imageProgress = (ImageView) findViewById(R.id.img_progress);

        startProgress();
    }

    /**
     * 이미지를 재생한다.
     *
     * @return
     */
    private void startProgress() {
        RequestOptions reqOptions = new RequestOptions()
                .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
                .centerCrop()
                .skipMemoryCache(true);

        Glide.with(context)
                .load(R.drawable.img_progress)
                .listener(requestListener)
                .apply(reqOptions)
                .into(imageProgress)
                .clearOnDetach();
    }

    /**
     * gif requestListener
     */
    RequestListener<Drawable> requestListener = new RequestListener<Drawable>() {
        @Override
        public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
            return false;
        }

        @Override
        public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
            final GifDrawable gifDrawable = (GifDrawable) resource;
            gifDrawable.setLoopCount(0);

            return false;
        }
    };
}