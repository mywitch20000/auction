package com.doollee.auction.ui;

import android.view.GestureDetector;
import android.view.MotionEvent;

import com.doollee.auction.utils.Logs;

/**
 *
 * Class: SwipeDetector
 * Created by wizard on 2020.10.05
 *
 * Description: 뷰에 좌, 우 swipe 기능 포함
 */

public class SwipeDetector extends GestureDetector.SimpleOnGestureListener {
    private int swipeMinDistance = 120;
    private int swipeMaxOffPath = 400;
    private int swipeThresholdVelocity = 200;

    private SwipeListener listener;

    @Override
    public boolean onFling(MotionEvent e1, MotionEvent e2, float velocityX, float velocityY) {
        try {
            if (Math.abs(e1.getY() - e2.getY()) > swipeMaxOffPath) {
                return false;
            }

            // right to left swipe
            if (e1.getX() - e2.getX() > swipeMinDistance
                    && Math.abs(velocityX) > swipeThresholdVelocity) {
                if (listener != null)
                    listener.onLeftSwipe();
            }
            // left to right swipe
            else if (e2.getX() - e1.getX() > swipeMinDistance
                    && Math.abs(velocityX) > swipeThresholdVelocity) {
                if (listener != null)
                    listener.onRightSwipe();
            }
        } catch (Exception e) {
            Logs.d(e.getMessage());
        }
        return false;
    }

    public void setSwipe(int swipeMinDistance, int swipeMaxOffPath, int swipeThresholdVelocity) {
        this.swipeMinDistance = swipeMinDistance;
        this.swipeMaxOffPath = swipeMaxOffPath;
        this.swipeThresholdVelocity = swipeThresholdVelocity;
    }

    /**
     * SwipeDetector 리스너를 등록한다.
     *
     * @param listener
     */
    public void setSwipeListener(SwipeListener listener) {
        this.listener = listener;
    }

    public interface SwipeListener {
        void onLeftSwipe();
        void onRightSwipe();
    }
}