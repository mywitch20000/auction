package com.doollee.auction.ui;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.view.View;

import com.doollee.auction.ACApplication;
import com.doollee.auction.R;
import com.doollee.auction.activity.IntroActivity;
import com.doollee.auction.activity.LoginActivity;
import com.doollee.auction.activity.MainActivity;
import com.doollee.auction.common.net.SocketRecvTask;
import com.doollee.auction.common.net.SocketRecver;
import com.doollee.auction.manager.DataManager;
import com.doollee.auction.utils.DialogUtils;
import com.doollee.auction.utils.JsonUtils;
import com.doollee.auction.utils.Logs;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 *
 * Class: BaseActivity
 * Created by wizard on 2020.10.05
 *
 * Description: 기본 activity
 */

public class BaseSocketActivity extends BaseActivity {
    public SocketRecvTask.OnSocketCallback socketCallback;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onResume() {
        super.onResume();

        setRecvSocket();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

    }

    private void setRecvSocket() {
        if (SocketRecver.getInstance().getIsRunning()) {
            if (socketCallback == null) {
                socketCallback = new SocketRecvTask.OnSocketCallback() {
                    @Override
                    public void onConnect() {

                    }

                    @Override
                    public void onReceive(String msg) {
                        Logs.d("====== onReceive : " + msg);
                        if (TextUtils.isEmpty(msg))
                            return;

                        //수신데이터 저장
                        recvDataSave(msg);

                        //응찰화면 데이터 set
                        biddingRecvMsg();
                    }

                    @Override
                    public void onFail(String msg) {
                        Logs.d("====== onFail : " + msg);

                        if (msg.contains(getString(R.string.server_connection_error))) {
                            DialogUtils.alert(
                                    BaseSocketActivity.this
                                    , getString(R.string.confirm)
                                    , getString(R.string.server_connection_error_msg) + "\n" + msg
                                    , new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Intent intent = new Intent(BaseSocketActivity.this, LoginActivity.class);
                                            startActivity(intent);
                                        }
                                    }
                            );
                        }
                    }

                    @Override
                    public void onDisconnect() {

                    }
                };
            }

            SocketRecver.getInstance().setSocketCallback(socketCallback);
        }
    }

    protected void recvDataSave(String inMsgData) {
        try {
            ///응찰시작/종료데이터 수신 /////////////////////////////////////////////////
            String recvRtMsg, sdfNow;
            List<Map<String, String>> list = new ArrayList<>();
            Handler mHandler = new Handler(Looper.getMainLooper());

            Map mapRecvRtData = JsonUtils.jsonStringToMap(inMsgData);
            if (mapRecvRtData == null) {

            } else {
                //메세지 생성하기
                if (mapRecvRtData.containsKey("WRS_C")) {
                    recvRtMsg = "[" + mapRecvRtData.get("AUC_NO") + "]" + "번 경매를 진행합니다.";
                } else {
                    recvRtMsg = "[" + mapRecvRtData.get("AUC_NO") + "]" + "번 경매"
                            + ("3".equals(mapRecvRtData.get("AUC_ST")) ? "에 낙찰" : "가 종료") + "되었습니다.";
                }

                //화면에 메세지 표시하기
                final String finalRecvRtMsg = recvRtMsg;
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        showCustomToast(finalRecvRtMsg);
                    }
                });

                // 현재시간가져오기
                sdfNow = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date(System.currentTimeMillis()));

                // 기존에 전역데이터가 있었던 경우 가져오기
                if (!TextUtils.isEmpty(String.valueOf(DataManager.getInstance().getData("NoticeList")))) {
                    list = (ArrayList<Map<String, String>>) DataManager.getInstance().getData("NoticeList");
                }

                // 전역변수에 값 추가
                Map<String, String> map = new ArrayMap<>();
                map.put("time" , sdfNow   );
                map.put("cont" , inMsgData);
                map.put("msg"  , recvRtMsg);
                map.put("newYn", "Y"      );
                list.add(map);
                DataManager.getInstance().setData("NoticeList", list);
                ////////////////////////////////////////////////////////////////////////////
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    protected void biddingRecvMsg() {
        //
    }
}
