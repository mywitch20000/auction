package com.doollee.auction.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.doollee.auction.R;
import com.doollee.auction.common.Constants;
import com.doollee.auction.common.net.SocketSendTask;
import com.doollee.auction.common.net.SocketSender;
import com.doollee.auction.manager.DataManager;
import com.doollee.auction.ui.BaseActivity;
import com.doollee.auction.utils.JsonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ScheduleDetailActivity extends BaseActivity {
    private TextView     mTextAucNo;
    private TextView     mTextWrsC;
    private TextView     mTextWrsMm;
    private TextView     mTextSenderId;
    private TextView     mTextProductArea;
    private TextView     mTextGrade;
    private TextView     mTextQt;
    private TextView     mTextWeight;
    private TextView     mTextSize;
    private TextView     mTextCommissionrate;
    private TextView     mTextAucTm;
    private TextView     mTextAucSt;
    private TextView     mTextAucLine;

    private int          mSelectedPos;
    private ArrayList    mListScheduleInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_detail);

        getExtra();
        initUX();

        requestScheduleDetail(mSelectedPos);
    }

    private void getExtra() {
        Intent intent = getIntent();

        if (intent.hasExtra(Constants.ScheduleInfo.SCHEDULE_POS)) {
            mSelectedPos = intent.getIntExtra(Constants.ScheduleInfo.SCHEDULE_POS, 0);
        }

        if (intent.hasExtra(Constants.ScheduleInfo.SCHEDULE_LIST)) {
            mListScheduleInfo = intent.getStringArrayListExtra(Constants.Schedule.SCHEDULE_LIST);
        }
    }

    private void initUX() {
        TextView btnBack =  (TextView) findViewById(R.id.btn_back);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mTextAucNo = findViewById(R.id.text_auc_no_detail);
        mTextWrsC = findViewById(R.id.text_wrs_c_detail);
        mTextWrsMm = findViewById(R.id.text_wrs_nm_detail);
        mTextSenderId = findViewById(R.id.text_sender_id_detail);
        mTextProductArea = findViewById(R.id.text_product_area_detail);
        mTextGrade = findViewById(R.id.text_grade_detail);
        mTextQt = findViewById(R.id.text_qt_detail);
        mTextWeight = findViewById(R.id.text_weight_detail);
        mTextSize = findViewById(R.id.text_size_detail);
        mTextCommissionrate = findViewById(R.id.text_commissionrate_detail);
        mTextAucTm = findViewById(R.id.text_auc_tm_detail);
        mTextAucSt = findViewById(R.id.text_auc_st_detail);
        mTextAucLine = findViewById(R.id.text_auc_line_detail);

        TextView preBtn = findViewById(R.id.text_pre_wrs_sel);
        preBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedPos -= 1;
                if (mSelectedPos < 0)
                    mSelectedPos = 0;

                requestScheduleDetail(mSelectedPos);
            }
        });

        TextView nextBtn =  (TextView) findViewById(R.id.text_next_wrs_sel);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mSelectedPos += 1;
                if (mSelectedPos >= mListScheduleInfo.size())
                    mSelectedPos = mListScheduleInfo.size() - 1;

                requestScheduleDetail(mSelectedPos);
            }
        });
    }

    public void requestScheduleDetail(int position) {
        Map mapInfo = (Map) mListScheduleInfo.get(position);
        String aucNo = (String) mapInfo.get("AUC_NO");
        if (TextUtils.isEmpty(aucNo))
            return;

        SocketSendTask.OnSocketCallback socketCallback = new SocketSendTask.OnSocketCallback() {
            @Override
            public void onConnect() {

            }

            @Override
            public void onFail(String msg) {
                dismissProgressDialog();
            }

            @Override
            public void onSuccess(String result) {
                dismissProgressDialog();

                Log.d("===================", result);

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject == null)
                        return;

                    Map mapDataInfo = JsonUtils.jsonToMap(jsonObject);
                    if (!"NO DATA".equals(mapDataInfo.get("FAIL_MSG"))) {
                        Log.d("", mapDataInfo.toString());
                        if (Constants.DefineValue.YES.equals(mapDataInfo.get("SUCCESS_YN"))) {
                            Log.d("", mapDataInfo.toString());
                            ArrayList listInfo = (ArrayList) mapDataInfo.get("body");
                            if (listInfo == null || listInfo.size() <= 0)
                                return;

                            HashMap map = (HashMap) listInfo.get(0);

                            String AUC_NO = (String) map.get("AUC_NO");
                            if (TextUtils.isEmpty(AUC_NO))
                                AUC_NO = "";

                            mTextAucNo.setText(AUC_NO);

                            String WRS_C = (String) map.get("WRS_C");
                            if (TextUtils.isEmpty(WRS_C))
                                WRS_C = "";

                            mTextWrsC.setText(WRS_C);

                            String WRS_NM = (String) map.get("WRS_NM");
                            if (TextUtils.isEmpty(WRS_NM))
                                WRS_NM = "";

                            mTextWrsMm.setText(WRS_NM);

                            String SENDER_ID = (String) map.get("SENDER_ID");
                            if (TextUtils.isEmpty(SENDER_ID))
                                SENDER_ID = "";

                            mTextSenderId.setText(SENDER_ID);

                            String PRODUCT_AREA = (String) map.get("PRODUCT_AREA");
                            if (TextUtils.isEmpty(PRODUCT_AREA))
                                PRODUCT_AREA = "";

                            mTextProductArea.setText(PRODUCT_AREA);

                            String GRADE = (String) map.get("GRADE");
                            if (TextUtils.isEmpty(GRADE))
                                GRADE = "";

                            mTextGrade.setText(GRADE);

                            String QT = (String) map.get("QT");
                            if (TextUtils.isEmpty(QT))
                                QT = "";

                            mTextQt.setText(QT);

                            String WEIGHT = (String) map.get("WEIGHT");
                            if (TextUtils.isEmpty(WEIGHT))
                                WEIGHT = "";

                            mTextWeight.setText(WEIGHT);

                            String SIZE = (String) map.get("SIZE");
                            if (TextUtils.isEmpty(SIZE))
                                SIZE = "";

                            mTextSize.setText(SIZE);

                            String COMMISSIONRATE = (String) map.get("COMMISSIONRATE");
                            if (TextUtils.isEmpty(COMMISSIONRATE))
                                COMMISSIONRATE = "";

                            mTextCommissionrate.setText(COMMISSIONRATE);

                            String AUC_TM = (String) map.get("AUC_TM");
                            if (TextUtils.isEmpty(AUC_TM))
                                AUC_TM = "";

                            mTextAucTm.setText(AUC_TM);

                            String AUC_ST = (String) map.get("AUC_ST");
                            if (TextUtils.isEmpty(AUC_ST))
                                AUC_ST = "";

                            mTextAucSt.setText(AUC_ST);

                            String AUC_LINE = (String) map.get("AUC_LINE");
                            if (TextUtils.isEmpty(AUC_LINE))
                                AUC_LINE = "";

                            mTextAucLine.setText(AUC_LINE);

                        } else {
                            showCustomToast((String) mapDataInfo.get("FAIL_MSG"));
                        }
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map map = setSendMsgHeader("103");
        map.put("AUC_NO", aucNo);

        JSONObject json = JsonUtils.mapToJson(map);

        showProgressDialog();

        Socket socket = DataManager.getInstance().getSocketMapData(10001);
        SocketSender.getInstance().sendMsg(socket, "10001", json.toString(), socketCallback);
    }
}