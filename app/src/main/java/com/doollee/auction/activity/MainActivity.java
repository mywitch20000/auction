package com.doollee.auction.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.doollee.auction.ACApplication;
import com.doollee.auction.R;
import com.doollee.auction.common.PreferenceManager;
import com.doollee.auction.ui.BaseSocketActivity;
import com.doollee.auction.utils.DialogUtils;
import com.doollee.auction.utils.JsonUtils;

import org.json.JSONException;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class MainActivity extends BaseSocketActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        initUX();

        // 화면실행시 오늘날짜 이외 관심목록삭제
        initFavoData();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        //SvrSocket.close("ALL"); //소켓모두닫기
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        DialogUtils.alert(
                MainActivity.this
                , getString(R.string.confirm)   //오른쪽버튼명
                , getString(R.string.cancel)    //왼쪽버튼명
                , getString(R.string.confirm_app_exit)
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ACApplication applicationClass = (ACApplication) getApplicationContext();
                        if (applicationClass != null)
                            applicationClass.allActivityFinish(true);
                        else
                            finish();
                    }
                }
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }
        );
    }

    private void initUX() {
        // 경매일정 화면이동 버튼
        TextView btnViewSchedule = findViewById(R.id.text_schedule);
        btnViewSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, InquiryScheduleActivity.class);
                startActivity(intent);
                overridePendingTransition(0, 0);
            }
        });

        // 낙찰내역 화면이동 버튼
        TextView btnViewSuccess = findViewById(R.id.text_success);
        btnViewSuccess.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, WinBidActivity.class);
                startActivity(intent);
            }
        });

        // 응찰참여 화면이동 버튼
        TextView btnGoBidding = findViewById(R.id.text_go_bidding);
        btnGoBidding.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, BiddingActivity.class);
                startActivity(intent);
            }
        });

        TextView btnGoPriceBid = findViewById(R.id.text_go_price_bid);
        btnGoPriceBid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, InquiryPriceBidActivity.class);
                startActivity(intent);
            }
        });
    }

    /**
     * 관심목록 초기화
     */
    private void initFavoData() {
        if (!"".equals(PreferenceManager.getString(this, "ScheduleFavoData"))) {
            try {
                Map favoMap = JsonUtils.jsonStringToMap((String) PreferenceManager.getString(this, "ScheduleFavoData"));
                if (favoMap.containsKey("insDay")) {
                    String sdfNow = new SimpleDateFormat("yyyy-MM-dd(EE)", Locale.getDefault()).format(new Date(System.currentTimeMillis()));
                    if (!sdfNow.equals(favoMap.get("insDay"))) {
                        PreferenceManager.setString(this, "ScheduleFavoData", "");
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }
}
