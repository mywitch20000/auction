package com.doollee.auction.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.doollee.auction.R;
import com.doollee.auction.adapter.ScheduleAdapter;
import com.doollee.auction.common.Constants;
import com.doollee.auction.ui.BaseSocketActivity;
import com.doollee.auction.utils.Logs;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class ScheduleActivity extends BaseSocketActivity {
    private TextView          mTextNoSchedule;
    private RecyclerView      mRecyclerSchedule;

    private int               mSelectedPos = 0;
    private ArrayList<String> mListScheduleInfo;
    private ScheduleAdapter   mScheduleAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_schedule);

        getExtra();
        initUX();
        setSchedule();
    }

    private void getExtra() {
        Intent intent = getIntent();

        if (intent.hasExtra(Constants.Schedule.SCHEDULE_LIST)) {
            mListScheduleInfo = intent.getStringArrayListExtra(Constants.Schedule.SCHEDULE_LIST);
        }
    }

    @SuppressLint("UseCompatLoadingForDrawables")
    private void initUX() {
        TextView sideSchedule = findViewById(R.id.go_schedule);
        sideSchedule.setBackground(getDrawable(R.drawable.selector_left_radius_btn_blue));

        TextView btnBack = findViewById(R.id.btn_back);
        mTextNoSchedule = findViewById(R.id.text_no_schedule);
        mRecyclerSchedule = findViewById(R.id.recycler_schedule);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // 낙찰내역 화면이동 버튼
        TextView successView = findViewById(R.id.go_win_bid);
        successView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScheduleActivity.this, WinBidActivity.class);
                startActivity(intent);
                finish();
            }
        });

        // 응찰참여 화면이동 버튼
        TextView bidView = findViewById(R.id.go_select_bid);
        bidView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScheduleActivity.this, BiddingActivity.class);
                startActivity(intent);
                finish();
            }
        });

        // 검색조건화면이동버튼
        TextView CallSearchView = findViewById(R.id.text_select_view);
        CallSearchView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScheduleActivity.this, InquiryScheduleActivity.class);
                startActivity(intent);
                finish();
            }
        });

        // 알림내용 보기
        TextView goNoticePopup = findViewById(R.id.go_notice_popup);
        goNoticePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScheduleActivity.this, BottomNoticeActivity.class);
                startActivity(intent);
            }
        });

        Button btnDetailSchedule = findViewById(R.id.btn_detail_schedule);
        btnDetailSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ScheduleActivity.this, ScheduleDetailActivity.class);
                intent.putExtra(Constants.ScheduleInfo.SCHEDULE_POS, mSelectedPos);
                intent.putStringArrayListExtra(Constants.ScheduleInfo.SCHEDULE_LIST, mListScheduleInfo);
                startActivity(intent);
            }
        });
    }

    private void setSchedule() {
        if(mListScheduleInfo == null || mListScheduleInfo.size() <= 0) {
            mTextNoSchedule.setVisibility(View.VISIBLE);
            mRecyclerSchedule.setVisibility(View.GONE);
            return;
        }

        //초기행선택값 생성
        ArrayList arrayListScheduleInfo = mListScheduleInfo;
        Map map_ScheduleInfo ;
        int initPosition = 0 ;
        for(int i = 0; i < mListScheduleInfo.size(); i++) {
            map_ScheduleInfo = (HashMap) arrayListScheduleInfo.get(i);
            if (TextUtils.isEmpty(String.valueOf(map_ScheduleInfo.get("AUC_TM")))) {
                initPosition = i;
                break;
            }
        }

        if (mScheduleAdapter == null) {
            mScheduleAdapter = new ScheduleAdapter(this, mListScheduleInfo, initPosition, new ScheduleAdapter.onListener() {
                @Override
                public void onClickListener(int position) {
                    mSelectedPos = position;
                }
            });
            mRecyclerSchedule.setAdapter(mScheduleAdapter);

            LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            mRecyclerSchedule.setLayoutManager(llm);
            mRecyclerSchedule.setHasFixedSize(true);

            //mRecyclerSchedule.smoothScrollToPosition(initPosition);
            ((LinearLayoutManager) mRecyclerSchedule.getLayoutManager()).scrollToPositionWithOffset(initPosition, 20);
        }

        mTextNoSchedule.setVisibility(View.GONE);
        mRecyclerSchedule.setVisibility(View.VISIBLE);
    }
}