package com.doollee.auction.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.doollee.auction.R;
import com.doollee.auction.adapter.SpinnerAdapter;
import com.doollee.auction.common.Constants;
import com.doollee.auction.common.net.SocketSendTask;
import com.doollee.auction.common.net.SocketSender;
import com.doollee.auction.manager.DataManager;
import com.doollee.auction.ui.BaseSocketActivity;
import com.doollee.auction.ui.OnSingleClickListener;
import com.doollee.auction.utils.JsonUtils;
import com.doollee.auction.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;

public class InquiryScheduleActivity extends BaseSocketActivity {
    private ScrollView       mScrollMain;
    private EditText         mEditWrsNm;
    private Spinner          mSpinnerItem;
    private Spinner          mSpinnerAuctionState;

    private SpinnerAdapter   mSpinnerItemAdapter;
    private SpinnerAdapter   mSpinnerStateAdapter;

    private String           mCodeItem;
    private String           mCodeAuctionState;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inquiry_schedule);

        initUX();
    }

    private void initUX() {
        TextView backBtn = findViewById(R.id.btn_back);
        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
            }
        });

        mScrollMain = findViewById(R.id.scrollvew_schedule_sel);
        mSpinnerItem = findViewById(R.id.spinner_receive_type);
        mEditWrsNm = findViewById(R.id.edit_wrs_nm);
        mSpinnerAuctionState = findViewById(R.id.spinner_auc_st);
        Button btnInquiry = findViewById(R.id.btn_inquiry_schedule);

        btnInquiry.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                requestSchedule();
            }
        });

        mEditWrsNm.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus) {
                    InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    inputMethodManager.hideSoftInputFromWindow(mEditWrsNm.getWindowToken(), 0);
                } else {
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            //mScrollMain.smoothScrollTo(0, mEditWrsNm.getBottom() + mScrollMain.getPaddingBottom());
                            //mScrollMain.fullScroll(View.FOCUS_DOWN);
                            View lastChild = mScrollMain.getChildAt(mScrollMain.getChildCount() - 1);
                            int bottom = lastChild.getBottom() + mScrollMain.getPaddingBottom();
                            int sy = mScrollMain.getScrollY();
                            int sh = mScrollMain.getHeight();
                            int delta = bottom - (sy + sh);

                            mScrollMain.smoothScrollBy(0, delta);
                        }
                    }, 200);
                }
            }
        });

        requestItemData();
    }

    private void requestItemData() {
        SocketSendTask.OnSocketCallback socketCallback = new SocketSendTask.OnSocketCallback() {
            @Override
            public void onConnect() {

            }

            @Override
            public void onFail(String msg) {
                Logs.d("onFail SocketCall");

                requestCommData();
            }

            @Override
            public void onSuccess(String result) {
                try {
                    JSONObject jsonObject = new JSONObject(result);

                    Map map = JsonUtils.jsonToMap(jsonObject);

                    final ArrayList arrayCode = (ArrayList) map.get("code");
                    ArrayList arrayName = (ArrayList) map.get("name");
                    if (arrayCode == null || arrayCode.size() <= 0 || arrayCode.size() <= 0) {
                        return;
                    } else {
                        arrayCode.size();
                        arrayCode.size();
                    }

                    mCodeItem = "99";

                    if (mSpinnerItemAdapter == null) {
                        mSpinnerItemAdapter = new SpinnerAdapter(InquiryScheduleActivity.this, arrayCode, arrayName);
                        mSpinnerItem.setAdapter(mSpinnerItemAdapter);
                    }

                    mSpinnerItem.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mCodeItem = (String) arrayCode.get(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    requestCommData();

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Map map = setSendMsgHeader("901");
        map.put("SIMP_TPC", "RECEIVE_TYPE");

        JSONObject json = JsonUtils.mapToJson(map);

        showProgressDialog();

        Socket socket = DataManager.getInstance().getSocketMapData(10001);
        SocketSender.getInstance().sendMsg(socket, "10001", json.toString(), socketCallback);
    }

    private void requestCommData() {
        SocketSendTask.OnSocketCallback socketCallback = new SocketSendTask.OnSocketCallback() {
            @Override
            public void onConnect() {

            }

            @Override
            public void onFail(String msg) {
                dismissProgressDialog();
            }

            @Override
            public void onSuccess(String result) {
                dismissProgressDialog();

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    Map map = JsonUtils.jsonToMap(jsonObject);

                    final ArrayList arrayCode = (ArrayList) map.get("code");
                    ArrayList arrayName = (ArrayList) map.get("name");
                    if (arrayCode == null || arrayCode.size() <= 0) {
                        return;
                    } else {
                        arrayCode.size();
                        arrayCode.size();
                    }

                    arrayCode.add(0, "");
                    arrayName.add(0, "전체");

                    mCodeAuctionState = "99";

                    if (mSpinnerStateAdapter == null) {
                        mSpinnerStateAdapter = new SpinnerAdapter(InquiryScheduleActivity.this, arrayCode, arrayName);
                        mSpinnerAuctionState.setAdapter(mSpinnerStateAdapter);
                    }

                    mSpinnerAuctionState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                            mCodeAuctionState = (String) arrayCode.get(position);
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> parent) {

                        }
                    });

                    // 기존 저장된 조회조건 값이 있으면 셋
                    if (!TextUtils.isEmpty((String) DataManager.getInstance().getData("ScheSelSelectReq"))) {
                        JSONObject jo = (JSONObject) DataManager.getInstance().getData("ScheSelSelectReq");
                        Map<String, Object> selectMap = JsonUtils.jsonToMap(jo);

                        if (mSpinnerStateAdapter != null)
                            mSpinnerStateAdapter.setValuetoCode(selectMap.get("RECEIVE_TYPE"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

        };

        Map map = setSendMsgHeader("901");
        map.put("SIMP_TPC", "AUC_ST");

        JSONObject json = JsonUtils.mapToJson(map);

        showProgressDialog();

        Socket socket = DataManager.getInstance().getSocketMapData(10001);
        SocketSender.getInstance().sendMsg(socket, "10001", json.toString(), socketCallback);
    }

    // 일정조회 데이터 받아오기
    public void requestSchedule() {
        SocketSendTask.OnSocketCallback socketCallback = new SocketSendTask.OnSocketCallback() {
            @Override
            public void onConnect() {

            }

            @Override
            public void onFail(String msg) {
                dismissProgressDialog();
                showCustomToast(getString(R.string.msg_no_response));
            }

            @Override
            public void onSuccess(String result) {
                dismissProgressDialog();

                Log.d("===================", result);

                try {
                    JSONObject jsonObject = new JSONObject(result);

                    Map mapDataInfo = JsonUtils.jsonToMap(jsonObject);
                    if (!"NO DATA".equals(mapDataInfo.get("FAIL_MSG"))) {
                        if (Constants.DefineValue.YES.equals(mapDataInfo.get("SUCCESS_YN")) ) {
                            ArrayList listSchedule = (ArrayList) mapDataInfo.get("body");
                            if (listSchedule == null || listSchedule.size() <= 0)
                                return;

                            Intent intent = new Intent(InquiryScheduleActivity.this, ScheduleActivity.class);
                            intent.putStringArrayListExtra(Constants.Schedule.SCHEDULE_LIST, listSchedule);
                            startActivity(intent);
                            finish();
                        } else {
                            showCustomToast((String) mapDataInfo.get("FAIL_MSG"));
                        }
                    } else {
                        showCustomToast(getString(R.string.msg_no_inquiry_result));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        };

        Map map = setSendMsgHeader("101");
        map.put("RECEIVE_TYPE", mCodeItem);
        map.put("WRS_NM", mEditWrsNm.getText().toString());
        map.put("AUC_ST", mCodeAuctionState);
        map.put("AUC_TM_ST", "0000");
        map.put("AUC_TM_ED", "2400");

        JSONObject json = JsonUtils.mapToJson(map);

        showProgressDialog();

        Socket socket = DataManager.getInstance().getSocketMapData(10001);
        SocketSender.getInstance().sendMsg(socket, "10001", json.toString(), socketCallback);
    }
}
