package com.doollee.auction.activity;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doollee.auction.R;
import com.doollee.auction.common.Constants;
import com.doollee.auction.common.net.SocketRecvTask;
import com.doollee.auction.common.net.SocketRecver;
import com.doollee.auction.manager.DataManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static com.doollee.auction.R.id.layout_notice_cont;
import static com.doollee.auction.R.layout.layout_notice_item;

public class BottomNoticeActivity extends Activity {
    LinearLayout   layout_notive_cont ;
    LayoutInflater inflater           ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        overridePendingTransition(R.anim.layout_horizon_start_left, 0);

        setContentView(R.layout.activity_bottom_notice);

        layout_notive_cont = (LinearLayout) findViewById(layout_notice_cont);
        inflater           = (LayoutInflater) BottomNoticeActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        initUX();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(0, R.anim.layout_horizon_end_right);
    }

    private void initUX() {
        LinearLayout layoutBottomPopup = (LinearLayout) findViewById(R.id.layout_bottom_popup);
        layoutBottomPopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.layout_horizon_end_right);
            }
        });

        LinearLayout activityForm = (LinearLayout) findViewById(R.id.activityForm);
        activityForm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, R.anim.layout_horizon_end_right);
            }
        });

        //데이터 화면에 표시(초기)
        insertNoticeData();

        //데이터 화면에 표시(실시간 변경)
        /*SocketRecvTask.OnSocketCallback socketCallback = new SocketRecvTask.OnSocketCallback() {
            @Override
            public void onConnect() { }

            @Override
            public void onReceive(String msg) {
                if (TextUtils.isEmpty(msg))
                    return;

                Handler mHandler = new Handler(Looper.getMainLooper());
                mHandler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        insertNoticeData();
                    }
                }, 0);
            }

            @Override
            public void onFail(String msg) { }

            @Override
            public void onDisconnect() { }
        };
        SocketRecver.getInstance().setSocketCallback(socketCallback);*/
    }

    public void insertNoticeData() {
        LinearLayout   ll ;
        TextView       tv ;
        if (!TextUtils.isEmpty(String.valueOf(DataManager.getInstance().getData("NoticeList")))) {
            List    list        = (ArrayList) DataManager.getInstance().getData("NoticeList");
            Map     rowData     ;
            for (int i = list.size()-1; i >= 0; i--) {
                //행추가
                ll = (LinearLayout) inflater.inflate(layout_notice_item, layout_notive_cont, true);
                ll = (LinearLayout) ll.getChildAt(ll.getChildCount()-1);
                //데이터 입력
                rowData = (Map) list.get(i);
                tv = (TextView) ll.findViewById(R.id.text_notice_time);
                tv.setText((String) rowData.get("time"));
                tv = (TextView) ll.findViewById(R.id.text_notice_cont);
                tv.setText((String) rowData.get("msg"));

                if (Constants.DefineValue.SUCCESS.equals(rowData.get("newYn"))) {
                    tv.setTextColor(Color.parseColor("#818FF7"));
                    rowData.put("newYn", "N");
                    list.set(i, rowData);
                }
            }
            DataManager.getInstance().setData("NoticeList", list);
        } else {
            ll = (LinearLayout) inflater.inflate(layout_notice_item, layout_notive_cont, true);
            ll = (LinearLayout) ll.getChildAt(ll.getChildCount()-1);
            tv = (TextView) ll.findViewById(R.id.text_notice_cont);
            tv.setText("알림 내역이 없습니다.");
            tv.setTextColor(Color.parseColor("#D9D9D9"));
        }
    }
}
