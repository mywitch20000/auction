package com.doollee.auction.activity;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.Manifest;
import android.content.Intent;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.Settings;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.doollee.auction.BuildConfig;
import com.doollee.auction.R;
import com.doollee.auction.common.AlertDialog;
import com.doollee.auction.common.Constants;
import com.doollee.auction.utils.ComUtils;
import com.gun0912.tedpermission.PermissionListener;
import com.gun0912.tedpermission.TedPermission;

import java.util.List;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

public class IntroActivity extends AppCompatActivity {
    private ProgressBar mProgressBar;
    private TextView    mTextStatusIntro;

    private boolean     mbCheckPermissionOk;
    private boolean     mbIsFront;

    private AlertDialog mAlertAirplane;
    private AlertDialog mAlertWifi;


    // 실행 단계별 메세지 배열
    String[] levelMsg = new String[]{
              "앱 실행 중 입니다"
            , "권한체크 중 입니다"
            , "시스템 설정 확인 중 입니다"
            , "네트워크 접속 확인 중 입니다"
            , "앱 실행 완료 중 입니다"
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_intro);

        //*** 강제 종료시 Event발생 위해 호출
        //startService(new Intent(this, ForecdTerminationService.class));

        mProgressBar = findViewById(R.id.progress_intro);
        mProgressBar.setProgress(0);
        mProgressBar.setMax(levelMsg.length);

        mTextStatusIntro = findViewById(R.id.text_status_intro);

        WifiManager.WifiLock wifiLock ;
        WifiManager wifiManager = (WifiManager) this.getApplicationContext().getSystemService(WIFI_SERVICE);
        wifiLock = wifiManager.createWifiLock("wifilock");
        wifiLock.setReferenceCounted(true);
        wifiLock.acquire();

        initUX();
    }

    @Override
    protected void onResume() {
        super.onResume();

        mbIsFront = true;
    }

    @Override
    protected void onPause() {
        super.onPause();

        mbIsFront = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        hideAlertAirplane();
        hideAlertWifi();
    }

    /**
     * 필요 퍼미션 설정 처리
     *
     */
    @RequiresApi(api = Build.VERSION_CODES.Q)
    private void getPermission() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            final PermissionListener permissionlistener = new PermissionListener() {
                @Override
                public void onPermissionGranted() {
                    mbCheckPermissionOk = true;
                    //hideAlertAirplane();
                }

                @Override
                public void onPermissionDenied(List<String> deniedPermissions) {
                    Toast.makeText(IntroActivity.this, "권한이 '항상허용'으로 허용되야 앱 사용이 가능합니다.", Toast.LENGTH_SHORT).show();
                    //showAlertAirplane();
                }
            };

            TedPermission.Builder tedPermissionBuilder =
                    TedPermission.with(IntroActivity.this)
                            .setPermissionListener(permissionlistener)
                            .setPermissions(Manifest.permission.READ_PHONE_STATE, Manifest.permission.READ_CALL_LOG,
                                    Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_BACKGROUND_LOCATION,
                                    Manifest.permission.ACCESS_COARSE_LOCATION);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
                tedPermissionBuilder.setDeniedMessage("[ 설정 > 권한 ] 에서 거부된 권한을 모두 허용해 주세요.")
                        .setGotoSettingButton(true);
            }

            tedPermissionBuilder.check();
        }
    }

    private void initUX() {

        ImageView imageIntro = findViewById(R.id.image_intro);
        Glide.with(this).load(R.drawable.intro).into(imageIntro);

        // 실행간격 지정
        final int sleepSec = 2;
        // 주기적인 작업을 위한
        final ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);

        exec.scheduleAtFixedRate(new Runnable() {
            // 작업단계 지정
            int glevel = 1;

            // 앱실행 (권한체크 반복)
            @RequiresApi(api = Build.VERSION_CODES.Q)
            public void run() {
                if (!mbIsFront)
                    return;

                try {
                    if (glevel <= mProgressBar.getMax()) {
                        // 화면에 진행상태 표시
                        mTextStatusIntro.setText(levelMsg[glevel - 1]);
                        // 프로그래스바 표시
                        mProgressBar.setProgress(glevel);
                    }

                    // 최종단계까지 진행 시 반복 종료/화면 이동
                    if (glevel == mProgressBar.getMax()) {
                        // 반복 종료
                        exec.shutdown();

                        Intent intent = new Intent(IntroActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }

                    if (glevel == 1) {
                        // 단계가 1인 경우 진행중 상태로 pass
                        glevel++;
                    } else {
                        if (glevel == 2) {
                            if (mbCheckPermissionOk) {
                                // 권한승인이 확인되었으면 통과
                                glevel++;
                            } else {
                                // 권한승인하지 않았으면 권한요청
                                getPermission();
                            }
                        } else {
                            if (glevel >= 4) {
                                if (BuildConfig.CHECK_WIFI) {
                                    if (ComUtils.checkWifi(IntroActivity.this)) {
                                        if (glevel >= 4) {
                                            glevel++;
                                            return;
                                        }
                                    } else {
                                        Handler handler = new Handler(Looper.getMainLooper());
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                showAlertWifi();
                                            }
                                        }, 0);
                                    }
                                } else {
                                    if (glevel == 4) {
                                        glevel++;
                                        return;
                                    }
                                }
                            }

                            if (glevel >= 3) {
                                if (BuildConfig.CHECK_AIRPLANE_MODE) {
                                    if (ComUtils.checkAirplane(IntroActivity.this)) {
                                        if (glevel == 3) {
                                            glevel++;
                                        }
                                    } else {
                                        Handler handler = new Handler(Looper.getMainLooper());
                                        handler.postDelayed(new Runnable() {
                                            @Override
                                            public void run() {
                                                showAlertAirplane();
                                            }
                                        }, 0);
                                    }
                                } else {
                                    if (glevel == 3) {
                                        glevel++;
                                    }
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, 0, sleepSec, TimeUnit.SECONDS);
    }

    private void showAlertAirplane() {
        if (mAlertAirplane != null && mAlertAirplane.isShowing()) {
            return;
        }

        mAlertAirplane = new AlertDialog(this);

        mAlertAirplane.msg = getResources().getString(R.string.go_set_airplane);
        mAlertAirplane.mNBtText = getResources().getString(R.string.finish);
        mAlertAirplane.mPBtText = getResources().getString(R.string.confirm);

        mAlertAirplane.mNListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.finishAffinity(IntroActivity.this);
            }
        };

        mAlertAirplane.mPListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideAlertAirplane();
                Intent airpIntent = new Intent(Settings.ACTION_AIRPLANE_MODE_SETTINGS);
                startActivity(airpIntent);
            }
        };

        if(!isFinishing()) {
            mAlertAirplane.show();
        }
    }

    private void hideAlertAirplane() {
        if (mAlertAirplane != null && mAlertAirplane.isShowing()) {
            mAlertAirplane.dismiss();
        }
    }

    private void showAlertWifi() {
        if (mAlertWifi != null && mAlertWifi.isShowing()) {
            return;
        }

        mAlertWifi = new AlertDialog(this);

        mAlertWifi.msg = getString(R.string.no_connect_ap) + "\n"
                + "확인 버튼을 누른 후 표시되는 목록에서\n"
                + " [ " + Constants.WIFI_SSID + " ] 으로 접속해 주세요.";
        mAlertWifi.mNBtText = getString(R.string.finish);
        mAlertWifi.mPBtText =  getString(R.string.confirm);

        mAlertWifi.mNListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ActivityCompat.finishAffinity(IntroActivity.this);
            }
        };

        mAlertWifi.mPListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent wifiIntent = new Intent(Settings.ACTION_WIFI_SETTINGS);
                startActivity(wifiIntent);
            }
        };

        if(!isFinishing()) {
            mAlertWifi.show();
        }
    }

    private void hideAlertWifi() {
        if (mAlertWifi != null && mAlertWifi.isShowing()) {
            mAlertWifi.dismiss();
        }
    }
}