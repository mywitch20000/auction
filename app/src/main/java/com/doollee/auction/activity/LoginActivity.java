package com.doollee.auction.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Switch;

import com.doollee.auction.ACApplication;
import com.doollee.auction.R;
import com.doollee.auction.common.Constants;
import com.doollee.auction.common.PreferenceManager;
import com.doollee.auction.common.net.SocketRecvTask;
import com.doollee.auction.common.net.SocketRecver;
import com.doollee.auction.common.net.SocketSendTask;
import com.doollee.auction.common.net.SocketSender;
import com.doollee.auction.datatype.LoginUserInfo;
import com.doollee.auction.manager.DataManager;
import com.doollee.auction.ui.BaseActivity;
import com.doollee.auction.utils.ComUtils;
import com.doollee.auction.utils.DialogUtils;
import com.doollee.auction.utils.JsonUtils;
import com.doollee.auction.utils.Logs;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;

public class LoginActivity extends BaseActivity {
    private LinearLayout layoutServerIP;
    private LinearLayout layoutLoginRequest;
    private LinearLayout layoutLoginAuth;
    private RelativeLayout layoutLoginInfoSave;
    private EditText editIpAddr;
    private EditText editLoginId;
    private EditText editAuthNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_login);

        initUX();
    }

    @Override
    public void onDestroy() {
        Log.d("LoginReqActivity", "onDestroy");

        //SvrSocket.close("ALL"); //소켓모두닫기

        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        DialogUtils.alert(LoginActivity.this, getString(R.string.confirm), getString(R.string.cancel), getString(R.string.confirm_app_exit),
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ACApplication applicationClass = (ACApplication) getApplicationContext();
                        if (applicationClass != null)
                            applicationClass.allActivityFinish(true);
                        else
                            finish();
                    }
                }
                , new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                    }
                }
        );
    }

    private void initUX() {
        layoutServerIP = findViewById(R.id.layout_server_ip);
        layoutLoginRequest = findViewById(R.id.layout_login_requst);
        layoutLoginAuth = findViewById(R.id.layout_login_auth);
        layoutLoginInfoSave = findViewById(R.id.layout_login_info_save);
        editIpAddr = findViewById(R.id.edit_ip_addr);
        editLoginId = findViewById(R.id.edit_login_id);
        editAuthNumber = findViewById(R.id.edit_auth_number);
        Button btnIpAddr = findViewById(R.id.btn_ip_addr);
        Button btnLoginRequest = findViewById(R.id.btn_login_requst);
        Button btnIpReturn = findViewById(R.id.btn_ip_return);
        Button btnCheckAuth = findViewById(R.id.btn_check_auth);
        Button btnIdReturn = findViewById(R.id.btn_id_return);
        CheckBox checkboxLoginInfoSave = findViewById(R.id.checkBox_login_info_save);

        layoutServerIP.setAlpha(1);                   // Wifi 입력 레이아웃 투명도 초기화
        layoutLoginRequest.setVisibility(View.GONE);  // 요청레이아웃 안보이게
        layoutLoginAuth.setVisibility(View.GONE);     // 인증레이아웃 안보이게
        layoutLoginInfoSave.setVisibility(View.GONE); // 로그인정보저장 레이아웃 안보이게

        // 로컬에 저장된 값이 있으면 넣기
        boolean b_LoginInfoURL = !TextUtils.isEmpty(PreferenceManager.getString(LoginActivity.this, "LoginInfoURL"));
        boolean b_LoginInfoID  = !TextUtils.isEmpty(PreferenceManager.getString(LoginActivity.this, "LoginInfoID"));
        if (b_LoginInfoURL) {
            editIpAddr.setText(PreferenceManager.getString(LoginActivity.this, "LoginInfoURL"));
        }
        if (b_LoginInfoID) {
            editLoginId.setText(PreferenceManager.getString(LoginActivity.this, "LoginInfoID"));
        }
        if (b_LoginInfoURL && b_LoginInfoID) {
            checkboxLoginInfoSave.setChecked(true);
        }

        // 로그인정보저장 Layout 클릭해서 변경
        layoutLoginInfoSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                checkboxLoginInfoSave.setChecked(!checkboxLoginInfoSave.isChecked());
            }
        });

        InputFilter[] filters = new InputFilter[1];
        filters[0] = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                if (end > start) {
                    String destTxt = dest.toString();
                    String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
                    if (!resultingTxt.matches("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                        return "";
                    } else {
                        String[] splits = resultingTxt.split("\\.");
                        for (String split : splits) {
                            if (Integer.parseInt(split) > 255) {
                                return "";
                            }
                        }
                    }
                }
                return null;
            }
        };
        editIpAddr.setFilters(filters);

        btnIpAddr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (String.valueOf(editIpAddr.getText()).length() == 0) {
                    showCustomToast("접속할 IP 주소를 입력해주세요.");
                    return;
                }

                Constants.URL_DOMAIN = String.valueOf(editIpAddr.getText());
                layoutServerIP.setAlpha(0.5f);  // ip입력 레이아웃 투명하게
                editIpAddr.setEnabled(false);
                layoutLoginRequest.setVisibility(View.VISIBLE); // 요청레이아웃 표시
                layoutLoginRequest.setAlpha(1.0f);
                editLoginId.setEnabled(true);

                try {
                    // ip 주소 재입력시 접속되어 있던 소켓 모두 close 처리
                    if (DataManager.getInstance().getSocketMap().size() > 0) {
                        Object[] x = DataManager.getInstance().getSocketMap().keySet().toArray();
                        Socket socket;
                        for (int i = 0; i < DataManager.getInstance().getSocketMap().size(); i++) {
                            socket = (Socket) DataManager.getInstance().getSocketMap().get(x[i]);
                            if (socket != null)
                                socket.close();
                        }
                        DataManager.getInstance().clearSocketMap();
                    }
                } catch (Exception e) {
                    e.getStackTrace();
                }
            }
        });

        // 로그인 요청 버튼 클릭
        btnLoginRequest.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SocketSendTask.OnSocketCallback socketCallback = new SocketSendTask.OnSocketCallback() {
                    @Override
                    public void onConnect() {

                    }

                    @Override
                    public void onFail(String msg) {
                        dismissProgressDialog();

                        showCustomToast(getString(R.string.msg_no_response));
                    }

                    @Override
                    public void onSuccess(String result) {
                        dismissProgressDialog();

                        if (!TextUtils.isEmpty(result)) {
                            try {
                                JSONObject json = new JSONObject(result);
                                Map<String, Object> req = JsonUtils.jsonToMap(json);
                                Log.d("", req.toString());
                                String authNum = (String) req.get("VALIDATE_BID_NO");
                                if (!TextUtils.isEmpty(authNum)) {
                                    editAuthNumber.setText(authNum);

                                    layoutLoginRequest.setAlpha(0.5f);    // 중도매인번호입력 레이아웃 투명하게
                                    editLoginId.setEnabled(false);
                                    layoutLoginAuth.setVisibility(View.VISIBLE); // 인증레이아웃 표시
                                    layoutLoginInfoSave.setVisibility(View.VISIBLE); // 로그인정보저장 레이아웃 표시

                                } else {
                                    showCustomToast("중도매인코드를 확인해주세요");
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            showCustomToast(getString(R.string.msg_no_response));
                        }
                    }
                };

                String biderId  = editLoginId.getText().toString();
                if (TextUtils.isEmpty(biderId))
                    biderId = "";

                String phoneNum = ComUtils.getPhoneNumber(LoginActivity.this);
                String uuid = ComUtils.getDeviceId(LoginActivity.this);

                // 요청일 조회
                Date date = new Date(System.currentTimeMillis());
                String sdfNow = new SimpleDateFormat("yyyyMMddHHmmssSS", Locale.getDefault()).format(date);

                // 송신데이터 생성
                Map<String, Object> req = new ArrayMap<>();
                req.put("SEND_GBN", "M001");
                req.put("BIDER_ID", biderId);
                req.put("BIDER_HP", phoneNum);
                req.put("VALIDATE_ID", uuid);
                req.put("REQ_TIMESTAMP", sdfNow);

                // 송신
                JSONObject json = JsonUtils.mapToJson(req);

                showProgressDialog();

                Socket socket = DataManager.getInstance().getSocketMapData(10001);
                SocketSender.getInstance().sendMsg(socket, "10001", json.toString(), socketCallback);
            }
        });

        // IP 주소 다시 입력
        btnIpReturn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                layoutServerIP.setAlpha(1);
                editIpAddr.setEnabled(true);
                layoutLoginRequest.setVisibility(View.GONE); //요청레이아웃 숨기기
                layoutLoginAuth.setVisibility(View.GONE); //인증레이아웃 숨기기
            }
        });

        // 로그인 인증 버튼 클릭
        btnCheckAuth.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                SocketSendTask.OnSocketCallback socketCallback = new SocketSendTask.OnSocketCallback() {
                    @Override
                    public void onConnect() {
                    }

                    @Override
                    public void onFail(String msg) {
                        dismissProgressDialog();
                        showCustomToast(getString(R.string.msg_no_response));
                    }

                    @Override
                    public void onSuccess(String result) {
                        dismissProgressDialog();

                        if (!TextUtils.isEmpty(result)) {
                            try {
                                JSONObject json = new JSONObject(result);
                                Map<String, Object> req = JsonUtils.jsonToMap(json);
                                Logs.d(req.toString());

                                String isOK = (String) req.get("SUCCESS_YN");
                                if (!TextUtils.isEmpty(isOK) && Constants.DefineValue.YES.equalsIgnoreCase(isOK)) {
                                    String biderId  = String.valueOf(editLoginId.getText());
                                    if (TextUtils.isEmpty(biderId))
                                        biderId = "";

                                    String phoneNum = ComUtils.getPhoneNumber(LoginActivity.this);
                                    String uuid = ComUtils.getDeviceId(LoginActivity.this);

                                    String validateBiderNo = editAuthNumber.getText().toString();
                                    if (TextUtils.isEmpty(validateBiderNo))
                                        validateBiderNo = "";

                                    Map<String, Object> map = new ArrayMap<>();
                                    map.put("bider_id", biderId);
                                    map.put("bider_hp", phoneNum);
                                    map.put("validate_id", uuid);
                                    map.put("validate_bid_no", validateBiderNo); // 인증번호(고유키)

                                    DataManager.getInstance().setData("user_info", map);

                                    LoginUserInfo.getInstance().setBiderId(biderId);
                                    LoginUserInfo.getInstance().setValidateBiderNo(validateBiderNo);

                                    if (checkboxLoginInfoSave.isChecked()) {
                                        PreferenceManager.setString(LoginActivity.this, "LoginInfoURL", String.valueOf(editIpAddr.getText()));
                                        PreferenceManager.setString(LoginActivity.this, "LoginInfoID" , String.valueOf(editLoginId.getText()));
                                    } else {
                                        PreferenceManager.setString(LoginActivity.this, "LoginInfoURL", "");
                                        PreferenceManager.setString(LoginActivity.this, "LoginInfoID" , "");
                                    }

                                    registerRecv();

                                    // 메인화면이동
                                    /*Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                    startActivity(intent);*/
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            showCustomToast("인증키를 확인해주세요");
                        }
                    }
                };

                String biderId  = editLoginId.getText().toString();
                if (TextUtils.isEmpty(biderId))
                    biderId = "";

                String phoneNum = ComUtils.getPhoneNumber(LoginActivity.this);
                String uuid = ComUtils.getDeviceId(LoginActivity.this);

                // 요청일 조회
                Date date = new Date(System.currentTimeMillis());
                String sdfNow = new SimpleDateFormat("yyyyMMddHHmmssSS", Locale.getDefault()).format(date);

                Map<String, Object> req = new ArrayMap<>();
                req.put("SEND_GBN", "M002");
                req.put("BIDER_ID", biderId);
                req.put("BIDER_HP", phoneNum);
                req.put("VALIDATE_ID", uuid);
                req.put("REQ_TIMESTAMP", sdfNow);
                req.put("VALIDATE_BID_NO", editAuthNumber.getText().toString());

                JSONObject json = JsonUtils.mapToJson(req);

                showProgressDialog();

                Socket socket = DataManager.getInstance().getSocketMapData(10001);
                SocketSender.getInstance().sendMsg(socket, "10001", json.toString(), socketCallback);
            }
        });

        // 중도매인코드 다시입력
        btnIdReturn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                layoutLoginRequest.setAlpha(1);
                editLoginId.setEnabled(true);
                layoutLoginAuth.setVisibility(View.GONE);
                layoutLoginInfoSave.setVisibility(View.GONE);
            }
        });
    }

    private void registerRecv() {
        SocketRecvTask.OnSocketCallback socketCallback = new SocketRecvTask.OnSocketCallback() {
            @Override
            public void onConnect() {
                //sendRegisterRecv();
            }

            @Override
            public void onReceive(String msg) {
                Logs.d("====== onReceive : " + msg);
                if (TextUtils.isEmpty(msg))
                    return;

                try {
                    JSONObject json = new JSONObject(msg);
                    Map<String, Object> req = JsonUtils.jsonToMap(json);
                    Log.d("", req.toString());

                    //String SEND_GBN = (String) req.get("SEND_GBN");
                    String SUCCESS_YN = (String) req.get("SUCCESS_YN");
                    if (//"S301".equalsIgnoreCase(SEND_GBN) &&
                        Constants.DefineValue.YES.equalsIgnoreCase(SUCCESS_YN)) {
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void onFail(String msg) {
                Logs.d("====== onFail : " + msg);
            }

            @Override
            public void onDisconnect() {

            }
        };

        String phoneNum = ComUtils.getPhoneNumber(LoginActivity.this);

        // 폰 고유번호 조회
        final String uuid = ComUtils.getDeviceId(LoginActivity.this);

        // 요청일 조회
        Date date = new Date(System.currentTimeMillis());
        final String sdfNow = new SimpleDateFormat("yyyyMMddHHmmssSS", Locale.getDefault()).format(date);

        Map<String, Object> req = new ArrayMap<>();
        req.put("SEND_GBN"       , "M301");
        req.put("BIDER_ID"       , editLoginId.getText().toString());
        req.put("BIDER_HP"       , phoneNum);
        req.put("VALIDATE_ID"    , uuid);
        req.put("REQ_TIMESTAMP"  , sdfNow);
        req.put("VALIDATE_BID_NO", editAuthNumber.getText().toString());

        JSONObject json = JsonUtils.mapToJson(req);

        Socket socket = DataManager.getInstance().getSocketMapData(10201);
        SocketRecver.getInstance().recvMsg(socket, "10201", json.toString(), socketCallback);
    }
}