package com.doollee.auction.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;;

import androidx.viewpager.widget.ViewPager;

import com.doollee.auction.R;
import com.doollee.auction.adapter.InquiryPriceAdapter;
import com.doollee.auction.fragment.FragmentCategory;
import com.doollee.auction.fragment.FragmentMarket;
import com.doollee.auction.ui.BaseActivity;
import com.doollee.auction.ui.OnSingleClickListener;
import com.google.android.material.tabs.TabLayout;

public class InquiryPriceBidActivity extends BaseActivity {
    private TabLayout       mTabLayoutInquiry;
    private ViewPager       mViewPagerInquiry;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_inquiry_price);

        initUX();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void initUX() {
        TextView btnBack =  findViewById(R.id.btn_back_inquiry_price_bid);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
            }
        });

        ImageView btnSettings = findViewById(R.id.image_settings);
        btnSettings.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                Intent intent = new Intent(InquiryPriceBidActivity.this, SettingsActivity.class);
                startActivity(intent);
                overridePendingTransition( R.anim.menu_right_in, R.anim.layout_stay);
            }
        });

        mTabLayoutInquiry = findViewById(R.id.tablayout_inquiry);

        mTabLayoutInquiry.addTab(mTabLayoutInquiry.newTab().setText("품목조회"));
        mTabLayoutInquiry.addTab(mTabLayoutInquiry.newTab().setText("시장조회"));

        mViewPagerInquiry = findViewById(R.id.viewpager_inquiry);
        InquiryPriceAdapter fragmentAdapter = new InquiryPriceAdapter(getSupportFragmentManager());
        mViewPagerInquiry.setAdapter(fragmentAdapter);

        mViewPagerInquiry.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(mTabLayoutInquiry));
        mTabLayoutInquiry.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPagerInquiry.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        mViewPagerInquiry.setClipToPadding(false);
        for (int index = 0; index < 2; index++) {
            if (index == 0) {
                FragmentCategory fragment = new FragmentCategory();
                fragmentAdapter.addItem(fragment);
            } else if (index == 1) {
                FragmentMarket fragment = new FragmentMarket();
                fragmentAdapter.addItem(fragment);
            }
        }
        fragmentAdapter.notifyDataSetChanged();
    }
}

