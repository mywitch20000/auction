package com.doollee.auction.activity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.doollee.auction.R;
import com.doollee.auction.adapter.PriceBidAdapter;
import com.doollee.auction.common.Constants;
import com.doollee.auction.common.net.HttpSenderTask;
import com.doollee.auction.common.net.HttpUtils;
import com.doollee.auction.datatype.PriceBidInfo;
import com.doollee.auction.ui.BaseSocketActivity;
import com.doollee.auction.utils.Logs;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * Class: PriceBidActivity
 * Created by wizard on 2021.02.15
 *
 * Description: 도매가격조회 결과 activity
 */

public class PriceBidActivity extends BaseSocketActivity {
    private RecyclerView      mRecyclerPriceBid;
    private TextView          mTextPos;

    private int                      mPagePos = 2;
    private String                   mInquiryType = "";
    private String                   mDate = "";
    private String                   mSubClass = "";
    private String                   mMarketCode = "";
    private int                      mTotalCount = 1;
    private ArrayList<PriceBidInfo>  mListPriceBidInfo;
    private PriceBidAdapter          mPriceBidAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_price_bid);

        getExtra();
        initUX();
        setPriceBid();
    }

    private void getExtra() {
        Intent intent = getIntent();

        if (intent.hasExtra(Constants.PriceBid.INQUIRY_TYPE)) {
            mInquiryType = intent.getStringExtra(Constants.PriceBid.INQUIRY_TYPE);
        }

        if (intent.hasExtra(Constants.PriceBid.DATE)) {
            mDate = intent.getStringExtra(Constants.PriceBid.DATE);
        }

        if (intent.hasExtra(Constants.PriceBid.SUB_CATEGORY_CODE)) {
            mSubClass = intent.getStringExtra(Constants.PriceBid.SUB_CATEGORY_CODE);
        }

        if (intent.hasExtra(Constants.PriceBid.MAIN_MARKET_CODE)) {
            mMarketCode = intent.getStringExtra(Constants.PriceBid.MAIN_MARKET_CODE);
        }

        if (intent.hasExtra(Constants.PriceBid.TOTAL_PRICE_BID)) {
            mTotalCount = intent.getIntExtra(Constants.PriceBid.TOTAL_PRICE_BID, 1);
        }

        if (intent.hasExtra(Constants.PriceBid.LIST_PRICE_BID)) {
            mListPriceBidInfo = (ArrayList<PriceBidInfo>) intent.getSerializableExtra(Constants.PriceBid.LIST_PRICE_BID);
        }
    }

    private void initUX() {
        TextView btnBack = (TextView) findViewById(R.id.btn_back);
        mRecyclerPriceBid = (RecyclerView) findViewById(R.id.recycler_price_bid);
        mTextPos = (TextView) findViewById(R.id.text_pos_price_bid);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        mRecyclerPriceBid.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

                if (!recyclerView.canScrollVertically(1) && newState == RecyclerView.SCROLL_STATE_IDLE) {
                    if (mListPriceBidInfo.size() >= mTotalCount)
                        return;

                    requestPrice();
                }
            }
        });
    }

    private void setPriceBid() {
        if (mPriceBidAdapter == null) {
            mPriceBidAdapter = new PriceBidAdapter(this, mListPriceBidInfo, new PriceBidAdapter.onListener() {
                @Override
                public void onClickListener(int position) {

                }
            });
            mRecyclerPriceBid.setAdapter(mPriceBidAdapter);

            LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            mRecyclerPriceBid.setLayoutManager(llm);
            mRecyclerPriceBid.setHasFixedSize(true);
        } else {
            mPriceBidAdapter.setPriceBid(mListPriceBidInfo);
        }

        mTextPos.setText(mListPriceBidInfo.size() + " / " + mTotalCount);
    }

    private void requestPrice() {
        StringBuilder urlBuilder = new StringBuilder();

        if (Constants.PriceBid.INQUIRY_TYPE_CATEGORY.equals(mInquiryType)) {
            urlBuilder = new StringBuilder(Constants.URL_PRICE_OPEN_API + Constants.PATH_PRICE_CATEGORY);
            try {
                urlBuilder.append("?").append(URLEncoder.encode("ServiceKey", "UTF-8"));
                urlBuilder.append("=");
                urlBuilder.append(Constants.KEY_OPEN_API);
                urlBuilder.append("&");
                urlBuilder.append("pageNo");
                urlBuilder.append("=");
                urlBuilder.append(mPagePos);
                urlBuilder.append("&");
                urlBuilder.append("numOfRows");
                urlBuilder.append("=");
                urlBuilder.append(Constants.NUM_OF_ROWS);
                urlBuilder.append("&");
                urlBuilder.append("delngDe");
                urlBuilder.append("=");
                urlBuilder.append(mDate);
                urlBuilder.append("&");
                urlBuilder.append("prdlstCd");
                urlBuilder.append("=");
                urlBuilder.append(mSubClass);
                if (!TextUtils.isEmpty(mMarketCode) && getString(R.string.all).equals(mMarketCode)) {
                    urlBuilder.append("&");
                    urlBuilder.append("whsalCd");
                    urlBuilder.append("=");
                    urlBuilder.append(mMarketCode);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        } else if (Constants.PriceBid.INQUIRY_TYPE_MARKET.equals(mInquiryType)) {
            urlBuilder = new StringBuilder(Constants.URL_PRICE_OPEN_API + Constants.PATH_PRICE_MARKET);
            try {
                urlBuilder.append("?").append(URLEncoder.encode("ServiceKey", "UTF-8"));
                urlBuilder.append("=");
                urlBuilder.append(Constants.KEY_OPEN_API);
                urlBuilder.append("&");
                urlBuilder.append("pageNo");
                urlBuilder.append("=");
                urlBuilder.append(mPagePos);
                urlBuilder.append("&");
                urlBuilder.append("numOfRows");
                urlBuilder.append("=");
                urlBuilder.append(Constants.NUM_OF_ROWS);
                urlBuilder.append("&");
                urlBuilder.append("delngDe");
                urlBuilder.append("=");
                urlBuilder.append(mDate);
                urlBuilder.append("&");
                urlBuilder.append("whsalCd");
                urlBuilder.append("=");
                urlBuilder.append(mMarketCode);
                if (!TextUtils.isEmpty(mSubClass)) {
                    urlBuilder.append("&");
                    urlBuilder.append("prdlstCd");
                    urlBuilder.append("=");
                    urlBuilder.append(mSubClass);
                }
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            }
        }

        showProgressDialog();

        Map<String, Object> param = new HashMap<>();
        HttpUtils.sendHttpTask(urlBuilder.toString(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                try {
                    JSONObject jsonObject = new JSONObject(ret);
                    JSONObject objectResponse = jsonObject.getJSONObject("response");
                    JSONObject objectHeader = objectResponse.getJSONObject("header");

                    String resultCode = objectHeader.getString("resultCode");
                    if (Constants.DefineValue.SUCCESS_CODE.equals(resultCode)) {
                        JSONObject objectBody = objectResponse.getJSONObject("body");
                        JSONObject objectItems = objectBody.getJSONObject("items");

                        int pageNo = objectBody.getInt("pageNo");
                        int numOfRows = objectBody.getInt("numOfRows");

                        if (pageNo == 1 && numOfRows == 1) {
                            JSONObject objectItem = objectItems.getJSONObject("item");

                            String delngDe = objectItem.getString("delngDe");
                            PriceBidInfo priceBidInfo = setBiddingItem(objectItem);
                            mListPriceBidInfo.add(priceBidInfo);

                        } else {
                            JSONArray arrayItem = objectItems.getJSONArray("item");
                            if (arrayItem.length() <= 0) {
                                return;
                            }

                            for (int index = 0; index < arrayItem.length(); index++) {
                                JSONObject objectItem = (JSONObject) arrayItem.get(index);
                                PriceBidInfo priceBidInfo = setBiddingItem(objectItem);
                                mListPriceBidInfo.add(priceBidInfo);
                            }

                            setPriceBid();
                            mPagePos = pageNo + 1;
                        }
                    }
                } catch (JSONException e) {
                    Logs.d(e.getMessage());
                }
            }
        });
    }

    private PriceBidInfo setBiddingItem(JSONObject objectItem) {
        PriceBidInfo info = new PriceBidInfo();

        try {
            if (objectItem.has("delngDe")) {
                String delngDe = objectItem.getString("delngDe");
                info.setDelngDe(delngDe);
            }

            if (objectItem.has("sbidTime")) {
                String sbidTime = objectItem.getString("sbidTime");
                info.setSbidTime(sbidTime);
            }

            if (objectItem.has("aucSeCode")) {
                int aucSeCode = objectItem.getInt("aucSeCode");
                info.setAucSeCode(String.valueOf(aucSeCode));
            }

            if (objectItem.has("aucSeNm")) {
                String aucSeNm = objectItem.getString("aucSeNm");
                info.setAucSeCode(aucSeNm);
            }

            if (objectItem.has("whsalMrktNewCode")) {
                String whsalMrktNewCode = objectItem.getString("whsalMrktNewCode");
                info.setWhsalMrktNewCode(whsalMrktNewCode);
            }

            if (objectItem.has("whsalMrktNewNm")) {
                String whsalMrktNewNm = objectItem.getString("whsalMrktNewNm");
                info.setWhsalMrktNewNm(whsalMrktNewNm);
            }

            if (objectItem.has("cprInsttNewCode")) {
                String cprInsttNewCode = objectItem.getString("cprInsttNewCode");
                info.setCprInsttNewCode(cprInsttNewCode);
            }

            if (objectItem.has("insttNewNm")) {
                String insttNewNm = objectItem.getString("insttNewNm");
                info.setInsttNewNm(insttNewNm);
            }

            if (objectItem.has("ledgNo")) {
                String ledgNo = objectItem.getString("ledgNo");
                info.setLedgNo(ledgNo);
            }

            if (objectItem.has("sleSeqn")) {
                String sleSeqn = objectItem.getString("sleSeqn");
                info.setSleSeqn(sleSeqn);
            }

            if (objectItem.has("stdPrdlstNewCode")) {
                String stdPrdlstNewCode = objectItem.getString("stdPrdlstNewCode");
                info.setStdPrdlstNewCode(stdPrdlstNewCode);
            }

            if (objectItem.has("stdPrdlstNewNm")) {
                String stdPrdlstNewNm = objectItem.getString("stdPrdlstNewNm");
                info.setStdPrdlstNewNm(stdPrdlstNewNm);
            }

            if (objectItem.has("stdSpciesNewCode")) {
                String stdSpciesNewCode = objectItem.getString("stdSpciesNewCode");
                info.setStdSpciesNewCode(stdSpciesNewCode);
            }

            if (objectItem.has("stdSpciesNewNm")) {
                String stdSpciesNewNm = objectItem.getString("stdSpciesNewNm");
                info.setStdSpciesNewNm(stdSpciesNewNm);
            }

            if (objectItem.has("delngPrut")) {
                String delngPrut = objectItem.getString("delngPrut");
                info.setDelngPrut(delngPrut);
            }

            if (objectItem.has("stdUnitNewCode")) {
                String stdUnitNewCode = objectItem.getString("stdUnitNewCode");
                info.setStdUnitNewCode(stdUnitNewCode);
            }

            if (objectItem.has("stdUnitNewNm")) {
                String stdUnitNewNm = objectItem.getString("stdUnitNewNm");
                info.setStdUnitNewNm(stdUnitNewNm);
            }

            if (objectItem.has("stdFrmlcNewCode")) {
                String stdFrmlcNewCode = objectItem.getString("stdFrmlcNewCode");
                info.setStdFrmlcNewCode(stdFrmlcNewCode);
            }

            if (objectItem.has("stdFrmlcNewNm")) {
                String stdFrmlcNewNm = objectItem.getString("stdFrmlcNewNm");
                info.setStdFrmlcNewNm(stdFrmlcNewNm);
            }

            if (objectItem.has("sbidPric")) {
                String sbidPric = objectItem.getString("sbidPric");
                info.setSbidPric(sbidPric);
            }

            if (objectItem.has("delngQy")) {
                String delngQy = objectItem.getString("delngQy");
                info.setDelngQy(delngQy);
            }

            if (objectItem.has("cprMtcNm")) {
                String cprMtcNm = objectItem.getString("cprMtcNm");
                info.setCprMtcNm(cprMtcNm);
            }

            /*String catgoryNewCode = objectItem.getString("catgoryNewCode");
            String catgoryNewNm = objectItem.getString("catgoryNewNm");
            String stdMgNewCode = objectItem.getString("stdMgNewCode");
            String stdMgNewNm = objectItem.getString("stdMgNewNm");
            String stdQlityNewCode = objectItem.getString("stdQlityNewCode");
            String stdQlityNewNm = objectItem.getString("stdQlityNewNm");
            String cprUsePrdlstCode = objectItem.getString("cprUsePrdlstCode");
            String cprUsePrdlstNm = objectItem.getString("cprUsePrdlstNm");
            String shipmntSeCode = objectItem.getString("shipmntSeCode");
            String shipmntSeNm = objectItem.getString("shipmntSeNm");
            String stdMtcNewCode = objectItem.getString("stdMtcNewCode");
            String stdMtcNewNm = objectItem.getString("stdMtcNewNm");*/

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return info;
    }
}
