package com.doollee.auction.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doollee.auction.R;
import com.doollee.auction.adapter.AreaAdapter;
import com.doollee.auction.datatype.AreaInfo;
import com.doollee.auction.ui.BaseActivity;
import com.doollee.auction.ui.OnSingleClickListener;
import com.opencsv.CSVReader;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;

public class SettingsActivity extends BaseActivity {
    private LinearLayout        mLayoutWeatherBottom;
    private TextView            mTextMapStep01;
    private TextView            mTextMapStep02;
    private TextView            mTextMapStep03;

    private final ArrayList<AreaInfo> mListArea = new ArrayList<>();
    private final ArrayList<AreaInfo> mListStep01Area = new ArrayList<>();

    private AreaAdapter         mAreaStep01Adapter;
    private AreaAdapter         mAreaStep02Adapter;
    private AreaAdapter         mAreaStep03Adapter;

    private String              mSelectedStep01Area;
    private String              mSelectedStep02Area;
    private String              mSelectedStep03Area;
    private String              mSelectedPosX;
    private String              mSelectedPosY;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_settings);

        initUX();
        setArea();
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.layout_stay, R.anim.menu_right_out);
    }

    private void initUX() {
        LinearLayout layoutWeather = findViewById(R.id.layout_map_weather);
        mLayoutWeatherBottom = findViewById(R.id.layout_map_weather_bottom);
        mTextMapStep01 = findViewById(R.id.text_map_weather_step_01);
        mTextMapStep02 = findViewById(R.id.text_map_weather_step_02);
        mTextMapStep03 = findViewById(R.id.text_map_weather_step_03);

        TextView btnBack =  findViewById(R.id.btn_back_settings);
        btnBack.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                finish();
                overridePendingTransition(R.anim.layout_stay, R.anim.menu_right_out);
            }
        });

        layoutWeather.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (mLayoutWeatherBottom.isShown())
                    mLayoutWeatherBottom.setVisibility(View.GONE);
                else
                    mLayoutWeatherBottom.setVisibility(View.VISIBLE);
            }
        });

        mTextMapStep01.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                showAreaStep01();
            }
        });

        mTextMapStep02.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                showAreaStep02();
            }
        });

        mTextMapStep03.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                showAreaStep03();
            }
        });
    }

    private void setArea() {
        try {
            InputStreamReader is = new InputStreamReader(getResources().openRawResource(R.raw.area_code_20210106), StandardCharsets.UTF_8);

            BufferedReader reader = new BufferedReader(is);
            CSVReader csvReader = new CSVReader(reader);

            String[] nextLine;
            int countLine = 0;

            String depth01 = "";

            mListArea.clear();
            mListStep01Area.clear();

            while ((nextLine = csvReader.readNext()) != null) {
                countLine++;
                if (countLine == 1)
                    continue;

                if (nextLine.length != 17)
                    continue;

                String division = nextLine[0];
                String areaCode = nextLine[1];
                String firstStep = nextLine[2];
                String secondStep = nextLine[3];
                String thirdStep = nextLine[4];
                String gridX = nextLine[5];
                String gridY = nextLine[6];
                String longitudeHour = nextLine[7];
                String longitudeMinute = nextLine[8];
                String longitudeSecond = nextLine[9];
                String latitudeHour = nextLine[10];
                String latitudeMinute = nextLine[11];
                String latitudeSecond = nextLine[12];
                String longitudeMilli = nextLine[13];
                String latitudeMilli = nextLine[14];

                AreaInfo areaInfo = new AreaInfo();
                areaInfo.setDivision(division);
                areaInfo.setAreaCode(areaCode);
                areaInfo.setFirstStep(firstStep);
                areaInfo.setSecondStep(secondStep);
                areaInfo.setThirdStep(thirdStep);
                areaInfo.setGridX(gridX);
                areaInfo.setGridY(gridY);
                areaInfo.setLongitudeHour(longitudeHour);
                areaInfo.setLongitudeMinute(longitudeMinute);
                areaInfo.setLongitudeSecond(longitudeSecond);
                areaInfo.setLatitudeHour(latitudeHour);
                areaInfo.setLatitudeMinute(latitudeMinute);
                areaInfo.setLatitudeSecond(latitudeSecond);
                areaInfo.setLongitudeMilli(longitudeMilli);
                areaInfo.setLatitudeMilli(latitudeMilli);

                mListArea.add(areaInfo);

                if (!depth01.equals(firstStep)) {
                    mListStep01Area.add(areaInfo);
                }

                depth01 = firstStep;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (mAreaStep01Adapter == null) {
            mAreaStep01Adapter = new AreaAdapter(this, mListStep01Area, 1);
        }
    }

    private void showAreaStep01() {
        DialogPlus dialog = DialogPlus.newDialog(this)
                .setAdapter(mAreaStep01Adapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        AreaInfo areaInfo = (AreaInfo) item;
                        mSelectedStep01Area = areaInfo.getFirstStep();
                        mSelectedPosX = areaInfo.getGridX();
                        mSelectedPosY = areaInfo.getGridY();
                        if (!TextUtils.isEmpty(mSelectedStep01Area))
                            mTextMapStep01.setText(mSelectedStep01Area);

                        setStep02Area();
                        mTextMapStep02.setText("");
                        mTextMapStep03.setText("");

                        dialog.dismiss();
                    }
                })
                .setHeader(R.layout.layout_main_category_header)
                .setExpanded(true)
                .create();
        dialog.show();
    }

    private void setStep02Area() {
        ArrayList<AreaInfo> listArea = new ArrayList<>();
        String depth02 = "";

        for (int index = 0; index < mListArea.size(); index++) {
            AreaInfo areaInfo = mListArea.get(index);
            String firstStep = areaInfo.getFirstStep();
            String secondStep = areaInfo.getSecondStep();

            if (TextUtils.isEmpty(firstStep) || TextUtils.isEmpty(secondStep))
                continue;

            if (firstStep.equals(mSelectedStep01Area) && !depth02.equals(secondStep))
                listArea.add(areaInfo);

            depth02 = secondStep;
        }

        mAreaStep02Adapter = new AreaAdapter(this, listArea, 2);
    }

    private void showAreaStep02() {
        DialogPlus dialog = DialogPlus.newDialog(this)
                .setAdapter(mAreaStep02Adapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        AreaInfo areaInfo = (AreaInfo) item;
                        mSelectedStep02Area = areaInfo.getSecondStep();
                        mSelectedPosX = areaInfo.getGridX();
                        mSelectedPosY = areaInfo.getGridY();

                        if (!TextUtils.isEmpty(mSelectedStep02Area))
                            mTextMapStep02.setText(mSelectedStep02Area);

                        setStep03Area();
                        mTextMapStep03.setText("");

                        dialog.dismiss();
                    }
                })
                .setHeader(R.layout.layout_area_header)
                .setExpanded(true)
                .create();
        dialog.show();
    }

    private void setStep03Area() {
        ArrayList<AreaInfo> listArea = new ArrayList<>();
        String depth02 = "";
        String depth03 = "";

        for (int index = 0; index < mListArea.size(); index++) {
            AreaInfo areaInfo = mListArea.get(index);

            String firstStep = areaInfo.getFirstStep();
            String secondStep = areaInfo.getSecondStep();
            String thirdStep = areaInfo.getThirdStep();

            if (TextUtils.isEmpty(firstStep) || TextUtils.isEmpty(secondStep) ||
                TextUtils.isEmpty(thirdStep))
                continue;

            if (firstStep.equals(mSelectedStep01Area) && depth02.equals(secondStep) &&
                !depth03.equals(thirdStep))
                listArea.add(areaInfo);

            depth02 = secondStep;
            depth03 = thirdStep;
        }

        mAreaStep03Adapter = new AreaAdapter(this, listArea, 3);
    }

    private void showAreaStep03() {
        DialogPlus dialog = DialogPlus.newDialog(this)
                .setAdapter(mAreaStep03Adapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        AreaInfo areaInfo = (AreaInfo) item;
                        mSelectedStep03Area = areaInfo.getThirdStep();
                        mSelectedPosX = areaInfo.getGridX();
                        mSelectedPosY = areaInfo.getGridY();

                        if (!TextUtils.isEmpty(mSelectedStep03Area))
                            mTextMapStep03.setText(mSelectedStep03Area);

                        dialog.dismiss();
                    }
                })
                .setHeader(R.layout.layout_area_header)
                .setExpanded(true)
                .create();
        dialog.show();
    }
}