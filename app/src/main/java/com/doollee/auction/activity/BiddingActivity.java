package com.doollee.auction.activity;

import android.animation.ValueAnimator;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.RequiresApi;

import com.doollee.auction.R;
import com.doollee.auction.common.Constants;
import com.doollee.auction.common.net.SocketSendTask;
import com.doollee.auction.common.net.SocketSender;
import com.doollee.auction.manager.DataManager;
import com.doollee.auction.ui.BaseSocketActivity;
import com.doollee.auction.utils.ComUtils;
import com.doollee.auction.utils.DialogUtils;
import com.doollee.auction.utils.JsonUtils;

import org.json.JSONObject;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BiddingActivity extends BaseSocketActivity {
    boolean initDataSelectYn = false;
    int     sidemenuSize     = 0 ;

    LinearLayout biddingNumberPad ; //응찰기 입력버튼 부분 Layout

    TextView textAucNo          ;
    TextView textWrsNm          ;
    TextView textSender         ;
    TextView textGrade          ;
    TextView textQt             ;
    TextView textBidAmt         ; //응찰기 입력 숫자 표시 부분
    TextView textAuctionWaitMsg ; //경매대기 메세지
    TextView textBidAmtErase    ;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bidding);

        biddingNumberPad   = findViewById(R.id.layout_bidding_number_pad);

        textAucNo          = findViewById(R.id.text_auc_no          );
        textWrsNm          = findViewById(R.id.text_wrs_nm          );
        textSender         = findViewById(R.id.text_sender          );
        textGrade          = findViewById(R.id.text_grade           );
        textQt             = findViewById(R.id.text_qt              );
        textBidAmt         = findViewById(R.id.text_bid_amt         ); //응찰기 입력 숫자 표시 부분
        textAuctionWaitMsg = findViewById(R.id.text_auction_wait_msg); //경매대기 메세지
        textBidAmtErase    = findViewById(R.id.text_bid_amt_erase   ); //입력값 하나씩 삭제 버튼

        initUX();

        textAuctionWaitMsg.setVisibility(View.VISIBLE);
        biddingNumberPad.setVisibility(View.GONE);

        initDataSelectYn = false;
        selectBiddingCret();
    }

    @Override
    public void onResume() {
        super.onResume();

        //사이드메뉴 레이아웃 값 저장 ( 후 초기화 )
        sideMenuAnim(0, 0);
    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();
        Intent intent = new Intent(BiddingActivity.this, MainActivity.class);
        startActivity(intent);
    }

    @Override
    protected void biddingRecvMsg() {
        //super.biddingRecvMsg();

        //화면에 실시간 수신데이터 적용
        if (initDataSelectYn) {
            Handler mHandler = new Handler(Looper.getMainLooper());
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    selectBidding();
                }
            }, 0);
        }
    }

    /**
     * 화면 터치 이벤트
     */
    View.OnTouchListener dragEventListener = new View.OnTouchListener() {
        float x = 0;
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    x = event.getX();
                    if (sidemenuSize > 0) {
                        sideMenuAnim(0, 500);
                        return false;
                    }
                    break;
                }
                case MotionEvent.ACTION_UP: {
                    if (x - event.getX() > 500) {
                        sideMenuAnim(250, 1000);
                    } else if(!"layout_bidding_center".equals(getResources().getResourceEntryName(v.getId()))) {
                        numberPadEvent((TextView) v);
                    }
                    break;
                }
            }
            return true;
        }
    };

    private void initUX() {
        //응찰참여화면에서 응찰참여버튼은 강조?상태로 표시
        TextView sideSelectBid = findViewById(R.id.go_select_bid);
        sideSelectBid.setBackground(getDrawable(R.drawable.selector_left_radius_btn_blue));

        //뒤로가기 버튼 이벤트
        TextView mBtnBack = findViewById(R.id.btn_back);
        mBtnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        //경매일정내역 화면이동 버튼
        TextView sideSchedule = findViewById(R.id.go_schedule);
        sideSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent;
                if (TextUtils.isEmpty(String.valueOf(DataManager.getInstance().getData("ScheSelSelectReq")))) {
                    intent = new Intent(BiddingActivity.this, InquiryScheduleActivity.class);
                } else {
                    intent = new Intent(BiddingActivity.this, ScheduleActivity.class);
                }
                startActivity(intent);
            }
        });

        //낙찰내역 화면이동 버튼
        TextView sideWinBid = findViewById(R.id.go_win_bid);
        sideWinBid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BiddingActivity.this, WinBidActivity.class);
                startActivity(intent);
            }
        });

        //알림내용 보기
        TextView sideNoticePopup = findViewById(R.id.go_notice_popup);
        sideNoticePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(BiddingActivity.this, BottomNoticeActivity.class);
                startActivityForResult(intent, 1);
            }
        });

        //화면 터치이벤트
        RelativeLayout layoutBiddingCenter = findViewById(R.id.layout_bidding_center);
        layoutBiddingCenter.setOnTouchListener(dragEventListener);

        //백 버튼-입력내용 하나씩 삭제
        textBidAmtErase.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if ("".contentEquals(textBidAmt.getText())) { return; } //지울 내용이 없으면 return
                //콤마를 제외한 기존 숫자와 입력한 값을 저장
                String sInputNum = ((String) textBidAmt.getText()).replaceAll(",","");
                sInputNum = sInputNum.substring(0, sInputNum.length()-1);
                if ("".equals(sInputNum)) {
                    textBidAmt.setText("");
                    return;
                } //지운 뒤 내용이 없으면 여기서 return
                //입력숫자에 콤마넣기
                textBidAmt.setText(ComUtils.setComma(sInputNum));
            }
        });

        //입력버튼 부분의 행(NumberPad) 클릭 이벤트 (반복)
        for (int i = 0; i < biddingNumberPad.getChildCount(); i++) {
            final View viewNumberPadRow = biddingNumberPad.getChildAt(i);
            //입력버튼 부분의 각 행별 cell (반복)
            for(int j = 0; j < ((LinearLayout) viewNumberPadRow).getChildCount(); j++) {
                View viewNumberPadColumn = ((LinearLayout) viewNumberPadRow).getChildAt(j);
                viewNumberPadColumn.setOnTouchListener(dragEventListener);
            }
        }
    }

    /**
     * 응찰화면 NumberPad Event
     */
    public void numberPadEvent(TextView innerTextview) {

        String inputText = (String) innerTextview.getText();

        //취소버튼-입력내용 전체삭제
        if (innerTextview.getId() == R.id.text_bid_amt_remove) {
            textBidAmt.setText("");
            return;
        }

        //응찰버튼
        if (innerTextview.getId() == R.id.btn_bide) {
            if (TextUtils.isEmpty(textAucNo.getText()) ) {
                DialogUtils.alert(
                        BiddingActivity.this
                        ,"응찰 대상의 경매번호가 확인되지 않습니다."
                );
                return;
            }
            if( TextUtils.isEmpty(textBidAmt.getText()) ) {
                DialogUtils.alert(
                        BiddingActivity.this
                        ,"응찰 대상의 응찰가가 확인되지 않습니다."
                );
                return;
            }

            showProgressDialog();

            //socket collback
            SocketSendTask.OnSocketCallback socketCallback = new SocketSendTask.OnSocketCallback() {
                @Override
                public void onConnect() { }

                @Override
                public void onFail(String msg) {
                    dismissProgressDialog();
                    showCustomToast(getString(R.string.msg_no_response));
                }

                @Override
                public void onSuccess(String result) {
                    dismissProgressDialog();

                    try {
                        Map<String, Object> rtnMapData = JsonUtils.jsonStringToMap(result);
                        if (rtnMapData != null) {
                            if (Constants.DefineValue.YES.equalsIgnoreCase(String.valueOf(rtnMapData.get("SUCCESS_YN")))) {
                                showCustomToast("[ " + textBidAmt.getText() + "원 ] 으로 응찰되었습니다.");
                            } else {
                                showCustomToast("응찰이 정상적으로 이루어지지 않았습니다.\n" + rtnMapData.get("ERRER_MSG"));
                            }
                        }
                    } catch (Exception e) {
                        e.getStackTrace();
                    }
                }
            };

            //응찰등록내용 생성
            Map map = setSendMsgHeader("202");
            map.put("AUC_NO" , String.valueOf(textAucNo.getText()) );
            map.put("BID_AMT", String.valueOf(textBidAmt.getText()).replaceAll(",", ""));
            JSONObject json = JsonUtils.mapToJson(map);

            //소켓통신 실행
            Socket socket = DataManager.getInstance().getSocketMapData(10101);
            SocketSender.getInstance().sendMsg(socket, "10101", json.toString(), socketCallback);

            return;
        }

        //콤마를 제외한 기존 숫자와 입력한 값을 저장
        String sInputNum = ((String) textBidAmt.getText()).replaceAll(",","") + inputText;

        //앞자리가 0인 경우 무시
        if ("0".equals(sInputNum.substring(0,1))) { return; }

        //입력값이 10자리를 넘어서면 무시 ---> 차후 최대 응찰가보다 큰 경우 최대값을 넣는 것으로 수정필요
        if( sInputNum.length() > 10 ) { return; }

        //입력숫자에 콤마넣기
        textBidAmt.setText(ComUtils.setComma(sInputNum));
    }

    /**
     * 데이터 요청 (초기화면실행시 경매진행중이었을 경우만)
     */
    public void selectBiddingCret() {

        showProgressDialog();

        //socket collback - 기존 받은 데이터가 없는 경우 현재 진행중인 경매가 있는지 조회
        SocketSendTask.OnSocketCallback socketCallback = new SocketSendTask.OnSocketCallback() {
            @Override
            public void onConnect() { }

            @Override
            public void onFail(String msg) {
                dismissProgressDialog();
                showCustomToast(getString(R.string.msg_no_response));
            }

            @Override
            public void onSuccess(String result) {
                dismissProgressDialog();

                try {
                    Map<String, Object> rtnMapData = JsonUtils.jsonStringToMap(result);
                    if (rtnMapData != null) {
                        if (!"NO DATA".equals(rtnMapData.get("FAIL_MSG"))) {

                            Map<String, Object> rtnInData = new ArrayMap<>();
                            
                            if ("1".equals(rtnMapData.get("SUCCESS_YN"))) {
                                String rt_Msg ;

                                //알림 메세지 생성
                                if (rtnMapData.containsKey("WRS_C")) {
                                    rt_Msg = "[" + rtnMapData.get("AUC_NO") + "]" + "번 경매를 진행합니다.";
                                } else {
                                    rt_Msg = "[" + rtnMapData.get("AUC_NO") + "]" + "번 경매"
                                            + ("3".equals(rtnMapData.get("AUC_ST")) ? "에 낙찰" : "가 종료") + "되었습니다.";
                                }
                                rt_Msg += "\n\n" + result;

                                rtnInData.put("time" , "------"   ); //진입시 최초조회
                                rtnInData.put("cont" , result     );
                                rtnInData.put("msg"  , rt_Msg     );
                                rtnInData.put("newYn", "Y"        );
                                List list = new ArrayList();
                                list.add(rtnInData);
                                DataManager.getInstance().setData("NoticeList", list);

                                selectBidding();   //화면에 데이터 표시
                            } else {
                                showCustomToast((String) rtnMapData.get("FAIL_MSG"));
                            }
                        }
                    }
                } catch (Exception e) {
                    e.getStackTrace();
                }
            }
        };

        //조건데이터전문생성
        Map map = setSendMsgHeader("105");
        JSONObject json = JsonUtils.mapToJson(map);

        //소켓통신 실행
        Socket socket = DataManager.getInstance().getSocketMapData(10001);
        SocketSender.getInstance().sendMsg(socket, "10001", json.toString(), socketCallback);

        initDataSelectYn = true; //초기실행 데이터 조회 완료시
    }

    /**
     * 전역변수의(server) 데이터를 화면에 표시
     */
    public void selectBidding() {
        //객체초기화
        textAucNo .setText("");
        textWrsNm .setText("");
        textSender.setText("");
        textGrade .setText("");
        textQt    .setText("");
        textBidAmt.setText("");

        textAuctionWaitMsg.setVisibility(View.VISIBLE);
        biddingNumberPad.setVisibility(View.GONE);

        //저장된 응찰 데이터가 없으면 return
        if (TextUtils.isEmpty(String.valueOf(DataManager.getInstance().getData("NoticeList")))) {
            return;
        }

        //저장된 데이터에서 최근 내용만 출력
        List   noticeList = (List) DataManager.getInstance().getData("NoticeList");
        String inData     = (String) ((Map) noticeList.get(noticeList.size()-1)).get("cont");

        try {
            JSONObject jsonObject = new JSONObject(inData);
            if (jsonObject != null) {
                Map mapDataInfo = JsonUtils.jsonToMap(jsonObject);

                if (mapDataInfo.containsKey("AUC_NO")) {
                    textAucNo.setText((String) mapDataInfo.get("AUC_NO"));
                }

                if (mapDataInfo.containsKey("WRS_NM")) {
                    textWrsNm.setText((String) mapDataInfo.get("WRS_NM"));
                }

                if (mapDataInfo.containsKey("SENDER")) {
                    textSender.setText((String) mapDataInfo.get("SENDER"));
                }

                if (mapDataInfo.containsKey("GRADE")) {
                    textGrade.setText((String) mapDataInfo.get("GRADE"));
                }

                if (mapDataInfo.containsKey("QT")) {
                    textQt.setText(ComUtils.setComma((String) mapDataInfo.get("QT")));
                }

                //상품명이나 출하자명이 없는 경우 진행중 경매가 아닌 것으로 판단
                if (mapDataInfo.containsKey("WRS_NM") && mapDataInfo.containsKey("SENDER")) {
                    textAuctionWaitMsg.setVisibility(View.GONE);
                    biddingNumberPad.setVisibility(View.VISIBLE);
                } else {
                    textAuctionWaitMsg.setVisibility(View.VISIBLE);
                    biddingNumberPad.setVisibility(View.GONE);
                }
            }

        } catch (Exception e) {
            Log.d("", e.toString());
        }
    }

    /**
     * sidemenu onoff animation 처리
     */
    private void sideMenuAnim(int size, int duration) {
        final LinearLayout vSideMenu = findViewById(R.id.layout_bidding_right);
        final ValueAnimator va = ValueAnimator.ofInt(vSideMenu.getMeasuredWidth(), size);
        va.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            int val = 0;
            RelativeLayout.LayoutParams lp = null;
            @Override
            public void onAnimationUpdate(ValueAnimator animation) {
                val = (Integer) va.getAnimatedValue();
                lp = (RelativeLayout.LayoutParams) vSideMenu.getLayoutParams();
                lp.width = val;
                vSideMenu.setLayoutParams( lp );
            }
        });
        sidemenuSize = size;
        va.setDuration(duration);
        va.start();
    }
}