package com.doollee.auction.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.doollee.auction.R;
import com.doollee.auction.adapter.WinBidAdapter;
import com.doollee.auction.common.Constants;
import com.doollee.auction.common.net.SocketSendTask;
import com.doollee.auction.common.net.SocketSender;
import com.doollee.auction.manager.DataManager;
import com.doollee.auction.ui.BaseSocketActivity;
import com.doollee.auction.utils.JsonUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.Socket;
import java.util.ArrayList;
import java.util.Map;

public class WinBidActivity extends BaseSocketActivity {
    private TextView        mTextNoWinBid;
    private RecyclerView    mRecyclerWinBid;

    private ArrayList       mListWinBid;

    private WinBidAdapter   mWinBidAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_win_bid);

        initUX();
    }

    private void initUX() {
        TextView btnBack =  (TextView) findViewById(R.id.btn_back);
        mTextNoWinBid = (TextView) findViewById(R.id.text_no_win_bid);
        mRecyclerWinBid = (RecyclerView) findViewById(R.id.recyclerview_win_bid);
        TextView goSchedule = (TextView) findViewById(R.id.go_schedule);
        TextView goWinBid = (TextView) findViewById(R.id.go_win_bid);
        TextView goSelectBid = (TextView) findViewById(R.id.go_select_bid);
        TextView goNoticePopup = (TextView) findViewById(R.id.go_notice_popup);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(0, 0);
            }
        });

        goSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WinBidActivity.this, InquiryScheduleActivity.class);
                startActivity(intent);
                finish();
            }
        });

        goWinBid.setBackground(getDrawable(R.drawable.selector_left_radius_btn_blue));

        goSelectBid.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WinBidActivity.this, BiddingActivity.class);
                startActivity(intent);
                finish();
            }
        });

        goNoticePopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(WinBidActivity.this, BottomNoticeActivity.class);
                startActivity(intent);
            }
        });

        requestWinBid();
    }

    public void requestWinBid() {
        SocketSendTask.OnSocketCallback socketCallback = new SocketSendTask.OnSocketCallback() {
            @Override
            public void onConnect() {

            }

            @Override
            public void onFail(String msg) {
                dismissProgressDialog();
                showCustomToast(getString(R.string.msg_no_response));
            }

            @Override
            public void onSuccess(String result) {
                dismissProgressDialog();

                try {
                    JSONObject jsonObject = new JSONObject(result);
                    if (jsonObject == null)
                        return;

                    Map mapDataInfo = JsonUtils.jsonToMap(jsonObject);

                    if (!"NO DATA".equals(mapDataInfo.get("FAIL_MSG"))) {
                        if (Constants.DefineValue.YES.equals(mapDataInfo.get("SUCCESS_YN")) ) {
                            mListWinBid = (ArrayList) mapDataInfo.get("body");
                            if (mListWinBid == null && mListWinBid.size() <= 0) {
                                mTextNoWinBid.setVisibility(View.VISIBLE);
                                mRecyclerWinBid.setVisibility(View.GONE);
                            } else {
                                setListWinBid();

                                mTextNoWinBid.setVisibility(View.GONE);
                                mRecyclerWinBid.setVisibility(View.VISIBLE);
                            }
                        } else {
                            showCustomToast((String) mapDataInfo.get("FAIL_MSG"));
                        }
                    }
                } catch (JSONException e) {

                }
            }
        };

        Map map = setSendMsgHeader("104");
        JSONObject json = JsonUtils.mapToJson(map);

        showProgressDialog();

        Socket socket = DataManager.getInstance().getSocketMapData(10001);
        SocketSender.getInstance().sendMsg(socket, "10001", json.toString(), socketCallback);
    }

    private void setListWinBid() {
        if (mWinBidAdapter == null) {
            mWinBidAdapter = new WinBidAdapter(this, mListWinBid, new WinBidAdapter.onListener() {
                @Override
                public void onClickListener(int position) {
                    Intent intent = new Intent(WinBidActivity.this, ScheduleDetailActivity.class);
                    intent.putExtra(Constants.ScheduleInfo.SCHEDULE_POS, position);
                    intent.putStringArrayListExtra(Constants.ScheduleInfo.SCHEDULE_LIST, mListWinBid);
                    startActivity(intent);
                }
            });

            mRecyclerWinBid.setAdapter(mWinBidAdapter);

            LinearLayoutManager llm = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
            mRecyclerWinBid.setLayoutManager(llm);
            mRecyclerWinBid.setHasFixedSize(true);
        }
    }
}