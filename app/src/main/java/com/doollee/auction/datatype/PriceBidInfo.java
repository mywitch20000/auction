package com.doollee.auction.datatype;

import java.io.Serializable;

/**
 *
 * Class: PriceBidInfo
 * Created by wizard on 2021.02.10
 *
 * Description: 경매 정보
 */

public class PriceBidInfo implements Serializable {
    private String   delngDe;            // 경락일자
    private String   sbidTime;           // 경매시간
    private String   aucSeCode;          // 경매구분코드
    private String   aucSeNm;            // 경매구분코드명
    private String   whsalMrktNewCode;   // 시장코드
    private String   whsalMrktNewNm;     // 시장명
    private String   whsalMrktCode;      // 구시장코드
    private String   whsalMrktNm;        // 구시장명
    private String   cprInsttNewCode;    // 법인코드
    private String   insttNewNm;         // 법인명
    private String   cprInsttCode;       // 구법인코드
    private String   insttNm;            // 구법인명
    private String   ledgNo;             // 경매원표번호
    private String   sleSeqn;            // 일련번호
    private String   stdPrdlstNewCode;   // 부류코드
    private String   stdPrdlstNewNm;     // 부류명
    private String   stdSpciesNewCode;   // 품목코드
    private String   stdSpciesNewNm;     // 품목명
    private String   delngPrut;          // 거래단량
    private String   stdUnitNewCode;     // 단위코드
    private String   stdUnitNewNm;       // 단위명
    private String   stdFrmlcNewCode;    // 포장상태코드
    private String   stdFrmlcNewNm;      // 포장상태명
    private String   sbidPric;           // 거래가격
    private String   delngQy;            // 거래량

    private String   catgoryNewCode;
    private String   catgoryNewNm;
    private String   stdMgNewCode;
    private String   stdMgNewNm;
    private String   stdQlityNewCode;
    private String   stdQlityNewNm;
    private String   cprUsePrdlstCode;
    private String   cprUsePrdlstNm;
    private String   shipmntSeCode;
    private String   shipmntSeNm;
    private String   stdMtcNewCode;     // 산지코드
    private String   stdMtcNewNm;       // 산지명
    private String   cprMtcCode;        // 구산지명
    private String   cprMtcNm;          // 구산지명

    public PriceBidInfo() {

    }

    /**
     * 경락일자 반환
     *
     * @return delngDe 경락일자
     */
    public String getDelngDe() {
        return delngDe;
    }

    /**
     * 경락일자 설정
     *
     * @param delngDe 경락일자
     * @return
     */
    public void setDelngDe(String delngDe) {
        this.delngDe = delngDe;
    }

    /**
     * 경매시간 반환
     *
     * @return sbidTime 시장명
     */
    public String getSbidTime() {
        return sbidTime;
    }

    /**
     * 경매시간 설정
     *
     * @param sbidTime 시장명
     * @return
     */
    public void setSbidTime(String sbidTime) {
        this.sbidTime = sbidTime;
    }

    /**
     * 경매구분코드 반환
     *
     * @return aucSeCode 경매구분코드
     */
    public String getAucSeCode() {
        return aucSeCode;
    }

    /**
     * 경매구분코드 설정
     *
     * @param aucSeCode 경매구분코드
     * @return
     */
    public void setAucSeCode(String aucSeCode) {
        this.aucSeCode = aucSeCode;
    }

    /**
     * 경매구분코드명 반환
     *
     * @return aucSeCode 경매구분코드명
     */
    public String getAucSeNm() {
        return aucSeNm;
    }

    /**
     * 경매구분코드명 설정
     *
     * @param aucSeNm 경매구분코드명
     * @return
     */
    public void setAucSeNm(String aucSeNm) {
        this.aucSeNm = aucSeNm;
    }

    /**
     * 시장코드 반환
     *
     * @return whsalMrktNewCode 시장코드
     */
    public String getWhsalMrktNewCode() {
        return whsalMrktNewCode;
    }

    /**
     * 시장코드 설정
     *
     * @param whsalMrktNewCode 시장코드
     * @return
     */
    public void setWhsalMrktNewCode(String whsalMrktNewCode) {
        this.whsalMrktNewCode = whsalMrktNewCode;
    }

    /**
     * 시장명 반환
     *
     * @return whsalMrktNewNm 시장명
     */
    public String getWhsalMrktNewNm() {
        return whsalMrktNewNm;
    }

    /**
     * 시장명 설정
     *
     * @param whsalMrktNewNm 시장명
     * @return
     */
    public void setWhsalMrktNewNm(String whsalMrktNewNm) {
        this.whsalMrktNewNm = whsalMrktNewNm;
    }

    /**
     * 구시장명 반환
     *
     * @return whsalMrktCode 시장명
     */
    public String getWhsalMrktCode() {
        return whsalMrktCode;
    }

    /**
     * 구시장명 설정
     *
     * @param whsalMrktCode 시장명
     * @return
     */
    public void setWhsalMrktCode(String whsalMrktCode) {
        this.whsalMrktCode = whsalMrktCode;
    }

    /**
     * 도매시장법인코드 반환
     *
     * @return cprInsttNewCode 도매시장법인코드
     */
    public String getCprInsttNewCode() {
        return cprInsttNewCode;
    }

    /**
     * 도매시장법인명 설정
     *
     * @param cprInsttNewCode 도매시장법인코드
     * @return
     */
    public void setCprInsttNewCode(String cprInsttNewCode) {
        this.cprInsttNewCode = cprInsttNewCode;
    }

    /**
     * 도매시장법인코드 설정
     *
     * @param insttNewNm 도매시장법인코드
     * @return
     */
    public void setInsttNewNm(String insttNewNm) {
        this.insttNewNm = insttNewNm;
    }

    /**
     * 도매시장법인명 반환
     *
     * @return insttNm 법인명
     */
    public String getInsttNewNm() {
        return insttNewNm;
    }

    /**
     * 구도매시장법인코드 반환
     *
     * @return cprInsttCode 도매시장법인코드
     */
    public String cprInsttCode() {
        return cprInsttCode;
    }

    /**
     * 구도매시장법인코드 설정
     *
     * @param cprInsttCode 도매시장법인코드
     * @return
     */
    public void setCprInsttCode(String cprInsttCode) {
        this.cprInsttCode = cprInsttCode;
    }

    /**
     * 구도매시장법인명 반환
     *
     * @return insttNm 법인명
     */
    public String getInsttNm() {
        return insttNm;
    }

    /**
     * 구도매시장법인명 설정
     *
     * @param insttNm 법인명
     * @return
     */
    public void setInsttNm(String insttNm) {
        this.insttNm = insttNm;
    }

    /**
     * 경매원표번호 반환
     *
     * @return ledgNo 경매원표번호
     */
    public String getLedgNo() {
        return ledgNo;
    }

    /**
     * 경매원표번호 설정
     *
     * @param ledgNo 경매원표번호
     * @return
     */
    public void setLedgNo(String ledgNo) {
        this.ledgNo = ledgNo;
    }

    /**
     * 일련번호 반환
     *
     * @return sleSeqn 일련번호
     */
    public String getSleSeqn() {
        return sleSeqn;
    }

    /**
     * 일련번호 설정
     *
     * @param sleSeqn 일련번호
     * @return
     */
    public void setSleSeqn(String sleSeqn) {
        this.sleSeqn = sleSeqn;
    }

    /**
     * 품목코드 반환
     *
     * @return stdPrdlstNewCode 품목코드
     */
    public String getStdPrdlstNewCode() {
        return stdPrdlstNewCode;
    }

    /**
     * 품목코드 설정
     *
     * @param stdPrdlstNewCode 품목코드
     * @return
     */
    public void setStdPrdlstNewCode(String stdPrdlstNewCode) {
        this.stdPrdlstNewCode = stdPrdlstNewCode;
    }

    /**
     * 품목명 반환
     *
     * @return stdPrdlstNewNm 품목명
     */
    public String getStdPrdlstNewNm() {
        return stdPrdlstNewNm;
    }

    /**
     * 품목명 설정
     *
     * @param stdPrdlstNewNm 품종코드
     * @return
     */
    public void setStdPrdlstNewNm(String stdPrdlstNewNm) {
        this.stdPrdlstNewNm = stdPrdlstNewNm;
    }

    /**
     * 품종코드 반환
     *
     * @return stdPrdlstNewNm 품종코드
     */
    public String getStdSpciesNewCode() {
        return stdSpciesNewCode;
    }

    /**
     * 품종코드 설정
     *
     * @param stdSpciesNewCode 품목명
     * @return
     */
    public void setStdSpciesNewCode(String stdSpciesNewCode) {
        this.stdSpciesNewCode = stdSpciesNewCode;
    }

    /**
     * 품종명 반환
     *
     * @return stdPrdlstNewNm 품종코드
     */
    public String getStdSpciesNewNm() {
        return stdSpciesNewNm;
    }

    /**
     * 품종명 설정
     *
     * @param stdSpciesNewNm 품목명
     * @return
     */
    public void setStdSpciesNewNm(String stdSpciesNewNm) {
        this.stdSpciesNewNm = stdSpciesNewNm;
    }

    /**
     * 거래단량 반환
     *
     * @return delngPrut 거래단량
     */
    public String getDelngPrut() {
        return delngPrut;
    }

    /**
     * 거래단량 설정
     *
     * @param delngPrut 거래단량
     * @return
     */
    public void setDelngPrut(String delngPrut) {
        this.delngPrut = delngPrut;
    }


    /**
     * 단위코드 반환
     *
     * @return stdUnitNewCode 단위코드
     */
    public String getStdUnitNewCode() {
        return stdUnitNewCode;
    }

    /**
     * 단위코드 설정
     *
     * @param stdUnitNewCode 단위코드
     * @return
     */
    public void setStdUnitNewCode(String stdUnitNewCode) {
        this.stdUnitNewCode = stdUnitNewCode;
    }

    /**
     * 단위명 반환
     *
     * @return stdUnitNewNm 단위명
     */
    public String getStdUnitNewNm() {
        return stdUnitNewNm;
    }

    /**
     * 단위명 설정
     *
     * @param stdUnitNewNm 단위명
     * @return
     */
    public void setStdUnitNewNm(String stdUnitNewNm) {
        this.stdUnitNewNm = stdUnitNewNm;
    }

    /**
     * 포장상태코드 반환
     *
     * @return stdFrmlcNewCode 포장상태코드
     */
    public String getStdFrmlcNewCode() {
        return stdFrmlcNewCode;
    }

    /**
     * 포장상태코드 설정
     *
     * @param stdFrmlcNewCode 포장상태코드
     * @return
     */
    public void setStdFrmlcNewCode(String stdFrmlcNewCode) {
        this.stdFrmlcNewCode = stdFrmlcNewCode;
    }

    /**
     * 포장상태명 반환
     *
     * @return stdFrmlcNewNm 포장상태명
     */
    public String getStdFrmlcNewNm() {
        return stdFrmlcNewNm;
    }

    /**
     * 포장상태명 설정
     *
     * @param stdFrmlcNewNm 포장상태명
     * @return
     */
    public void setStdFrmlcNewNm(String stdFrmlcNewNm) {
        this.stdFrmlcNewNm = stdFrmlcNewNm;
    }

    /**
     * 거래가격 반환
     *
     * @return sbidPric 거래가격
     */
    public String getSbidPric() {
        return sbidPric;
    }

    /**
     * 거래가격 설정
     *
     * @param sbidPric 거래가격
     * @return
     */
    public void setSbidPric(String sbidPric) {
        this.sbidPric = sbidPric;
    }


    /**
     * 거래량 반환
     *
     * @return delngQy 거래량
     */
    public String getDelngQy() {
        return delngQy;
    }

    /**
     * 거래량 설정
     *
     * @param delngQy 거래량
     * @return
     */
    public void setDelngQy(String delngQy) {
        this.delngQy = delngQy;
    }

    /**
     * 부류코드 반환
     *
     * @return catgoryNewCode 부류코드
     */
    public String getCatgoryNewCode() {
        return catgoryNewCode;
    }

    /**
     * 부류코드 설정
     *
     * @param catgoryNewCode 부류코드
     * @return
     */
    public void setCatgoryNewCode(String catgoryNewCode) {
        this.catgoryNewCode = catgoryNewCode;
    }

    /**
     * 부류명 반환
     *
     * @return catgoryNewNm 부류명
     */
    public String getCatgoryNewNm() {
        return catgoryNewNm;
    }

    /**
     * 부류명 설정
     *
     * @param catgoryNewNm 부류명
     * @return
     */
    public void setCatgoryNewNm(String catgoryNewNm) {
        this.catgoryNewNm = catgoryNewNm;
    }

    /**
     * 크기코드 반환
     *
     * @return stdMgNewCode 크기코드
     */
    public String getStdMgNewCode() {
        return stdMgNewCode;
    }

    /**
     * 크기코드 설정
     *
     * @param stdMgNewCode 크기코드
     * @return
     */
    public void setStdMgNewCode(String stdMgNewCode) {
        this.stdMgNewCode = stdMgNewCode;
    }

    /**
     * 크기명 반환
     *
     * @return stdMgNewNm 크기명
     */
    public String getStdMgNewNm() {
        return stdMgNewNm;
    }

    /**
     * 크기명 설정
     *
     * @param stdMgNewNm 크기명
     * @return
     */
    public void setStdMgNewNm(String stdMgNewNm) {
        this.stdMgNewNm = stdMgNewNm;
    }

    /**
     * 등급코드 반환
     *
     * @return stdQlityNewCode 등급코드
     */
    public String getStdQlityNewCode() {
        return stdQlityNewCode;
    }

    /**
     * 등급코드 설정
     *
     * @param stdQlityNewCode 등급코드
     * @return
     */
    public void setStdQlityNewCode(String stdQlityNewCode) {
        this.stdQlityNewCode = stdQlityNewCode;
    }

    /**
     * 등급명 반환
     *
     * @return stdQlityNewNm 등급명
     */
    public String getStdQlityNewNm() {
        return stdQlityNewNm;
    }

    /**
     * 경락일자 설정
     *
     * @param stdQlityNewNm 등급명
     * @return
     */
    public void setStdQlityNewNm(String stdQlityNewNm) {
        this.stdQlityNewNm = stdQlityNewNm;
    }

    /**
     * 법인사용품목코드 반환
     *
     * @return cprUsePrdlstCode 법인사용품목코드
     */
    public String getCprUsePrdlstCode() {
        return cprUsePrdlstCode;
    }

    /**
     * 법인사용품목코드 설정
     *
     * @param cprUsePrdlstCode 법인사용품목코드
     * @return
     */
    public void setCprUsePrdlstCode(String cprUsePrdlstCode) {
        this.cprUsePrdlstCode = cprUsePrdlstCode;
    }

    /**
     * 법인사용품목명 반환
     *
     * @return cprUsePrdlstNm 법인사용품목명
     */
    public String getCprUsePrdlstNm() {
        return cprUsePrdlstNm;
    }

    /**
     * 법인사용품목명 설정
     *
     * @param cprUsePrdlstNm 법인사용품목명
     * @return
     */
    public void setCprUsePrdlstNm(String cprUsePrdlstNm) {
        this.cprUsePrdlstNm = cprUsePrdlstNm;
    }

    /**
     * 출하구분코드 반환
     *
     * @return shipmntSeCode 출하구분코드
     */
    public String getShipmntSeCode() {
        return shipmntSeCode;
    }

    /**
     * 출하구분코드 설정
     *
     * @param shipmntSeCode 출하구분코드
     * @return
     */
    public void setShipmntSeCode(String shipmntSeCode) {
        this.shipmntSeCode = shipmntSeCode;
    }

    /**
     * 출하구분명 반환
     *
     * @return shipmntSeNm 출하구분명
     */
    public String getShipmntSeNm() {
        return shipmntSeNm;
    }

    /**
     * 출하구분명 설정
     *
     * @param shipmntSeNm 출하구분명
     * @return
     */
    public void setShipmntSeNm(String shipmntSeNm) {
        this.shipmntSeNm = shipmntSeNm;
    }

    /**
     * 산지코드 반환
     *
     * @return stdMtcNewCode 산지코드
     */
    public String getStdMtcNewCode() {
        return stdMtcNewCode;
    }

    /**
     * 산지코드 설정
     *
     * @param stdMtcNewCode 산지코드
     * @return
     */
    public void setStdMtcNewCode(String stdMtcNewCode) {
        this.stdMtcNewCode = stdMtcNewCode;
    }

    /**
     * 산지명 반환
     *
     * @return stdMtcNewNm 산지명
     */
    public String getStdMtcNewNm() {
        return stdMtcNewNm;
    }

    /**
     * 산지명 설정
     *
     * @param stdMtcNewNm 산지명
     * @return
     */
    public void setStdMtcNewNm(String stdMtcNewNm) {
        this.stdMtcNewNm = stdMtcNewNm;
    }

    /**
     * 산지코드 반환
     *
     * @return cprMtcCode 산지명
     */
    public String getCprMtcCode() {
        return cprMtcCode;
    }

    /**
     * 산지코드 설정
     *
     * @param cprMtcCode 산지코드
     * @return
     */
    public void setCprMtcCode(String cprMtcCode) {
        this.cprMtcCode = cprMtcCode;
    }

    /**
     * 산지명 반환
     *
     * @return cprMtcCode 산지명
     */
    public String getCprMtcNm() {
        return cprMtcNm;
    }

    /**
     * 산지명 설정
     *
     * @param cprMtcNm 산지명
     * @return
     */
    public void setCprMtcNm(String cprMtcNm) {
        this.cprMtcNm = cprMtcNm;
    }
}
