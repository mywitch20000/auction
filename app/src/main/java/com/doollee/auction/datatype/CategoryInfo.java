package com.doollee.auction.datatype;

import java.io.Serializable;

/**
 *
 * Class: CategoryInfo
 * Created by wizard on 2021.02.09
 *
 * Description: 경매 정보
 */

public class CategoryInfo implements Serializable {
    private String   mainCategoryCode;   // 부류코드
    private String   mainCategoryName;   // 부류명
    private String   subCategoryCode;    // 품목코드
    private String   subCategoryName;    // 품목명
    private String   varietiesCode;      // 품종코드
    private String   varietiesName;      // 품종명

    public CategoryInfo() {

    }

    public CategoryInfo(String mainCategoryCode, String mainCategoryName) {
        this.mainCategoryCode = mainCategoryCode;
        this.mainCategoryName = mainCategoryName;
    }

    public CategoryInfo(String mainCategoryCode, String mainCategoryName, String subCategoryCode, String subCategoryName) {
        this.mainCategoryCode = mainCategoryCode;
        this.mainCategoryName = mainCategoryName;
        this.subCategoryCode = subCategoryCode;
        this.subCategoryName = subCategoryName;
    }

    public CategoryInfo(String mainCategoryCode, String mainCategoryName, String subCategoryCode, String subCategoryName,
                        String varietiesCod, String varietiesName) {
        this.mainCategoryCode = mainCategoryCode;
        this.mainCategoryName = mainCategoryName;
        this.subCategoryCode = subCategoryCode;
        this.subCategoryName = subCategoryName;
        this.varietiesCode = varietiesCod;
        this.varietiesName = varietiesName;
    }

    /**
     * 부류코드 반환
     *
     * @return mainCategoryCode 부류코드
     */
    public String getMainCategoryCode() {
        return mainCategoryCode;
    }

    /**
     * 부류코드 설정
     *
     * @param mainCategoryCode 부류코드
     * @return
     */
    public void setMainCategoryCode(String mainCategoryCode) {
        this.mainCategoryCode = mainCategoryCode;
    }

    /**
     * 부류명 반환
     *
     * @return mainCategoryName 부류명
     */
    public String getMainCategoryName() {
        return mainCategoryName;
    }

    /**
     * 부류명 설정
     *
     * @param mainCategoryName 부류명
     * @return
     */
    public void setMainCategoryName(String mainCategoryName) {
        this.mainCategoryName = mainCategoryName;
    }

    /**
     * 품목코드 반환
     *
     * @return subCategoryCode 품목코드
     */
    public String getSubCategoryCode() {
        return subCategoryCode;
    }

    /**
     * 품목코드 설정
     *
     * @param subCategoryCode 품목코드
     * @return
     */
    public void setSubCategoryCode(String subCategoryCode) {
        this.subCategoryCode = subCategoryCode;
    }

    /**
     * 품목명 반환
     *
     * @return subCategoryName 품목명
     */
    public String getSubCategoryName() {
        return subCategoryName;
    }

    /**
     * 품목명 설정
     *
     * @param subCategoryName 품목명
     * @return
     */
    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    /**
     * 품종코드 반환
     *
     * @return varietiesCode 품종코드
     */
    public String getVarietiesCode() {
        return varietiesCode;
    }

    /**
     * 품종코드 설정
     *
     * @param varietiesCode 품종코드
     * @return
     */
    public void setVarietiesCode(String varietiesCode) {
        this.varietiesCode = varietiesCode;
    }

    /**
     * 업데이트일자 반환
     *
     * @return varietiesName 업데이트일자
     */
    public String getvarietiesName() {
        return varietiesName;
    }

    /**
     * 업데이트일자 설정
     *
     * @param varietiesName 업데이트일자
     * @return
     */
    public void setVarietiesName(String varietiesName) {
        this.varietiesName = varietiesName;
    }
}
