package com.doollee.auction.datatype;

import java.util.ArrayList;

/**
 *
 * Class: LoginUserInfo
 * Created by 513751 on 2019.02.12
 *
 * Description: 로그인 사용자 정보
 */

public class LoginUserInfo {
    private static LoginUserInfo instance = null;

    private String   biderId;             // 로그인 여부

    private String   validateBiderNo;    //

    public static LoginUserInfo getInstance() {
        if (instance == null) {
            synchronized (LoginUserInfo.class) {
                instance = new LoginUserInfo();
            }
        }
        return instance;
    }

    /**
     * 로그인 사용자 정보 인스턴트 삭제
     *
     * @return
     */
    public static void clearInstance() {
        if (instance == null)
            return;

        instance = null;
    }

    /**
     * 로그인 사용자 정보 생성자
     */
    private LoginUserInfo() {

    }

    /**
     * 입찰자 사용자 정보 인스턴트 설정
     *
     * @return
     */
    public static void setInstance(LoginUserInfo instance) {
        LoginUserInfo.instance = instance;
    }

    /**
     * 입찰자 아이디 반환
     *
     * @return boolean 로그인 여부
     */
    public String getBiderId() {
        return biderId;
    }

    /**
     * 입찰자 아이디 설정
     *
     * @param biderId 중개인 여부
     * @return
     */
    public void setBiderId(String biderId) {
        this.biderId = biderId;
    }

    /**
     * 입찰자 아이디 반환
     *
     * @return boolean 로그인 여부
     */
    public String getValidateBiderNo() {
        return validateBiderNo;
    }

    /**
     * 입찰자 아이디 설정
     *
     * @param validateBiderNo 중개인 여부
     * @return
     */
    public void setValidateBiderNo(String validateBiderNo) {
        this.validateBiderNo = validateBiderNo;
    }
}