package com.doollee.auction.datatype;

import java.io.Serializable;

/**
 *
 * Class: AreaInfo
 * Created by wizard on 2021.02.22
 *
 * Description: 경매 정보
 */

public class AreaInfo implements Serializable {
    private String  division;         // 구분
    private String  areaCode;         // 행정구역코드
    private String  firstStep;        // 1단계
    private String  secondStep;       // 2단계
    private String  thirdStep;        // 3단계
    private String  gridX;            // 격자 X
    private String  gridY;            // 격자 Y
    private String  longitudeHour;    // 경도(시)
    private String  longitudeMinute;  // 경도(분)
    private String  longitudeSecond;  // 경도(초)
    private String  latitudeHour;     // 위도(시)
    private String  latitudeMinute;   // 위도(분)
    private String  latitudeSecond;   // 위도(초)
    private String  longitudeMilli;   // 경도(초/100)
    private String  latitudeMilli;    // 위도(초/100)

    public AreaInfo() {

    }

    /**
     * 구분 반환
     *
     * @return division 구분
     */
    public String getDivision() {
        return division;
    }

    /**
     * 구분 설정
     *
     * @param division 구분
     * @return
     */
    public void setDivision(String division) {
        this.division = division;
    }

    /**
     * 행정구역코드 반환
     *
     * @return areaCode 행정구역코드
     */
    public String getAreaCode() {
        return areaCode;
    }

    /**
     * 행정구역코드 설정
     *
     * @param areaCode 행정구역코드
     * @return
     */
    public void setAreaCode(String areaCode) {
        this.areaCode = areaCode;
    }

    /**
     * 1단계 반환
     *
     * @return firstStep 1단계
     */
    public String getFirstStep() {
        return firstStep;
    }

    /**
     * 1단계 설정
     *
     * @param firstStep 1단계
     * @return
     */
    public void setFirstStep(String firstStep) {
        this.firstStep = firstStep;
    }

    /**
     * 2단계 반환
     *
     * @return secondStep 2단계
     */
    public String getSecondStep() {
        return secondStep;
    }

    /**
     * 2단계 설정
     *
     * @param secondStep 2단계
     * @return
     */
    public void setSecondStep(String secondStep) {
        this.secondStep = secondStep;
    }

    /**
     * 3단계 반환
     *
     * @return thirdStep 3단계
     */
    public String getThirdStep() {
        return thirdStep;
    }

    /**
     * 3단계 설정
     *
     * @param thirdStep 3단계
     * @return
     */
    public void setThirdStep(String thirdStep) {
        this.thirdStep = thirdStep;
    }

    /**
     * 격자 X 반환
     *
     * @return division 구분
     */
    public String getGridX() {
        return gridX;
    }

    /**
     * 격자 X 설정
     *
     * @param gridX 격자 X
     * @return
     */
    public void setGridX(String gridX) {
        this.gridX = gridX;
    }

    /**
     * 격자 Y 반환
     *
     * @return gridY 격자 Y
     */
    public String getGridY() {
        return gridY;
    }

    /**
     * 격자 Y 설정
     *
     * @param gridY 격자 Y
     * @return
     */
    public void setGridY(String gridY) {
        this.gridY = gridY;
    }

    /**
     * 경도(시) 반환
     *
     * @return longitudeHour 경도(시)
     */
    public String getLongitudeHour() {
        return longitudeHour;
    }

    /**
     * 경도(시) 설정
     *
     * @param longitudeHour 경도(시)
     * @return
     */
    public void setLongitudeHour(String longitudeHour) {
        this.longitudeHour = longitudeHour;
    }

    /**
     * 경도(분) 반환
     *
     * @return longitudeMinute 경도(분)
     */
    public String getLongitudeMinute() {
        return longitudeMinute;
    }

    /**
     * 경도(분) 설정
     *
     * @param longitudeMinute 경도(분)
     * @return
     */
    public void setLongitudeMinute(String longitudeMinute) {
        this.longitudeMinute = longitudeMinute;
    }

    /**
     * 경도(초) 반환
     *
     * @return longitudeSecond 경도(초)
     */
    public String getLongitudeSecond() {
        return longitudeSecond;
    }

    /**
     * 경도(초) 설정
     *
     * @param longitudeSecond 경도(초)
     * @return
     */
    public void setLongitudeSecond(String longitudeSecond) {
        this.longitudeSecond = longitudeSecond;
    }

    /**
     * 위도(시) 반환
     *
     * @return longitudeSecond 경도(시)
     */
    public String getLatitudeHour() {
        return latitudeHour;
    }

    /**
     * 위도(시) 설정
     *
     * @param latitudeHour 위도(시)
     * @return
     */
    public void setLatitudeHour(String latitudeHour) {
        this.latitudeHour = latitudeHour;
    }

    /**
     * 위도(분) 반환
     *
     * @return latitudeMinute 위도(분)
     */
    public String getLatitudeMinute() {
        return latitudeMinute;
    }

    /**
     * 위도(분) 설정
     *
     * @param latitudeMinute 위도(분)
     * @return
     */
    public void setLatitudeMinute(String latitudeMinute) {
        this.latitudeMinute = latitudeMinute;
    }

    /**
     * 위도(초) 반환
     *
     * @return latitudeSecond 위도(초)
     */
    public String getLatitudeSecond() {
        return latitudeSecond;
    }

    /**
     * 위도(초) 설정
     *
     * @param latitudeSecond 위도(초)
     * @return
     */
    public void setLatitudeSecond(String latitudeSecond) {
        this.latitudeSecond = latitudeSecond;
    }

    /**
     * 경도(초/100) 반환
     *
     * @return latitudeMilli 경도(초/100)
     */
    public String getongitudeMilli() {
        return latitudeMilli;
    }

    /**
     * 경도(초/100) 설정
     *
     * @param longitudeMilli 경도(초/100)
     * @return
     */
    public void setLongitudeMilli(String longitudeMilli) {
        this.longitudeMilli = longitudeMilli;
    }

    /**
     * 위도(초/100) 반환
     *
     * @return latitudeMilli 위도(초/100)
     */
    public String getLatitudeMilli() {
        return latitudeMilli;
    }

    /**
     * 위도(초/100) 설정
     *
     * @param latitudeMilli 위도(초/100)
     * @return
     */
    public void setLatitudeMilli(String latitudeMilli) {
        this.latitudeMilli = latitudeMilli;
    }
}