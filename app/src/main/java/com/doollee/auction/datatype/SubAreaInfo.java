package com.doollee.auction.datatype;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

/**
 *
 * Class: SubAreaInfo
 * Created by wizard on 2021.02.22
 *
 * Description: 두번째 depth 지역 정보
 */

public class SubAreaInfo implements Serializable {
    private ArrayList<String> listDepth2Area;            // 두번째 depth 지역 리스트
    private HashMap<String, ArrayList> hmDepth3Area;     // 세번째 depth 지역 맵

    public SubAreaInfo() {

    }

    public SubAreaInfo(ArrayList<String> listDepth2Area, HashMap<String, ArrayList> hmDepth3Area) {
        this.listDepth2Area = listDepth2Area;
        this.hmDepth3Area = hmDepth3Area;
    }

    /**
     * 두번째 depth 지역 정보 반환
     *
     * @return listDepth2Menu 두번째 depth 지역 리스트
     */
    public ArrayList<String> getListDepth2Area() {
        return listDepth2Area;
    }

    /**
     * 두번째 depth 지역 정보 설정
     *
     * @param listDepth2Area 두번째 depth 지역 리스트
     * @return
     */
    public void setListDepth2Area(ArrayList<String> listDepth2Area) {
        this.listDepth2Area = listDepth2Area;
    }

    /**
     * 세번째 depth 지역 정보 반환
     *
     * @return hmDepth3Menu 세번째 depth 지역 맵
     */
    public HashMap<String, ArrayList> getListDepth3Area() {
        return hmDepth3Area;
    }

    /**
     * 세번째 depth 지역 정보 설정
     *
     * @param hmDepth3Area 세번째 depth 메뉴 맵
     * @return
     */
    public void setListDepth3Area(HashMap<String, ArrayList> hmDepth3Area) {
        this.hmDepth3Area = hmDepth3Area;
    }
}
