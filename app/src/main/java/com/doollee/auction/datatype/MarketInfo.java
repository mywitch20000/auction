package com.doollee.auction.datatype;

import java.io.Serializable;

/**
 *
 * Class: MarketInfo
 * Created by wizard on 2021.02.09
 *
 * Description: 시장 정보
 */

public class MarketInfo implements Serializable {
    private String   whsalMrktCode;     // 시장코드
    private String   whsalMrktNm;       // 시장명
    private String   whsalOldCode;      // 구시장코드
    private String   marketOldNm;       // 구시장명
    private String   cprCode;           // 법인코드
    private String   cprNM;             // 법인명
    private String   cprOldCode;        // 구법인코드
    private String   cprOldNM;          // 구법인명
    private String   updtDe;            // 업데이트일자

    public MarketInfo() {

    }

    public MarketInfo(String whsalMrktCode, String whsalMrktNm, String whsalOldCode, String marketOldNm) {
        this.whsalMrktCode = whsalMrktCode;
        this.whsalMrktNm = whsalMrktNm;
        this.whsalOldCode = whsalOldCode;
        this.marketOldNm = marketOldNm;
    }

    public MarketInfo(String whsalMrktCode, String whsalMrktNm, String whsalOldCode, String marketOldNm,
                      String cprCode, String cprNM, String cprOldCode, String cprOldNM) {
        this.whsalMrktCode = whsalMrktCode;
        this.whsalMrktNm = whsalMrktNm;
        this.whsalOldCode = whsalOldCode;
        this.marketOldNm = marketOldNm;
        this.cprCode = cprCode;
        this.cprNM = cprNM;
        this.cprOldCode = cprOldCode;
        this.cprOldNM = cprOldNM;
    }

    /**
     * 시장코드 반환
     *
     * @return whsalMrktCode 시장코드
     */
    public String getWhsalMrktCode() {
        return whsalMrktCode;
    }

    /**
     * 시장코드 설정
     *
     * @param whsalMrktCode 시장코드
     * @return
     */
    public void setWhsalMrktCode(String whsalMrktCode) {
        this.whsalMrktCode = whsalMrktCode;
    }

    /**
     * 시장명 반환
     *
     * @return whsalMrktNm 시장명
     */
    public String getWhsalMrktNm() {
        return whsalMrktNm;
    }

    /**
     * 시장명 설정
     *
     * @param whsalMrktNm 시장명
     * @return
     */
    public void setWhsalMrktNm(String whsalMrktNm) {
        this.whsalMrktNm = whsalMrktNm;
    }

    /**
     * 구시장코드 반환
     *
     * @return examinMrktCode 조사시장코드
     */
    public String getWhsalOldCode() {
        return whsalOldCode;
    }

    /**
     * 구시장코드 설정
     *
     * @param whsalOldCode 조사시장코드
     * @return
     */
    public void setWhsalOldCode(String whsalOldCode) {
        this.whsalOldCode = whsalOldCode;
    }

    /**
     * 구시장명 반환
     *
     * @return marketOldNm 구시장명
     */
    public String getMarketOldNm() {
        return marketOldNm;
    }

    /**
     * 구시장명 설정
     *
     * @param marketOldNm 구시장명
     * @return
     */
    public void setMarketOldNm(String marketOldNm) {
        this.marketOldNm = marketOldNm;
    }

    /**
     * 법인코드 반환
     *
     * @return cprCode 법인코드
     */
    public String getCprCode() {
        return cprCode;
    }

    /**
     * 법인코드 설정
     *
     * @param cprCode 법인코드
     * @return
     */
    public void setCprCode(String cprCode) {
        this.cprCode = cprCode;
    }

    /**
     * 법인명 반환
     *
     * @return cprNM 법인명
     */
    public String getCprNM() {
        return cprNM;
    }

    /**
     * 법인명 설정
     *
     * @param cprNM 법인명
     * @return
     */
    public void setCprNM(String cprNM) {
        this.cprNM = cprNM;
    }

    /**
     * 구법인코드 반환
     *
     * @return cprOldCode 구법인코드
     */
    public String getCprOldCode() {
        return cprOldCode;
    }

    /**
     * 구법인코드 설정
     *
     * @param cprOldCode 구법인코드
     * @return
     */
    public void setCprOldCode(String cprOldCode) {
        this.cprOldCode = cprOldCode;
    }

    /**
     * 구법인명 반환
     *
     * @return cprOldNM 구법인명
     */
    public String getCprOldNM() {
        return cprOldNM;
    }

    /**
     * 구법인명 설정
     *
     * @param cprOldNM 업데이트구법인명일자
     * @return
     */
    public void setCprOldNM(String cprOldNM) {
        this.cprOldNM = cprOldNM;
    }

    /**
     * 업데이트일자 반환
     *
     * @return updtDe 업데이트일자
     */
    public String getUpdtDe() {
        return updtDe;
    }

    /**
     * 업데이트일자 설정
     *
     * @param updtDe 업데이트일자
     * @return
     */
    public void setUpdtDe(String updtDe) {
        this.updtDe = updtDe;
    }
}
