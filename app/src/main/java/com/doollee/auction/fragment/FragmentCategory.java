package com.doollee.auction.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.doollee.auction.R;
import com.doollee.auction.activity.PriceBidActivity;
import com.doollee.auction.adapter.MainCategoryAdapter;
import com.doollee.auction.adapter.MainMarketAdapter;
import com.doollee.auction.adapter.SubCategoryAdapter;
import com.doollee.auction.adapter.SubMarketAdapter;
import com.doollee.auction.common.Constants;
import com.doollee.auction.common.net.HttpSenderTask;
import com.doollee.auction.common.net.HttpUtils;
import com.doollee.auction.datatype.CategoryInfo;
import com.doollee.auction.datatype.MarketInfo;
import com.doollee.auction.datatype.PriceBidInfo;
import com.doollee.auction.ui.OnSingleClickListener;
import com.doollee.auction.utils.DialogUtils;
import com.doollee.auction.utils.Logs;
import com.opencsv.CSVReader;
import com.orhanobut.dialogplus.DialogPlus;
import com.orhanobut.dialogplus.OnItemClickListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 *
 * Class: FragmentBase
 * Created by wizard on 2021.02.15
 *
 * Description: 가격조회 부류 기준 fragment
 */

public class FragmentCategory extends FragmentBase {
    private TextView        mTextDate;
    private TextView        mTextMainMarket;
    private TextView        mTextSubMarket;
    private TextView        mTextMainCategory;
    private TextView        mTextSubCategory;
    private RelativeLayout  mLayoutCalendar;
    private CalendarView    mCalendar;

    private String          mSelectedDate;
    private String          mSelectedMainMarketCode;
    private String          mSelectedSubMarketCode;
    private String          mSelectedMainCategoryCode;
    private String          mSelectedSubCategoryCode;

    private int             mTotalCount = 0;

    private final ArrayList<MarketInfo>                    mListMainMarketInfo = new ArrayList<>();
    private final HashMap<String, ArrayList<MarketInfo>>   hmSubMarketInfo = new HashMap<>();
    private final ArrayList<CategoryInfo>                  mListMainCategoryInfo = new ArrayList<>();
    private final HashMap<String, ArrayList<CategoryInfo>> hmSubCategoryInfo = new HashMap<>();


    private MainMarketAdapter      mMainMarketAdapter;
    private MainCategoryAdapter    mMainCategoryAdapter;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup rootView = (ViewGroup) inflater.inflate(R.layout.fragment_inquiry_category, container, false);

        initUX(rootView);
        setMarket();
        setCategory();

        return rootView;
    }

    private void initUX(ViewGroup rootView) {
        mTextDate = rootView.findViewById(R.id.text_date_in_price_category);
        mTextMainMarket = rootView.findViewById(R.id.text_main_market_in_price_category);
        mTextSubMarket = rootView.findViewById(R.id.text_sub_market_in_price_category);
        mTextMainCategory = rootView.findViewById(R.id.text_main_category_in_price_category);
        mTextSubCategory = rootView.findViewById(R.id.text_sub_category_in_price_category);
        Button btnInquiryPrice = rootView.findViewById(R.id.btn_inquiry_price_in_price_category);
        mLayoutCalendar = rootView.findViewById(R.id.layout_calendar_in_price_category);
        mCalendar = rootView.findViewById(R.id.calendar_in_price_category);
        Button cancelCalendar = rootView.findViewById(R.id.btn_cancel_calendar_in_price_category);
        Button confirmCalendar = rootView.findViewById(R.id.btn_confirm_calendar_in_price_category);

        Date date = new Date();
        mSelectedDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        mTextDate.setText(mSelectedDate);

        mTextDate.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (!TextUtils.isEmpty(mTextDate.getText().toString())) {
                    String parts[] = mSelectedDate.split("-");

                    int year = Integer.parseInt(parts[0]);
                    int month = Integer.parseInt(parts[1]);
                    int day = Integer.parseInt(parts[2]);

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, (month - 1));
                    calendar.set(Calendar.DAY_OF_MONTH, day);

                    long milliTime = calendar.getTimeInMillis();
                    mCalendar.setDate(milliTime,true,true);
                }

                mLayoutCalendar.setVisibility(View.VISIBLE);
            }
        });

        mTextMainMarket.setText(getString(R.string.all));
        mTextMainMarket.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                showMainMarketList();
            }
        });

        mTextSubMarket.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String mainMarketName = mTextMainMarket.getText().toString();
                if (TextUtils.isEmpty(mainMarketName) || getString(R.string.all).equals(mainMarketName)) {
                    DialogUtils.alert(mContext, getString(R.string.msg_not_input_main_market));
                    return;
                }

                showSubMarketList(mSelectedMainMarketCode);
            }
        });

        mTextMainCategory.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                showMainCategoryList();
            }
        });

        mTextSubCategory.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String mainCategory = mTextMainCategory.getText().toString();
                if (TextUtils.isEmpty(mainCategory)) {
                    DialogUtils.alert(mContext, getString(R.string.msg_not_input_main_category));
                    return;
                }

                showSubCategoryList(mSelectedMainCategoryCode);
            }
        });

        btnInquiryPrice.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                String mainCategory = mTextMainCategory.getText().toString();
                if (TextUtils.isEmpty(mainCategory)) {
                    DialogUtils.alert(mContext, getString(R.string.msg_not_input_main_category));
                    return;
                }

                String subCategory = mTextSubCategory.getText().toString();
                if (TextUtils.isEmpty(subCategory)) {
                    DialogUtils.alert(mContext, getString(R.string.msg_not_input_sub_category));
                    return;
                }

                requestPrice();
            }
        });

        mCalendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                Calendar cal = new GregorianCalendar();
                cal.set(year, month, dayOfMonth);

                Calendar curr = new GregorianCalendar(); // current cal
                curr.setTimeInMillis(System.currentTimeMillis());

                if (cal.getTimeInMillis() > curr.getTimeInMillis()) {
                    Toast.makeText(mContext, getString(R.string.msg_over_today), Toast.LENGTH_SHORT).show();
                    mCalendar.setDate(Calendar.getInstance().getTimeInMillis(),false,true);
                    return;
                }

                mSelectedDate = String.format("%4d-%02d-%02d", year, month + 1, dayOfMonth);
            }
        });

        cancelCalendar.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                mLayoutCalendar.setVisibility(View.GONE);
            }
        });

        confirmCalendar.setOnClickListener(new OnSingleClickListener() {
            @Override
            public void onSingleClick(View v) {
                if (TextUtils.isEmpty(mSelectedDate)) {
                    mSelectedDate = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
                }

                mTextDate.setText(mSelectedDate);
                mLayoutCalendar.setVisibility(View.GONE);
            }
        });
    }

    private void setMarket() {
        mListMainMarketInfo.clear();

        try {
            InputStreamReader is = new InputStreamReader(getResources().openRawResource(R.raw.market_list), "EUC-KR");
            BufferedReader reader = new BufferedReader(is);
            CSVReader csvReader = new CSVReader(reader);

            String[] nextLine;
            int countLine = 0;

            String depth01Code = "";
            String depth02Code = "";
            ArrayList<MarketInfo> listSubMarketInfo = new ArrayList<>();

            MarketInfo info = new MarketInfo(getString(R.string.all_code), getString(R.string.all), getString(R.string.all_code), getString(R.string.all));
            mListMainMarketInfo.add(info);

            while ((nextLine = csvReader.readNext()) != null) {
                countLine++;
                if (countLine < 3)
                    continue;

                if (nextLine.length != 9)
                    continue;

                MarketInfo marketInfo = new MarketInfo();
                String whsalMrktCode = nextLine[0];
                String whsalMrktNm = nextLine[1];
                String whsalOldCode = nextLine[2];
                String marketOldNm = nextLine[3];
                String cprCode = nextLine[4];
                String cprNM = nextLine[5];
                String cprOldCode = nextLine[6];
                String cprOldNM = nextLine[7];
                String updtDe = nextLine[8];

                if (TextUtils.isEmpty(whsalOldCode) || TextUtils.isEmpty(marketOldNm))
                    continue;

                marketInfo.setWhsalMrktCode(whsalMrktCode);
                marketInfo.setWhsalMrktNm(whsalMrktNm);
                marketInfo.setWhsalOldCode(whsalOldCode);
                marketInfo.setMarketOldNm(marketOldNm);
                marketInfo.setCprCode(cprCode);
                marketInfo.setCprNM(cprNM);
                marketInfo.setCprOldCode(cprOldCode);
                marketInfo.setCprOldNM(cprOldNM);
                marketInfo.setCprNM(cprNM);
                marketInfo.setUpdtDe(updtDe);

                if (!depth01Code.equals(whsalOldCode)) {
                    MarketInfo mainMarket = new MarketInfo(whsalMrktCode, whsalMrktNm, whsalOldCode, marketOldNm);
                    mListMainMarketInfo.add(mainMarket);

                    if (!TextUtils.isEmpty(depth01Code)) {
                        ArrayList<MarketInfo> listMainMarket = new ArrayList<>();
                        for (Object item : listSubMarketInfo) {
                            if (item instanceof MarketInfo) {
                                listMainMarket.add((MarketInfo) item);
                            }
                        }

                        if (listMainMarket.size() > 0)
                            hmSubMarketInfo.put(depth01Code, listMainMarket);

                        listSubMarketInfo.clear();
                    }
                }

                if (!depth02Code.equals(cprOldCode)) {
                    MarketInfo subMarketInfo = new MarketInfo(whsalMrktCode, whsalMrktNm, whsalOldCode, marketOldNm, cprCode, cprNM, cprOldCode, cprOldNM);
                    listSubMarketInfo.add(subMarketInfo);
                }

                depth01Code = whsalOldCode;
                depth02Code = cprOldCode;
            }

            if (mListMainMarketInfo.size() <= 0)
                return;

            if (mMainMarketAdapter == null) {
                mMainMarketAdapter = new MainMarketAdapter(mContext, mListMainMarketInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showMainMarketList() {
        if (mMainMarketAdapter == null)
            return;

        DialogPlus dialog = DialogPlus.newDialog(mContext)
                .setAdapter(mMainMarketAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        MarketInfo marketInfo = (MarketInfo) item;
                        String stdMrktNm = marketInfo.getMarketOldNm();
                        mSelectedMainMarketCode = marketInfo.getWhsalOldCode();
                        if (!TextUtils.isEmpty(stdMrktNm))
                            mTextMainMarket.setText(stdMrktNm);

                        mTextSubMarket.setText("");
                        dialog.dismiss();
                    }
                })
                .setHeader(R.layout.layout_market_header)
                .setExpanded(true)
                .create();
        dialog.show();
    }

    private void showSubMarketList(String mainMarketCode) {
        ArrayList<MarketInfo> listMarketInfo = hmSubMarketInfo.get(mainMarketCode);
        if (listMarketInfo == null || listMarketInfo.size() <= 0)
            return;

        SubMarketAdapter adapter = new SubMarketAdapter(mContext, listMarketInfo);

        DialogPlus dialog = DialogPlus.newDialog(mContext)
                .setAdapter(adapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        MarketInfo marketInfo = (MarketInfo) item;
                        String subMarketName = marketInfo.getCprOldNM();
                        String subMarketCode = marketInfo.getCprOldCode();
                        if (!TextUtils.isEmpty(subMarketName))
                            mTextSubMarket.setText(subMarketName);

                        mSelectedSubMarketCode = subMarketCode;
                        dialog.dismiss();
                    }
                })
                .setHeader(R.layout.layout_sub_category_header)
                .setExpanded(true)
                .create();
        dialog.show();
    }

    private void setCategory() {
        mListMainCategoryInfo.clear();

        try {
            InputStreamReader is = new InputStreamReader(getResources().openRawResource(R.raw.category_code), "EUC-KR");
            BufferedReader reader = new BufferedReader(is);
            CSVReader csvReader = new CSVReader(reader);

            String[] nextLine;
            int countLine = 0;

            String depth01Code = "";
            String depth02Code = "";

            ArrayList<CategoryInfo> listSubCategoryInfo = new ArrayList<>();

            while ((nextLine = csvReader.readNext()) != null) {
                countLine++;
                if (countLine == 1)
                    continue;

                if (nextLine.length != 6)
                    continue;

                String mainCategoryCode = nextLine[0];
                String mainCategoryName = nextLine[1];
                String subCategoryCode = nextLine[2];
                String subCategoryName = nextLine[3];
                String varietiesCode = nextLine[4];
                String varietiesName = nextLine[5];

                if (TextUtils.isEmpty(mainCategoryCode) || TextUtils.isEmpty(subCategoryCode) ||
                        TextUtils.isEmpty(varietiesCode))
                    continue;

                CategoryInfo categoryInfo = new CategoryInfo();
                categoryInfo.setMainCategoryCode(mainCategoryCode);
                categoryInfo.setMainCategoryName(mainCategoryName);
                categoryInfo.setSubCategoryCode(subCategoryCode);
                categoryInfo.setSubCategoryName(subCategoryName);
                categoryInfo.setVarietiesCode(varietiesCode);
                categoryInfo.setVarietiesName(varietiesName);

                //mListCategoryInfo.add(categoryInfo);

                if (!depth01Code.equals(mainCategoryCode)) {
                    CategoryInfo mainCategory = new CategoryInfo(mainCategoryCode, mainCategoryName);
                    mListMainCategoryInfo.add(mainCategory);

                    if (!TextUtils.isEmpty(depth01Code)) {
                        ArrayList<CategoryInfo> listCategory = new ArrayList<>();
                        for (Object item : listSubCategoryInfo) {
                            if (item instanceof CategoryInfo) {
                                listCategory.add((CategoryInfo) item);
                            }
                        }

                        if (listCategory.size() > 0)
                            hmSubCategoryInfo.put(depth01Code, listCategory);

                        listSubCategoryInfo.clear();
                    }
                }

                if (!depth02Code.equals(subCategoryCode)) {
                    CategoryInfo subCategoryInfo = new CategoryInfo(mainCategoryCode, mainCategoryName, subCategoryCode, subCategoryName);
                    listSubCategoryInfo.add(subCategoryInfo);
                }

                depth01Code = mainCategoryCode;
                depth02Code = subCategoryCode;
            }

            if (mListMainCategoryInfo.size() <= 0)
                return;

            if (mMainCategoryAdapter == null) {
                mMainCategoryAdapter = new MainCategoryAdapter(mContext, mListMainCategoryInfo);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showMainCategoryList() {
        if (mMainCategoryAdapter == null)
            return;

        DialogPlus dialog = DialogPlus.newDialog(mContext)
                .setAdapter(mMainCategoryAdapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        CategoryInfo categoryInfo = (CategoryInfo) item;
                        String mainCategoryName = categoryInfo.getMainCategoryName();
                        mSelectedMainCategoryCode = categoryInfo.getMainCategoryCode();
                        if (!TextUtils.isEmpty(mainCategoryName))
                            mTextMainCategory.setText(mainCategoryName);

                        mTextSubCategory.setText("");
                        dialog.dismiss();
                    }
                })
                .setHeader(R.layout.layout_main_category_header)
                .setExpanded(true)
                .create();
        dialog.show();
    }

    private void showSubCategoryList(String mainCategoryCode) {
        ArrayList<CategoryInfo> listCategoryInfo = hmSubCategoryInfo.get(mainCategoryCode);
        if (listCategoryInfo == null || listCategoryInfo.size() <= 0)
            return;

        SubCategoryAdapter adapter = new SubCategoryAdapter(mContext, listCategoryInfo);

        DialogPlus dialog = DialogPlus.newDialog(mContext)
                .setAdapter(adapter)
                .setOnItemClickListener(new OnItemClickListener() {
                    @Override
                    public void onItemClick(DialogPlus dialog, Object item, View view, int position) {
                        CategoryInfo categoryInfo = (CategoryInfo) item;
                        String subCategoryName = categoryInfo.getSubCategoryName();
                        String subCategoryCode = categoryInfo.getSubCategoryCode();
                        if (!TextUtils.isEmpty(subCategoryName))
                            mTextSubCategory.setText(subCategoryName);

                        mSelectedSubCategoryCode = subCategoryCode;

                        dialog.dismiss();
                    }
                })
                .setHeader(R.layout.layout_sub_category_header)
                .setExpanded(true)
                .create();
        dialog.show();
    }

    private void requestPrice() {
        if (TextUtils.isEmpty(mSelectedSubCategoryCode)) {
            return;
        }

        String date = mTextDate.getText().toString();
        final String mainMarketName = mTextMainMarket.getText().toString();
        final String subMarketName = mTextSubMarket.getText().toString();
        if (!TextUtils.isEmpty(date)) {
            date = date.replaceAll("-", "");
        } else {
            date = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(new Date());
        }

        StringBuilder urlBuilder = new StringBuilder(Constants.URL_PRICE_OPEN_API + Constants.PATH_PRICE_CATEGORY);
        try {
            urlBuilder.append("?").append(URLEncoder.encode("ServiceKey", "UTF-8"));
            urlBuilder.append("=");
            urlBuilder.append(Constants.KEY_OPEN_API);
            urlBuilder.append("&");
            urlBuilder.append("pageNo");
            urlBuilder.append("=");
            urlBuilder.append(1);
            urlBuilder.append("&");
            urlBuilder.append("numOfRows");
            urlBuilder.append("=");
            urlBuilder.append(Constants.NUM_OF_ROWS);
            urlBuilder.append("&");
            urlBuilder.append("delngDe");
            urlBuilder.append("=");
            urlBuilder.append(date);
            urlBuilder.append("&");
            urlBuilder.append("prdlstCd");
            urlBuilder.append("=");
            urlBuilder.append(mSelectedSubCategoryCode);

            if (!TextUtils.isEmpty(mainMarketName) && !getString(R.string.all).equals(mainMarketName)) {
                urlBuilder.append("&");
                urlBuilder.append("whsalCd");
                urlBuilder.append("=");
                urlBuilder.append(mSelectedMainMarketCode);
            }

            if (!TextUtils.isEmpty(subMarketName)) {
                urlBuilder.append("&");
                urlBuilder.append("cprCd");
                urlBuilder.append("=");
                urlBuilder.append(mSelectedSubMarketCode);
            }
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        showProgressDialog();

        Map<String, Object> param = new HashMap<>();
        HttpUtils.sendHttpTask(urlBuilder.toString(), param, new HttpSenderTask.HttpRequestListener() {
            @Override
            public void endHttpRequest(String ret) {
                dismissProgressDialog();

                try {
                    JSONObject jsonObject = new JSONObject(ret);
                    JSONObject objectResponse = jsonObject.getJSONObject("response");
                    JSONObject objectHeader = objectResponse.getJSONObject("header");

                    ArrayList<PriceBidInfo> listPriceBid = new ArrayList<>();

                    String resultCode = objectHeader.getString("resultCode");
                    if (Constants.DefineValue.SUCCESS_CODE.equals(resultCode)) {
                        JSONObject objectBody = objectResponse.getJSONObject("body");

                        mTotalCount = objectBody.getInt("totalCount");
                        if (mTotalCount == 0) {
                            DialogUtils.alert(mContext, getString(R.string.msg_not_no_inquiry_price));
                            return;
                        }

                        JSONObject objectItems = objectBody.getJSONObject("items");

                        int pageNo = objectBody.getInt("pageNo");
                        int numOfRows = objectBody.getInt("numOfRows");

                        if (pageNo == 1 && numOfRows == 1) {
                            JSONObject objectItem = objectItems.getJSONObject("item");

                            String delngDe = objectItem.getString("delngDe");
                            PriceBidInfo priceBidInfo = setBiddingItem(objectItem);
                            listPriceBid.add(priceBidInfo);

                        } else {
                            JSONArray arrayItem = objectItems.getJSONArray("item");
                            if (arrayItem.length() <= 0) {
                                return;
                            }

                            for (int index = 0; index < arrayItem.length(); index++) {
                                JSONObject objectItem = (JSONObject) arrayItem.get(index);
                                PriceBidInfo priceBidInfo = setBiddingItem(objectItem);
                                listPriceBid.add(priceBidInfo);
                            }
                        }

                        String inputDate = mTextDate.getText().toString();
                        if (!TextUtils.isEmpty(inputDate)) {
                            inputDate = inputDate.replaceAll("-", "");
                        } else {
                            inputDate = new SimpleDateFormat("yyyyMMdd", Locale.getDefault()).format(new Date());
                        }

                        Intent intent = new Intent(mContext, PriceBidActivity.class);
                        intent.putExtra(Constants.PriceBid.INQUIRY_TYPE, Constants.PriceBid.INQUIRY_TYPE_CATEGORY);
                        intent.putExtra(Constants.PriceBid.DATE, inputDate);

                        if (!TextUtils.isEmpty(mSelectedMainMarketCode))
                            intent.putExtra(Constants.PriceBid.MAIN_MARKET_CODE, mSelectedMainMarketCode);

                        if (!TextUtils.isEmpty(mSelectedSubMarketCode))
                            intent.putExtra(Constants.PriceBid.SUB_MARKET_CODE, mSelectedSubMarketCode);

                        intent.putExtra(Constants.PriceBid.SUB_CATEGORY_CODE, mSelectedSubCategoryCode);
                        intent.putExtra(Constants.PriceBid.TOTAL_PRICE_BID, mTotalCount);
                        intent.putExtra(Constants.PriceBid.LIST_PRICE_BID, listPriceBid);
                        startActivity(intent);
                    } else {
                        DialogUtils.alert(mContext, getString(R.string.msg_not_no_inquiry_price));
                    }
                } catch (JSONException e) {
                    Logs.d(e.getMessage());
                }
            }
        });
    }

    private PriceBidInfo setBiddingItem(JSONObject objectItem) {
        PriceBidInfo info = new PriceBidInfo();

        try {
            if (objectItem.has("delngDe")) {
                String delngDe = objectItem.getString("delngDe");
                info.setDelngDe(delngDe);
            }

            if (objectItem.has("sbidTime")) {
                String sbidTime = objectItem.getString("sbidTime");
                info.setSbidTime(sbidTime);
            }

            if (objectItem.has("aucSeCode")) {
                int aucSeCode = objectItem.getInt("aucSeCode");
                info.setAucSeCode(String.valueOf(aucSeCode));
            }

            if (objectItem.has("aucSeNm")) {
                String aucSeNm = objectItem.getString("aucSeNm");
                info.setAucSeCode(aucSeNm);
            }

            if (objectItem.has("whsalMrktNewCode")) {
                String whsalMrktNewCode = objectItem.getString("whsalMrktNewCode");
                info.setWhsalMrktNewCode(whsalMrktNewCode);
            }

            if (objectItem.has("whsalMrktNewNm")) {
                String whsalMrktNewNm = objectItem.getString("whsalMrktNewNm");
                info.setWhsalMrktNewNm(whsalMrktNewNm);
            }

            if (objectItem.has("cprInsttNewCode")) {
                String cprInsttNewCode = objectItem.getString("cprInsttNewCode");
                info.setCprInsttNewCode(cprInsttNewCode);
            }

            if (objectItem.has("insttNewNm")) {
                String insttNewNm = objectItem.getString("insttNewNm");
                info.setInsttNewNm(insttNewNm);
            }

            if (objectItem.has("ledgNo")) {
                String ledgNo = objectItem.getString("ledgNo");
                info.setLedgNo(ledgNo);
            }

            if (objectItem.has("sleSeqn")) {
                String sleSeqn = objectItem.getString("sleSeqn");
                info.setSleSeqn(sleSeqn);
            }

            if (objectItem.has("stdPrdlstNewCode")) {
                String stdPrdlstNewCode = objectItem.getString("stdPrdlstNewCode");
                info.setStdPrdlstNewCode(stdPrdlstNewCode);
            }

            if (objectItem.has("stdPrdlstNewNm")) {
                String stdPrdlstNewNm = objectItem.getString("stdPrdlstNewNm");
                info.setStdPrdlstNewNm(stdPrdlstNewNm);
            }

            if (objectItem.has("stdSpciesNewCode")) {
                String stdSpciesNewCode = objectItem.getString("stdSpciesNewCode");
                info.setStdSpciesNewCode(stdSpciesNewCode);
            }

            if (objectItem.has("stdSpciesNewNm")) {
                String stdSpciesNewNm = objectItem.getString("stdSpciesNewNm");
                info.setStdSpciesNewNm(stdSpciesNewNm);
            }

            if (objectItem.has("delngPrut")) {
                String delngPrut = objectItem.getString("delngPrut");
                info.setDelngPrut(delngPrut);
            }

            if (objectItem.has("stdUnitNewCode")) {
                String stdUnitNewCode = objectItem.getString("stdUnitNewCode");
                info.setStdUnitNewCode(stdUnitNewCode);
            }

            if (objectItem.has("stdUnitNewNm")) {
                String stdUnitNewNm = objectItem.getString("stdUnitNewNm");
                info.setStdUnitNewNm(stdUnitNewNm);
            }

            if (objectItem.has("stdFrmlcNewCode")) {
                String stdFrmlcNewCode = objectItem.getString("stdFrmlcNewCode");
                info.setStdFrmlcNewCode(stdFrmlcNewCode);
            }

            if (objectItem.has("stdFrmlcNewNm")) {
                String stdFrmlcNewNm = objectItem.getString("stdFrmlcNewNm");
                info.setStdFrmlcNewNm(stdFrmlcNewNm);
            }

            if (objectItem.has("sbidPric")) {
                String sbidPric = objectItem.getString("sbidPric");
                info.setSbidPric(sbidPric);
            }

            if (objectItem.has("delngQy")) {
                String delngQy = objectItem.getString("delngQy");
                info.setDelngQy(delngQy);
            }

            if (objectItem.has("cprMtcNm")) {
                String cprMtcNm = objectItem.getString("cprMtcNm");
                info.setCprMtcNm(cprMtcNm);
            }

            /*String catgoryNewCode = objectItem.getString("catgoryNewCode");
            String catgoryNewNm = objectItem.getString("catgoryNewNm");
            String stdMgNewCode = objectItem.getString("stdMgNewCode");
            String stdMgNewNm = objectItem.getString("stdMgNewNm");
            String stdQlityNewCode = objectItem.getString("stdQlityNewCode");
            String stdQlityNewNm = objectItem.getString("stdQlityNewNm");
            String cprUsePrdlstCode = objectItem.getString("cprUsePrdlstCode");
            String cprUsePrdlstNm = objectItem.getString("cprUsePrdlstNm");
            String shipmntSeCode = objectItem.getString("shipmntSeCode");
            String shipmntSeNm = objectItem.getString("shipmntSeNm");
            String stdMtcNewCode = objectItem.getString("stdMtcNewCode");
            */
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return info;
    }
}
