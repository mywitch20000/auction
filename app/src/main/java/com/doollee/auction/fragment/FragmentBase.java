package com.doollee.auction.fragment;

import android.app.Activity;
import android.content.Context;

import androidx.fragment.app.Fragment;

import com.doollee.auction.ui.ProgressExDialog;

/**
 *
 * Class: FragmentBase
 * Created by wizard on 2021.02.15
 *
 * Description: 기본 fragment
 */

public class FragmentBase extends Fragment {
    public Context   mContext;
    public Activity  mActivity;

    private ProgressExDialog mProgressExDialog;       // 진행바 표시창

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

        mContext = context;

        if (context instanceof Activity)
            mActivity = (Activity) context;
    }

    /**
     * 진행바 출력
     */
    public void showProgressDialog() {
        dismissProgressDialog();

        if (mActivity == null || mActivity.isFinishing())
            return;

        if (mProgressExDialog == null) {
            mProgressExDialog = new ProgressExDialog(mContext);
            mProgressExDialog.show();
        }
    }

    /**
     * 진행바 종료
     */
    public void dismissProgressDialog() {
        if (mProgressExDialog != null && mProgressExDialog.isShowing()) {
            mProgressExDialog.dismiss();
            mProgressExDialog = null;
        }
    }
}
