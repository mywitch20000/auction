package com.doollee.auction.manager;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkRequest;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.net.wifi.WifiNetworkSpecifier;
import android.os.Build;
import android.provider.Settings;

import androidx.annotation.NonNull;

import com.doollee.auction.common.Constants;
import com.doollee.auction.ui.BaseActivity;
import com.doollee.auction.utils.Logs;

public class SettingsManager {
    private static SettingsManager instance = null;
    private SettingsListener listener = null;

    private Context      context;

    private WifiManager  wifiManager;

    /**
     * Singleton instance 생성
     *
     * @return instance
     */
    public static SettingsManager getInstance(Context context) {
        if (instance == null) {
            synchronized (SettingsManager.class) {
                if (instance == null) {
                    instance = new SettingsManager(context);
                }
            }
        }
        return instance;
    }

    public SettingsManager(Context context) {
        this.context = context;
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
    }

    public WifiManager getWifiManager() {
        return wifiManager;
    }

    // 비행기 모드 실행 여부
    public boolean isAirplaneModeOn() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            return Settings.System.getInt(context.getContentResolver(),
                    Settings.System.AIRPLANE_MODE_ON, 0) != 0;
        } else {
            return Settings.Global.getInt(context.getContentResolver(),
                    Settings.Global.AIRPLANE_MODE_ON, 0) != 0;
        }
    }

    public boolean isWifiOn() {
        return wifiManager.isWifiEnabled();
    }

    public boolean isWifiOnAndConnected() {
        if (wifiManager.isWifiEnabled()) {
            WifiInfo wifiInfo = wifiManager.getConnectionInfo();
            if (wifiInfo.getNetworkId() == -1) {
                return false; // Not connected to an access point
            }
            return true; // Connected to an access point
        } else {
            return false; // Wi-Fi adapter is OFF
        }
    }

    public void addAuctionAP() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            WifiNetworkSpecifier.Builder builder = new WifiNetworkSpecifier.Builder();
            builder.setSsid(Constants.WIFI_SSID);
            builder.setWpa2Passphrase("doollee1!");

            WifiNetworkSpecifier wifiNetworkSpecifier = builder.build();
            NetworkRequest.Builder networkRequestBuilder = new NetworkRequest.Builder();
            networkRequestBuilder.addTransportType(NetworkCapabilities.TRANSPORT_WIFI);
            networkRequestBuilder.setNetworkSpecifier(wifiNetworkSpecifier);
            NetworkRequest networkRequest = networkRequestBuilder.build();
            final ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            if (cm != null) {
                cm.requestNetwork(networkRequest, new ConnectivityManager.NetworkCallback() {
                    @Override
                    public void onAvailable(@NonNull Network network) {
                        super.onAvailable(network);
                        Logs.d("onAvailable");
                        cm.bindProcessToNetwork(network);
                    }

                    @Override public void onUnavailable() {
                        Logs.d("onUnavailable");
                    }
                });
            }
        }
    }

    /**
     * Settings 리스너를 등록한다.
     *
     * @param listener
     */
    public void setSettingsCompleteListner(SettingsListener listener) {
        this.listener = listener;
    }

    public interface SettingsListener {
        void onCompleteSettings(String code);
    }
}