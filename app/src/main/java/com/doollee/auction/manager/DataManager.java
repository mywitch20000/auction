package com.doollee.auction.manager;
import android.util.ArrayMap;

import java.net.Socket;
import java.util.Map;

public class DataManager {
    private static DataManager instance = null;

    private Map map = new ArrayMap();
    private Map socketMap = new ArrayMap();

    /**
     * Singleton instance 생성
     *
     * @return instance
     */
    public static DataManager getInstance() {
        if (instance == null) {
            synchronized (DataManager.class) {
                if (instance == null) {
                    instance = new DataManager();
                }
            }
        }
        return instance;
    }

    public DataManager() {

    }

    /**
     * 전역변수 저장
     * @param sKey
     * @param sItem
     */
    public void setData(Object sKey, Object sItem) {
        map.put(sKey, sItem);
    }

    /**
     * 저장된 전역변수 조회
     * @param sKey
     * @return
     */
    public Object getData(Object sKey) {
        if (!map.containsKey(sKey))
            return "";

        return map.get(sKey);
    }

    /**
     * 전역변수 저장
     */
    public void clearSocketMap() {
        socketMap.clear();
    }


    /**
     * 전역변수 저장
     */
    public Map getSocketMap() {
        return socketMap;
    }

    /**
     * 전역변수 저장
     * @param sKey
     * @param sItem
     */
    public void setSocketMapData(Integer sKey, Socket sItem) {
        socketMap.put(sKey, sItem);
    }

    /**
     * 저장된 전역변수 조회
     * @param sKey
     * @return
     */
    public Socket getSocketMapData(Integer sKey) {
        if (!socketMap.containsKey(sKey))
            return null;

        return (Socket) socketMap.get(sKey);
    }
}