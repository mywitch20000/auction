package com.doollee.auction.manager;
import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

import androidx.annotation.Nullable;

import com.doollee.auction.common.net.SvrSocket;

/**
 * 강제종료시 해당 클래스의 onTaskRemoved Event발생.
 * 앱 실행시 초기 실행되는 Activity에서 클래스 실행 필요.
 * onDestroy Event도 발생하지만 강제종료시에도 발생되게 하려면 위 작업 필요.
 */
public class ForecdTerminationService extends Service {
    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onTaskRemoved(Intent rootIntent) {
        //SvrSocket.close("ALL"); //소켓모두닫기

        stopSelf(); //서비스 종료
    }
}