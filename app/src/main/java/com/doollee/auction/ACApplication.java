package com.doollee.auction;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.multidex.MultiDexApplication;

import com.doollee.auction.common.net.SocketRecver;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class ACApplication extends MultiDexApplication implements Application.ActivityLifecycleCallbacks {
    private Map<String, Activity> allActivity;

    @Override
    public void onCreate() {
        super.onCreate();

        allActivity = new HashMap<String, Activity>();
        registerActivityLifecycleCallbacks(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();

        unregisterActivityLifecycleCallbacks(this);
    }

    @Override
    public void onActivityCreated(@NonNull Activity activity, @Nullable Bundle savedInstanceState) {
        allActivity.put(activity.toString(), activity);
    }

    @Override
    public void onActivityStarted(@NonNull Activity activity) {

    }

    @Override
    public void onActivityResumed(@NonNull Activity activity) {

    }

    @Override
    public void onActivityPaused(@NonNull Activity activity) {

    }

    @Override
    public void onActivityStopped(@NonNull Activity activity) {

    }

    @Override
    public void onActivitySaveInstanceState(@NonNull Activity activity, @NonNull Bundle outState) {

    }

    @Override
    public void onActivityDestroyed(@NonNull Activity activity) {
        allActivity.remove(activity.toString());
    }


    /**
     * 모든 떠있는 화면을 한번에 종료한다.
     *
     * @param includeMain : TRUE이면 메인화면까지 닫아버린다. FALSE이면 메인 화면은 남겨둔다.
     */
    public void allActivityFinish(boolean includeMain) {
        if (SocketRecver.getInstance().getIsRunning()) {
            SocketRecver.getInstance().stopTask();
        }

        Iterator<String> keys = this.allActivity.keySet().iterator();
        while (keys.hasNext()) {
            String key = keys.next();
            if(key.contains(".MainActivity") && !includeMain) {
                continue;
            }

            Activity activity = this.allActivity.get(key);
            if (activity != null && !activity.isFinishing()) {
                /*if (BuildConfig.FORCE_STOP) {
                    if (includeMain) {
                        activity.moveTaskToBack(true);     // 태스크를 백그라운드로 이동
                        activity.finishAndRemoveTask();    // 액티비티 종료 + 태스크 리스트에서 지우기
                    } else {
                        activity.finish();
                    }
                } else*/ {
                    activity.finish();
                }
            }
        }

        if (includeMain)
            android.os.Process.killProcess(android.os.Process.myPid());
    }
}