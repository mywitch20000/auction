package com.doollee.auction.utils;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.provider.Settings;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

/**
 *
 * Class: PermissionUtils
 * Created by wizard on 2020.11.04
 *
 * Description: 퍼미션 관련 함수
 */

public class PermissionUtils {
    public static boolean checkPermission(Activity activity, String[] pList,int requestCode){
        for (int index = 0; index < pList.length; index++) {
            int perStatus = ContextCompat.checkSelfPermission(activity, pList[index]);
            if (perStatus == PackageManager.PERMISSION_DENIED){
                requestPermission(activity, pList, requestCode);
                return false;
            }
        }

        return true;
    }

    public static void requestPermission(Activity activity, String[] pList, int requestCode){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            ActivityCompat.requestPermissions(activity, pList, requestCode);
        }
    }

    public static void goAppSettingsActivity(Context context) {
        Uri uri = Uri.parse("package:"+ context.getPackageName());
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS).setData(uri);
        context.startActivity(intent);
    }
}
