package com.doollee.auction.utils;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewParent;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ScrollView;
import android.widget.Spinner;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.doollee.auction.adapter.SpinnerAdapter;
import com.doollee.auction.common.Constants;
import com.doollee.auction.manager.DataManager;
import com.doollee.auction.manager.SettingsManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.net.NetworkInterface;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;

/**
 *
 * Class : ComUtils
 * Created by wizard on 2020.10.05
 *
 * Description : 공통 util
 */

public class ComUtils {
	/**
	 * 어플의 캐시메모리 삭제
	 *
	 * @return
	 */
	public static void clearApplicationCache(Activity activity, File dir) {
		if (dir == null)
			dir = activity.getCacheDir();

		if (dir == null)
			return;

		File[] children = dir.listFiles();
		try {
			for (int index = 0; index < children.length; index++)
				if (children[index].isDirectory())
					clearApplicationCache(activity, children[index]);
				else
					children[index].delete();
		} catch (Exception e) {
			Logs.d(e.getMessage());
		}
	}

	/*public static int getConnectivityStatus(Context context) {
		ConnectivityManager manager = (ConnectivityManager) context.getSystemService(context.CONNECTIVITY_SERVICE);

		NetworkInfo networkInfo = manager.getActiveNetworkInfo();
		if(networkInfo != null){
			int type = networkInfo.getType();
			if(type == ConnectivityManager.TYPE_MOBILE) {
				return Constants.NetworkStatus.TYPE_MOBILE;
			}else if(type == ConnectivityManager.TYPE_WIFI) {
				return Constants.NetworkStatus.TYPE_WIFI;
			}
		}
		return Constants.NetworkStatus.TYPE_NOT_CONNECTED;
	}*/

	/**
	 * 키보드 감추기
	 *
	 * @param activity
	 * @return
	 */
	public static void hideKeyboard(Activity activity) {
		InputMethodManager imm = (InputMethodManager) activity.getSystemService(AppCompatActivity.INPUT_METHOD_SERVICE);
		View view = activity.getCurrentFocus();
		if (view == null) {
			view = new View(activity);
		}
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	/**
	 * device id 가져오기
	 *
	 * @param context
	 * @return device id
	 */
	public static String getDeviceId(Context context) {
		TelephonyManager telephonyManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		String deviceId = "";
		if (telephonyManager != null && (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED)) {
			if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
				deviceId = Settings.Secure.getString(
						context.getContentResolver(),
						Settings.Secure.ANDROID_ID);
			} else if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
				if (telephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_CDMA) {
					deviceId = telephonyManager.getMeid();
				} else if (telephonyManager.getPhoneType() == TelephonyManager.PHONE_TYPE_GSM) {
					deviceId = telephonyManager.getImei();
				}
			} else {
				deviceId = telephonyManager.getDeviceId();
			}
		}

		if (TextUtils.isEmpty(deviceId)) {
			WifiManager wifi = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

			WifiInfo winfo = wifi.getConnectionInfo();
			deviceId = winfo.getMacAddress();
			if ("02:00:00:00:00:00".equals(deviceId)) {
				deviceId = ComUtils.getMACAddress("wlan0");
				deviceId = deviceId.replaceAll(":", "");
			}
		}
		return deviceId;
	}

	/**
	 * 현재 기기의 맥어드레스 가져오기
	 *
	 * @param interfaceName
	 * @return 현재 기기의 맥어드레스
	 */
	public static String getMACAddress(String interfaceName) {
		try {
			List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
			for (NetworkInterface intf : interfaces) {
				if (interfaceName != null) {
					if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
				}
				byte[] mac = intf.getHardwareAddress();
				if (mac == null) return "";
				StringBuilder buf = new StringBuilder();
				for (int idx = 0; idx < mac.length; idx++)
					buf.append(String.format("%02X:", mac[idx]));
				if (buf.length() > 0) buf.deleteCharAt(buf.length() - 1);
				return buf.toString();
			}
		} catch (Exception ex) {
			Logs.d(ex.getMessage());
		}

		return "";
	}

	/**
	 * 비행기모드 실행 여부 확인
	 * 실행확인 안될 시 false
	 * @retuen boolean
	 */
	public static boolean checkAirplane(Context context) {
		if ("1".equals(DataManager.getInstance().getData("TestAirModeCkPass")))
			return true; //테스트를 위한 무조건 통과 ( 나중에 지우기 )

		if (SettingsManager.getInstance(context).isAirplaneModeOn()) {
			return true;
		}
		return false;
	}

	/**
	 * 경매가능 wifi접속 여부 확인
	 * 접속확인 안될 시 wifiCheckYn 값 false
	 * 정상접속이거나 사용자 설정 후 확인 버튼 누른 경우만 wifiCheckYn 값 true로 변경
	 * @retuen boolean
	 */
	public static boolean checkWifi(Context context) {
		// 접속 Wifi 정보 저장
		WifiInfo wifiInfo = SettingsManager.getInstance(context).getWifiManager().getConnectionInfo();

		// SSID (접속 wifi 명) 저장
		String ssid = wifiInfo.getSSID().replaceAll("\"", "");

		if (Constants.WIFI_SSID.equalsIgnoreCase(ssid)) {
			return true;
		}

		return false;
	}

	/**
	 * 콤마세팅
	 * @param sInputNum
	 * @return
	 */
	public static String setComma(String sInputNum) {
		// 숫자에 콤보를 넣기 위해 3자리씩 저장하는 변수
		List inputNum    = new ArrayList<Object>();
		// 숫자 길이 저장 ( 3자리씩 쪼갰을때 개수, 전체 문자열(숫자) 중 각 위치의 시작값, 전체 문자열(숫자) 중 각 위치의 종료값 )
		int[] NumberLen  = {0, 0, 0};
		// 3자리씩 쪼개서 나온 갯수 저장
		NumberLen[0] = (int) Math.ceil((double) sInputNum.length() / 3);
		// List 배열에 앞부분부터 하나씩 저장
		for(int z = 0; z < NumberLen[0]; z++) {
			//시작위치 저장
			NumberLen[1] = sInputNum.length()-3*(NumberLen[0]-z);
			NumberLen[1] = NumberLen[1] < 0 ? 0 : NumberLen[1];
			//종료위치 저장
			NumberLen[2] = sInputNum.length()-3*(NumberLen[0]-z-1);
			//잘라낸 내용을 List에 저장
			inputNum.add(sInputNum.substring((int) NumberLen[1], (int) NumberLen[2]));
		}
		// List에 저장된 내용을 사이에 콤마를 넣으면서 합치기
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
			sInputNum = String.join(",", inputNum);
		}

		return sInputNum;
	}

	/**
	 * Spinner(combobox) 값 생성
	 * @param inJson //server에서 받은 공통콤보 데이터 JsonString
	 * @return //Spinner 생성 후 사용 Adapter를 return / 생성 후 값을 조회하는데 Adapter로 필요한 함수 사용
	 */
	public static SpinnerAdapter spinnerToJsonStr(Activity activity, String inJson) {
		SpinnerAdapter spinnerAdt = null;

		if (TextUtils.isEmpty(inJson)) {
			return spinnerAdt;
		}

		try {
			JSONObject jsonObject = new JSONObject(inJson);
			if (jsonObject == null) return null;
			Map map = JsonUtils.jsonToMap(jsonObject);
			if (map == null)
				return null;

			ArrayList arrayCode = (ArrayList) map.get("code");
			ArrayList arrayName = (ArrayList) map.get("name");

			if (arrayCode == null || arrayCode.size() <= 0 ||
			    arrayCode == null || arrayCode.size() <= 0 ||
				arrayCode.size() != arrayCode.size())
				return null;

			spinnerAdt = new SpinnerAdapter(activity, arrayCode, arrayName);

		} catch (JSONException e) {
			e.printStackTrace();
		}
		return spinnerAdt;
	}

	/**
	 * device id 가져오기
	 *
	 * @param context
	 * @return device id
	 */
	public static String getPhoneNumber(Context context) {
		if (ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_SMS) !=
				PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_NUMBERS) != PackageManager.PERMISSION_GRANTED &&
				ActivityCompat.checkSelfPermission(context, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
			return "";
		}

		TelephonyManager telManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
		@SuppressLint("HardwareIds")
		String PhoneNum = telManager.getLine1Number();
		if (PhoneNum.startsWith("+82")) {
			PhoneNum = PhoneNum.replace("+82", "0");
		}

		return PhoneNum;
	}
}