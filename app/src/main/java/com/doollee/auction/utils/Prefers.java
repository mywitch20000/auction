package com.doollee.auction.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 *
 * Class: Prefers
 * Created by wizard on 2020.10.12
 *
 * Description: Preference 저장 관련 함수
 */


public class Prefers {
    /**
     * 최초 실행 여부 반환
     *
     * @return boolean 최초 실행 여부
     */
    public static boolean getIsEntryFirst(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getBoolean("pref_is_entry_first",false);
    }

    /**
     * 최초 실행 여부 저장
     *
     * @param context
     * @param val 최초 실행 여부
     * @return
     */
    public static void setIsEntryFirst(Context context, boolean val) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putBoolean("pref_is_entry_first", val);
        editor.apply();
    }

    /**
     * 전화 수신 방법 반환
     *
     * @return int 전화 수신 방법
     */
    static public int getCallMethod(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getInt("pref_call_method",0);
    }

    /**
     * 전화 수신 방법 저장
     *
     * @param context
     * @param val 전화 수신 방법, 0 : 설정 안됨, 1 : 전화 수신, 2 : 비행기 모드
     * @return
     */
    public static void setCallMethod(Context context, int val) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putInt("pref_call_method", val);
        editor.apply();
    }


    /**
     * FCM ID 반환
     *
     * @return String FCM ID
     */
    public static String getFCMId(Context context) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        return prefs.getString("pref_fcm_id","");
    }

    /**
     * FCM ID 저장
     *
     * @param context
     * @param val FCM ID
     * @return
     */
    public static void setFCMId(Context context, String val) {
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString("pref_fcm_id", val);
        editor.apply();
    }


}
