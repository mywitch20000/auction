package com.doollee.auction.utils;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.doollee.auction.R;
import com.doollee.auction.common.AlertDialog;

/**
 *
 * Class : DialogUtils
 * Created by wizard on 2020.09.21
 *
 * Description : 다이얼로그 util
 */

public class DialogUtils {
	/**
	 * =====================================
	 * Alert, Confirm Dialog
	 * =====================================
	 */
	private static AlertDialog mAlertDialog;

	/**
	 * Alert Dialog
     *
	 * @param context
	 * @param msgId 메세지 문자열 ID
     * @return
	 */
	public static void alert(Context context, int msgId){
		alert(context, context.getString(msgId));
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param msg 메세지 문자열
     * @return
	 */
	public static void alert(Context context, String msg){
		alert(context, null, msg, null);
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param title 타이틀 문자열
	 * @param msg   메세지 문자열
     * @return
	 */
	public static void alert(Context context, String title, String msg){
		alert(context, title, msg, null);
	}

	/**
	 *
	 * @param context
	 * @param titleId 타이틀 문자열 ID
	 * @param msgId   메세지 문자열 ID
     * @return
	 */
	public static void alert(Context context, int titleId, int msgId){
		alert(context, context.getString(titleId), context.getString(msgId));
	}

	/**
	 * Alert Dialog
     *
	 * @param context
	 * @param msgId   메세지 문자열 ID
	 * @param pListener 버튼 콜백 리스너
	 */
	public static void alert(Context context, int msgId, View.OnClickListener pListener){
		alert(context, context.getString(R.string.confirm), "", context.getString(msgId), pListener, null);
	}

	/**
	 * Alert Dialog
     *
	 * @param context
	 * @param pId	버튼 문자열 ID
	 * @param msg	메세지 문자열
	 * @param pListener	버튼 콜백 리스너
     * @return
	 */
	public static void alert(Context context, int pId, String msg, View.OnClickListener pListener){
		alert(context, context.getString(pId), "", msg, pListener, null);
	}

	/**
	 *
	 * @param context
	 * @param msg	메세지 문자열
	 * @param pListener 버튼 콜백 리스너
     * @return
	 */
	public static void alert(Context context, String msg, View.OnClickListener pListener) {
		alert(context, context.getString(R.string.confirm), "", msg, pListener, null);
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param title	타이틀 문자열
	 * @param msg	메세지 문자열
	 * @param pListener	버튼 콜백 리스너
     * @return
	 */
	public static void alert(Context context, String title, String msg, View.OnClickListener pListener) {
		alert(context, title, context.getString(R.string.confirm), "", msg, pListener, null);
	}

	/**
	 * Alert Dialog
	 *
	 * @param context
	 * @param title	타이틀 문자열
	 * @param msg	메세지 문자열
	 * @param pListener	버튼 콜백 리스너
	 */
	public static void alert(Context context, String title, String msg, String pBtn ,View.OnClickListener pListener) {
		alert(context, title, pBtn, "", msg, pListener, null);
	}

	/**
	 * Confirm Dialog
	 *
	 * @param context
	 * @param msg	메세지 문자열 ID
	 * @param pListener	확인버튼 콜백 리스너
	 * @param nListener 취소버튼 콜백 리스너
     * @return
	 */
	public static void alert(Context context, int msg, View.OnClickListener pListener, View.OnClickListener nListener) {
		alert(context, context.getString(R.string.confirm), context.getString(R.string.cancel), context.getString(msg), pListener, nListener);
	}

	/**
	 *
	 * @param context
	 * @param msg
	 * @param pListener
	 * @param nListener
     * @return
	 */
	public static void alert(Context context, String msg, View.OnClickListener pListener, View.OnClickListener nListener) {
		alert(context, context.getString(R.string.confirm), context.getString(R.string.cancel), msg, pListener, nListener);
	}

	/**
	 * Confirm Dialog
	 *
	 * @param context
	 * @param title	타이틀 문자열
	 * @param msg	메세지 문자열
	 * @param pListener	확인버튼 콜백 리스너
	 * @param nListener 취소버튼 콜백 리스너
     * @return
	 */
	public static void alert(Context context, String title, String msg, View.OnClickListener pListener, View.OnClickListener nListener) {
		alert(context, title, context.getString(R.string.confirm), context.getString(R.string.cancel), msg, pListener, nListener);
	}

	/**
	 * Confirm Dialog
	 *
	 * @param context
	 * @param pBtn	positive 버튼 문자열
	 * @param nBtn  nagative 버튼 문자열
	 * @param msg	메세지 문자열
	 * @param pListener	positive 버튼 콜백 리스너
	 * @param nListener nagative 버튼 콜백 리스터
     * @return
     */
	public static void alert(Context context, String pBtn, String nBtn, String msg, View.OnClickListener pListener, View.OnClickListener nListener) {
		alert(context, null, pBtn, nBtn, msg, pListener, nListener);
	}

	/**
	 * Alert, Confirm Dialog 구성함수
	 *
	 * @param context
	 * @param title	타이틀 문자열
	 * @param pBtn	Positive 버튼 문자열
	 * @param nBtn	nagative 버튼 문자열
	 * @param msg	메세지 문자열
	 * @param pListener	positive 버튼 콜백 리스너
	 * @param nListener	nagative 버튼 콜백 리스너
     * @return
	 */
	public static void alert(Context context, String title, String pBtn, String nBtn, String msg, View.OnClickListener pListener, View.OnClickListener nListener) {
		mAlertDialog = new AlertDialog(context);
/*
		if(title != null && !"".equals(title)){
			mAlertDialog.title = title;
		}
*/
		if(msg != null && !"".equals(msg)) {
			mAlertDialog.msg = msg;
		}

		if(nBtn != null && !"".equals(nBtn)) {
			mAlertDialog.mNBtText = nBtn;
		}

		if(pBtn != null && !"".equals(pBtn)) {
			mAlertDialog.mPBtText = pBtn;
		}

		if(nListener != null){
			mAlertDialog.mNListener = nListener;
		}

		if(pListener != null){
			mAlertDialog.mPListener = pListener;
		}

        if(context instanceof Activity && !((Activity)context).isFinishing()) {
            mAlertDialog.show();
        }
	}
}
