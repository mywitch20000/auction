package com.doollee.auction.common;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;

import com.doollee.auction.R;
import com.doollee.auction.activity.MainActivity;
import com.doollee.auction.common.net.HttpSenderTask;
import com.doollee.auction.common.net.HttpUtils;
import com.doollee.auction.utils.ComUtils;
import com.doollee.auction.utils.Logs;
import com.doollee.auction.utils.Prefers;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class FCMMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        Logs.d("From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Logs.d("Message data payload: " + remoteMessage.getData());

            String title = remoteMessage.getData().get("title");
            String body = remoteMessage.getData().get("body");
            String color = remoteMessage.getData().get("color");

            /* Check if data needs to be processed by long running job */
            /*if (true) {
                // For long-running tasks (10 seconds or more) use WorkManager.
                scheduleJob();
            } else {
                // Handle message within 10 seconds
                handleNow();
            }*/
        }

        if (remoteMessage.getNotification() != null) {
            sendNotification(remoteMessage);
        }
    }

    @Override
    public void onNewToken(String token) {
        Logs.d("Refreshed token: " + token);
        String FCMId = Prefers.getFCMId(this);
        if (TextUtils.isEmpty(FCMId) || !FCMId.equals(FCMId))
            sendFCMId();
    }

    /**
     * Persist token to third-party servers.
     *
     * Modify this method to associate the user's FCM InstanceID token with any server-side account
     * maintained by your application.
     */
    private void sendFCMId() {
        final String deviceId = ComUtils.getDeviceId(FCMMessagingService.this);
        FirebaseInstanceId.getInstance().getInstanceId().addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
            @Override
            public void onComplete(@NonNull Task<InstanceIdResult> task) {
                if (!task.isSuccessful()) {
                    return;
                }

                // Get new Instance ID token
                final String token = task.getResult().getToken();
                String FCMId = Prefers.getFCMId(FCMMessagingService.this);
                Logs.d("FCM", "+++++++ token : " + token);

                if (TextUtils.isEmpty(FCMId) || !FCMId.equals(token)) {
                    Map param = new HashMap();
                    param.put("in_id", deviceId);
                    param.put("fcm_id", token);
                    // TODO 전문 주소 변경 필요
                    HttpUtils.sendHttpTask(Constants.URL_FCM_ID, param, new HttpSenderTask.HttpRequestListener() {
                        @Override
                        public void endHttpRequest(String ret) {
                            try {
                                JSONObject jsonObject = new JSONObject(ret);
                                if (jsonObject == null) {
                                    return;
                                }

                                String successYn = jsonObject.getString("successYn");
                                if (Constants.DefineValue.SUCCESS.equalsIgnoreCase(successYn)) {
                                    Prefers.setFCMId(FCMMessagingService.this, token);
                                }
                            } catch (JSONException e) {
                                Logs.d(e.getMessage());
                            }
                        }
                    });
                }
            }
        });
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param remoteMessage FCM message RemoteMessage
     */
    private void sendNotification(RemoteMessage remoteMessage) {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent,
                PendingIntent.FLAG_ONE_SHOT);

        String title = remoteMessage.getNotification().getTitle();
        String body = remoteMessage.getNotification().getBody();

        if (TextUtils.isEmpty(title))
            title = getString(R.string.fcm_message);

        if (TextUtils.isEmpty(body))
            body = getString(R.string.fcm_message);

        String channelId = getString(R.string.default_notification_channel_id);
        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder =
                new NotificationCompat.Builder(this, channelId)
                        .setSmallIcon(R.drawable.ic_launcher_background)
                        .setContentTitle(title)
                        .setContentText(body)
                        .setAutoCancel(true)
                        .setSound(defaultSoundUri)
                        .setContentIntent(pendingIntent);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(channelId,
                    "Channel human readable title",
                    NotificationManager.IMPORTANCE_DEFAULT);
            notificationManager.createNotificationChannel(channel);
        }

        notificationManager.notify(0 , notificationBuilder.build());
    }
}
