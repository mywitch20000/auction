package com.doollee.auction.common.net;

import android.annotation.SuppressLint;
import android.os.Build;
import android.text.TextUtils;

import com.doollee.auction.utils.Logs;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import static org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER;

/**
 *
 * Class: HttpSender
 * Created by wizard on 2020.10.05
 *
 * Description: Http 요청
 */

public class HttpSender {
    private static final int DEFAULT_CONNECT_TIMEOUT = 60 * 1000;    // HTTP 연결에 대한 기본 Timeout
	private static final int DEFAULT_READ_TIMEOUT	 = 60 * 1000;    // HTTP 연결 이후 Input Stream 읽기에 대한 기본 Timeout

	/**
	 * HTTP GET 요청을 수행한다.
	 *
	 * @param urlStr 요청 URL
	 * @return 응답 문자열
	 */
	@SuppressLint("AllowAllHostnameVerifier")
	public static String requestGet(String urlStr) {
		String body = "";
		URL url;
		HttpURLConnection mConn = null;

		try {
			url = new URL(urlStr);

			if (url.getProtocol().toLowerCase().equals("https")) {
				HttpsURLConnection https = (HttpsURLConnection) url.openConnection();

				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
					https.setHostnameVerifier(ALLOW_ALL_HOSTNAME_VERIFIER);

				mConn = https;

			} else {
				mConn = (HttpURLConnection) url.openConnection();
			}
			
			if (mConn != null) {
				mConn.setConnectTimeout(DEFAULT_CONNECT_TIMEOUT);
				mConn.setReadTimeout(DEFAULT_READ_TIMEOUT);
				mConn.setRequestMethod("GET");
				mConn.setUseCaches(false);

				HttpUtils.setCooKie(urlStr, mConn);

				mConn.setRequestProperty("Accept", "application/json");
				mConn.setRequestProperty("Content-Type", "application/json");

				if (!TextUtils.isEmpty(HttpUtils.getUserAgent()))
				    mConn.setRequestProperty("User-Agent", HttpUtils.getUserAgent());

				int responseCode = mConn.getResponseCode();
				if (responseCode != HttpURLConnection.HTTP_OK) {
					mConn.disconnect();
					return body;
				}

				HttpUtils.saveCookie(mConn);

				BufferedReader br = new BufferedReader(new InputStreamReader(mConn.getInputStream(), HttpUtils.CHARSET_TYPE));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = br.readLine()) != null) {
					response.append(inputLine);
				}

				br.close();
				body = response.toString();
			}
		} catch (Exception e) {
			Logs.d(e.getMessage());
		}

		if (mConn != null)
			mConn.disconnect();

		return body.trim();
	}

	/**
	 * HTTP POST 요청을 수행한다.
	 *
	 * @param urlStr 요청 URL
	 * @param param 요청 파라미터 문자열
	 * @return 응답 문자열
	 */
	@SuppressLint("AllowAllHostnameVerifier")
	public static String requestPost(String urlStr, String param) {
		String body = "";
		URL url;
		HttpURLConnection mConn = null;

		try {
			url = new URL(urlStr);

			if (url.getProtocol().toLowerCase().equals("https")) {
				HttpsURLConnection https = (HttpsURLConnection) url.openConnection();

				if (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP)
					https.setHostnameVerifier(ALLOW_ALL_HOSTNAME_VERIFIER);

				mConn = https;
			} else {
				mConn = (HttpURLConnection) url.openConnection();
			}

			if (mConn != null) {
				mConn.setConnectTimeout(DEFAULT_CONNECT_TIMEOUT);
				mConn.setReadTimeout(DEFAULT_READ_TIMEOUT);
				mConn.setRequestMethod("POST");
				mConn.setUseCaches(false);
				mConn.setDoInput(true);
				mConn.setDoOutput(true);

				HttpUtils.setCooKie(urlStr, mConn);

				mConn.setRequestProperty("Content-Type", "application/json");

				if (!TextUtils.isEmpty(HttpUtils.getUserAgent()))
					mConn.setRequestProperty("User-Agent", HttpUtils.getUserAgent());

				if (!TextUtils.isEmpty(param)) {
					OutputStream os = mConn.getOutputStream();
					os.write(param.getBytes(HttpUtils.CHARSET_TYPE));
					os.flush();
				}

				int responseCode = mConn.getResponseCode();
				if (responseCode != HttpURLConnection.HTTP_OK) {
					mConn.disconnect();
					return body;
				}

				HttpUtils.saveCookie(mConn);

				BufferedReader br = new BufferedReader(new InputStreamReader(mConn.getInputStream(), HttpUtils.CHARSET_TYPE));
				String inputLine;
				StringBuffer response = new StringBuffer();

				while ((inputLine = br.readLine()) != null) {
					response.append(inputLine);
				}

				br.close();
				body = response.toString();
			}
		} catch(Exception e) {
			Logs.d(e.getMessage());
		} finally {
			if(mConn != null) {
				mConn.disconnect();
			}
		}
		return body.trim();
	}
}
