package com.doollee.auction.common.net;

import android.os.AsyncTask;

import com.doollee.auction.manager.DataManager;
import com.doollee.auction.ui.BaseActivity;

import java.net.Socket;

/**
 *
 * Class: SvrSocket
 * Created by lim on 2020.12.28
 *
 * Description: server와 socket 통신 설정
 */

public class SocketRecver {
	private static SocketRecver instance = null;

	SocketRecvTask asyncTask;

	/**
	 * Singleton instance 생성
	 *
	 * @return instance
	 */
	public static SocketRecver getInstance() {
		if (instance == null) {
			synchronized (DataManager.class) {
				if (instance == null) {
					instance = new SocketRecver();
				}
			}
		}
		return instance;
	}

	/**
	 * Send an instruction to the specified port
	 *
	 * @param port
	 * @param socketCallback
	 */
	public void recvMsg(Socket socket, String port, String msg, SocketRecvTask.OnSocketCallback socketCallback) {
		if (asyncTask == null || socket == null) {
			asyncTask = new SocketRecvTask(socket, socketCallback);
			asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, String.valueOf(port), msg);
		} else {
			asyncTask.setSocketCallback(socketCallback);
		}
	}

	public boolean getIsRunning() {
		if (asyncTask == null)
			return false;

		return asyncTask.getIsRunning();
	}

	public void stopTask() {
		if (asyncTask == null)
			return;

		asyncTask.stopSocket();
	}

	public void setSocketCallback(SocketRecvTask.OnSocketCallback socketCallback) {
		if (asyncTask == null)
			return;

		asyncTask.setSocketCallback(socketCallback);
	}
}
