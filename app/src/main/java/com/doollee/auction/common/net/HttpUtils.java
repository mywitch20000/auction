package com.doollee.auction.common.net;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkCapabilities;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.text.TextUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.ValueCallback;

import com.doollee.auction.utils.Logs;

import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * Class: HttpUtils
 * Created by wizard on 2020.10.05
 *
 * Description: Http 관련 util
 */

public class HttpUtils {
    public static final String CHARSET_TYPE = "UTF-8";

    /*
     * UserAgent값을 생성해 두고 가지고 있는다.
     * */
    private static String mUserAgent;

    // 현재 연결된 네트웍의 상태값.
    public enum NetState {
        NET_STATE_MOBILE,
        NET_STATE_WIFI,
        NET_STATE_ETHERNET,
        NET_STATE_OFFLINE,
        NET_STATE_NOT_SUPPORT,
    }

    /**
     * POST 방식으로 웹에 접속하여 데이타 조회시 사용
     *
     * @param url      접속주소
     * @param param    서버에 보낼 데이타 String 타입
     * @param listener 리턴받을 리스너
     */
    public static void sendHttpTask(String url, String param, HttpSenderTask.HttpRequestListener listener) {
        new HttpSenderTask(url, listener).execute(param);
    }

    /**
     * POST 방식으로 웹에 접속하여 데이타 조회시 사용
     *
     * @param url      접속주소
     * @param param    서버에 보낼 데이타 Map 타입
     * @param listener 리턴받을 리스너
     */
    public static void sendHttpTask(String url, Map<String, Object> param, HttpSenderTask.HttpRequestListener listener) {
        StringBuffer sbParams = new StringBuffer();

        try {
            String key;
            String value;

            JSONObject object = new JSONObject();
            Iterator<String> iterator = param.keySet().iterator();
            while (iterator.hasNext()) {
                key = (String) iterator.next();
                value = (String) param.get(key);

                object.put(key, value);
            }

            sbParams.append(object.toString());

        } catch (Exception e) {
            Logs.printException(e);
        }

        if (param.size() <= 0)
            new HttpSenderTask(url, listener).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, "");
        else
            new HttpSenderTask(url, listener).executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, sbParams.toString());
    }

    /**
     * 앱에서 사용되는 User-Agent 값을 생성한다.
     * baseUserAgent 값이 있는 경우 baseUserAgent에 앱전용 User-Agent값이 append된다.
     *
     * @param context
     * @return User-Agent 문자열
     */
    public static String makeUserAgent(Context context) {
        StringBuilder sb = new StringBuilder();

        String userAgect = getUserAgent();
        if (!TextUtils.isEmpty(userAgect)) {
            sb.append(userAgect).append("&");
        }

        sb.append(userAgect).append("entryPoint=app");

        mUserAgent = sb.toString();

        return mUserAgent;
    }

    public static String getUserAgent() {
        return mUserAgent;
    }

    /**
     * Http연결시 Header에 쿠키정보를 넣어준다.
     *
     * @param url  접속주소
     * @param conn Connection정보
     */
    public static void setCooKie(String url, HttpURLConnection conn) {
        String cookieString = CookieManager.getInstance().getCookie(url);
        if (!TextUtils.isEmpty(cookieString)) {
            conn.setRequestProperty("Cookie", cookieString);
        }
    }

    /**
     * Http접속후 넘어오는 Cookie정보를 저장해 둔다.
     *
     * @param conn
     */
    public static void saveCookie(URLConnection conn) {
        String cookie = conn.getHeaderField("Set-Cookie");
        String userAgent = conn.getHeaderField("User-Agent");

        if (cookie != null) {
            String url = conn.getURL().toString();
            CookieManager cookieManger = CookieManager.getInstance();
            cookieManger.setCookie(url, cookie);

            // permanent 영역에 쿠기를 즉시 동기화 한다.
            if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
                CookieSyncManager.getInstance().sync();
            } else {
                cookieManger.flush();
            }
        }
    }

    /**
     * 쿠키정보를 Clear한다.
     */
    public static void removeCookie() {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            CookieManager.getInstance().removeSessionCookie();
        } else {
            CookieManager.getInstance().removeSessionCookies(new ValueCallback<Boolean>() {
                @Override
                public void onReceiveValue(Boolean aBoolean) {

                }
            });
        }
    }

    /**
     * Web Browser에서 앱 호출시 파라미터 자름
     * nonghyupsb://smartnais?json={"MENU_ID":"MPDT0110101000M","KEY1":"한글 한글 asdfasf","KEY2":"abcd 한글 가나다"}
     *
     * @param url
     * @return String 자른 파라미터 부분 문자열
     */
    public static String getMobileWebParam(String url) {
        final String DIV_STR = "json=";

        if (TextUtils.isEmpty(url)) {
            return "";
        }

        int pos = url.indexOf(DIV_STR) + DIV_STR.length();
        if (pos < 0) return "";
        if (pos + 1 == url.length()) return "";

        return url.substring(pos, url.length());
    }

    /**
     * 모바일 단말기의 인터넷 접속 상태를 체크한다.
     *
     * @param context
     * @return 상태값
     */
    public static NetState checkNetworkState(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (cm == null)
            return NetState.NET_STATE_NOT_SUPPORT;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            final NetworkInfo ni = cm.getActiveNetworkInfo();
            if (ni != null) {
                if (ni.isConnectedOrConnecting()) {
                    if (ni.getType() == ConnectivityManager.TYPE_WIFI) {
                        // WIFI 네트워크 연결중
                        return NetState.NET_STATE_WIFI;
                    } else if (ni.getType() == ConnectivityManager.TYPE_MOBILE) {
                        // 모바일 네트워크 연결중
                        return NetState.NET_STATE_MOBILE;
                    } else if (ni.getType() == ConnectivityManager.TYPE_ETHERNET) {
                        // 모바일 네트워크 연결중
                        return NetState.NET_STATE_ETHERNET;
                    }
                }
            } else {
                // 네트워크 오프라인 상태.
                return NetState.NET_STATE_OFFLINE;
            }
        } else {
            final NetworkInfo ni = cm.getActiveNetworkInfo();
            if (ni != null) {
                if (ni.isConnected()) {
                    Network[] ns = cm.getAllNetworks();
                    if (ns != null) {
                        for (int index = 0; index < ns.length; index++) {
                            NetworkCapabilities nc = cm.getNetworkCapabilities(ns[index]);
                            if (nc.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                                // WIFI 네트워크 연결중
                                return NetState.NET_STATE_WIFI;
                            } else if (nc.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                                // 모바일 네트워크 연결중
                                return NetState.NET_STATE_MOBILE;
                            } else if (nc.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                                // 모바일 네트워크 연결중
                                return NetState.NET_STATE_MOBILE;
                            } else {
                                return NetState.NET_STATE_OFFLINE;
                            }
                        }
                    } else {
                        return NetState.NET_STATE_OFFLINE;
                    }
                }
            } else {
                return NetState.NET_STATE_OFFLINE;
            }
        }

        // 네트워크 null.. 모뎀이 없는 경우??
        return NetState.NET_STATE_NOT_SUPPORT;
    }

    /**
     * Map을 파라미터 문자열 형태로 변환한다.
     *
     * @param paramMap 파라미터 Map
     * @return 파라미터 문자열 (key1=value1&key2=value2&...)
     * @throws UnsupportedEncodingException
     */
    public static String mapToParamString(Map<String, Object> paramMap) throws UnsupportedEncodingException {
        if (paramMap == null) {
            return null;
        }

        StringBuilder sb = new StringBuilder();
        int paramCnt = paramMap.size();
        int index = 0;
        for (Map.Entry<String, Object> entry : paramMap.entrySet()) {
            sb.append(entry.getKey()).append("=").append(URLEncoder.encode((String) entry.getValue(), CHARSET_TYPE));
            if (index < paramCnt - 1) {
                sb.append("&");
            }
            index++;
        }

        return sb.toString();
    }

    /**
     * JavaScript의 encodeURIComponent()과 동일 기능
     *
     * @param uri 엔코딩할 uri
     * @return encode
     */
    public static String encodeURIComponent(String uri) {
        String result = null;

        try {
            result = URLEncoder.encode(uri, "UTF-8")
                    .replaceAll("\\+", "%20")
                    .replaceAll("\\%21", "!")
                    .replaceAll("\\%27", "'")
                    .replaceAll("\\%28", "(")
                    .replaceAll("\\%29", ")")
                    .replaceAll("\\%7E", "~");
        } catch (UnsupportedEncodingException e) {
            result = uri;
        }

        return result;
    }
}
