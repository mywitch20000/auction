package com.doollee.auction.common.net;

import android.os.AsyncTask;

import com.doollee.auction.manager.DataManager;
import com.doollee.auction.ui.BaseActivity;

import java.net.Socket;

/**
 *
 * Class: SvrSocket
 * Created by lim on 2020.12.28
 *
 * Description: server와 socket 통신 설정
 */

public class SocketSender {
	private static SocketSender instance = null;

	/**
	 * Singleton instance 생성
	 *
	 * @return instance
	 */
	public static SocketSender getInstance() {
		if (instance == null) {
			synchronized (DataManager.class) {
				if (instance == null) {
					instance = new SocketSender();
				}
			}
		}
		return instance;
	}

	/**
	 * Send an instruction to the specified port
	 *
	 * @param port
	 * @param msg
	 * @param socketCallback
	 */
	public void sendMsg(Socket socket, String port, String msg, SocketSendTask.OnSocketCallback socketCallback) {
		SocketSendTask asyncTask = new SocketSendTask(socket, socketCallback);
		asyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR, String.valueOf(port), msg);
	}
}
