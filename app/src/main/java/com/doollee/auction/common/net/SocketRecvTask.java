package com.doollee.auction.common.net;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.doollee.auction.common.Constants;
import com.doollee.auction.manager.DataManager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

/**
 *
 * Class: SocketSendTask
 * Created by wizard on 2021.01.18
 *
 * Description: Socket Connect를 할경우 AsyncTask로 접속해야 해서 추가함.
 * Post방식에서 자료형은 String, Map만 지원함.
 */

public class SocketRecvTask extends AsyncTask<String, String, String> {
    private static final int DEFAULT_CONNECT_TIMEOUT = 10 * 1000; // 연결에 대한 기본 Timeout

    private String sSocketError = "";
    private boolean isRunning = false;

    private Socket socket;

    private OnSocketCallback socketCallback;

    public SocketRecvTask(Socket socket, OnSocketCallback socketCallback) {
        this.socket = socket;
        this.socketCallback = socketCallback;
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected String doInBackground(String... params) {
        int port = Integer.parseInt(params[0]);
        String msg = params[1];
        String data = "";

        isRunning = true;

        try {
            if (socket == null ||
                !socket.isConnected() || socket.isClosed()) {
                socket = new Socket();
                //socket.setSoTimeout(DEFAULT_CONNECT_TIMEOUT);
                socket.setTcpNoDelay(true);
                socket.setKeepAlive(true);
                socket.setReuseAddress(true);

                SocketAddress socketAddress = new InetSocketAddress(Constants.URL_DOMAIN, port);
                socket.connect(socketAddress, DEFAULT_CONNECT_TIMEOUT);

                DataManager.getInstance().setSocketMapData(port, socket);

                if (socketCallback != null) {
                    socketCallback.onConnect();
                }
            }

            if (!TextUtils.isEmpty(msg)) {
                PrintWriter pw = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), HttpUtils.CHARSET_TYPE)), true);
                pw.println(msg);
            }

            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), HttpUtils.CHARSET_TYPE));
            while (isRunning) {
                data = in.readLine();
                if (!TextUtils.isEmpty(data))
                    socketCallback.onReceive(data);
            }
        } catch (IOException e) {
            e.printStackTrace();
            sSocketError = e.getMessage();
        } finally {
            try {
                if (socket != null) {
                    socket.close();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

            socket = null;
            DataManager.getInstance().setSocketMapData(port, null);
        }

        return data;
    }

    @Override
    protected void onPostExecute(String result) {
        if (!TextUtils.isEmpty(sSocketError) && socketCallback != null) {
            socketCallback.onFail(sSocketError);
        } else {
            socketCallback.onDisconnect();
        }
    }

    public void setSocketCallback(OnSocketCallback socketCallback) {
        this.socketCallback = socketCallback;
    }

    public void stopSocket() {
        isRunning = false;
    }

    public boolean getIsRunning() {
        return isRunning;
    }

    public interface OnSocketCallback {
        void onConnect();
        void onReceive(String msg);
        void onFail(String msg);
        void onDisconnect();
    }
}

