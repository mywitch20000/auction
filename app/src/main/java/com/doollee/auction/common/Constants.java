package com.doollee.auction.common;

/**
 *
 * Class : Constants
 * Created by wizard on 2020.09.22
 *
 * Description : 앱 내에서 사용되는 상수 정의
 */

public class Constants {
    public static String URL_DOMAIN                        = "192.168.0.96";//"http://www.doollee.co.kr";

    public final static String URL_FCM_ID                  = URL_DOMAIN + "/fcm_update.do";

    public final static String URL_PRICE_OPEN_API          = "http://apis.data.go.kr/B552895/openapi/service/OrgPriceAuctionService/";
    public final static String URL_PRICE_REAL_OPEN_API     = "http://openapi.epis.or.kr/openapi/service/RltmAucBrknewsService/";
    public final static String PATH_PRICE_CATEGORY         = "getRealProdPriceList";
    public final static String PATH_PRICE_MARKET           = "getRealMarketPriceList";
    public final static String PATH_PRICE_REAL_CATEGORY    = "getPrdlstRltmAucBrknewsList";
    public final static String PATH_PRICE_REAL_MARKET      = "getWltRltmAucBrknewsList";

    public final static String KEY_OPEN_API                = "8gxbnB5Uiy5OsAHmHaq38FlmyUTOY%2B9F2NwQTFaOIyl2ck055xHykHslAEsjPXdT5nE79VjZWnCYduOpijhnZQ%3D%3D";

    public final static int NUM_OF_ROWS                    = 20;

    public static final String WIFI_SSID                   = "doollee5G";
    public static final String PRESHAREDKEY                = "doollee1!";

    public static class DefineValue {                                                // 공통 사용 정의값
        public static final String SUCCESS                 = "Y";
        public static final String FAIL                    = "N";
        public static final String YES                     = "1";
        public static final String NO                      = "0";
        public static final String SUCCESS_CODE            = "00";
    }

    public static class Schedule {
        public static final String SCHEDULE_LIST           = "schedule_list";
    }

    public static class ScheduleInfo {
        public static final String SCHEDULE_LIST           = "schedule_list";
        public static final String SCHEDULE_POS            = "schedule_pos";
    }

    public static class PriceBid {
        public static final String INQUIRY_TYPE            = "inquiry_type";
        public static final String DATE                    = "date";
        public static final String SUB_CATEGORY_CODE       = "sub_class_code";
        public static final String MAIN_MARKET_CODE        = "main_market_code";
        public static final String SUB_MARKET_CODE         = "sub_market_code";
        public static final String TOTAL_PRICE_BID         = "total_price_bid";
        public static final String LIST_PRICE_BID          = "list_price_bid";

        public static final String INQUIRY_TYPE_CATEGORY   = "category";
        public static final String INQUIRY_TYPE_MARKET     = "market";
    }

    public static class Settings {
        public static final int REQUEST_NETWOTK_SETTINGS   = 10000;
        public static final int REQUEST_WIFI_SETTINGS      = 10001;
    }
}
