package com.doollee.auction.common.net;

import android.os.AsyncTask;
import android.text.TextUtils;

/**
 *
 * Class: HttpSenderTask
 * Created by wizard on 2020.09.21
 *
 * Description: Http Connect를 할경우 AsyncTask로 접속해야 해서 추가함.
 * Post방식에서 자료형은 String, Map만 지원함.
 */

public class HttpSenderTask extends AsyncTask<String, Void, String> {
    private String url;
    private HttpRequestListener listener;

    public HttpSenderTask(String url, HttpRequestListener listener) {
        this.url = url;
        this.listener = listener;
    }

    @Override
    protected void onPreExecute() {

    }

    @Override
    protected String doInBackground(String... params) {
        String retString;

        if (TextUtils.isEmpty(params[0])) {
            retString = HttpSender.requestGet(url);
        } else{
            retString = HttpSender.requestPost(url, (String) params[0]);
        }

        return retString;
    }

    @Override
    protected void onPostExecute(String s) {
        listener.endHttpRequest(s);
    }

    public interface HttpRequestListener {
        void endHttpRequest(String ret);
    }
}

