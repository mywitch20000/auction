package com.doollee.auction.common.net;

import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.ArrayMap;
import android.util.Log;

import com.doollee.auction.common.Constants;
import com.doollee.auction.manager.DataManager;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 *
 * Class: SvrSocket
 * Created by lim on 2020.12.28
 *
 * Description: server와 socket 통신 설정
 */

public class SvrSocket {
    private static final int DEFAULT_CONNECT_TIMEOUT = 10 * 1000; // 연결에 대한 기본 Timeout
	private static final int DEFAULT_READ_TIMEOUT	 = 10 * 1000; // 연결 이후 Input Stream 읽기에 대한 기본 Timeout

	/**
	 * 소켓통신 - port접속
	 */
	public static boolean conn(final int port) {
	    final String[]  errStr     = {""};
		final boolean[] rtnBoolean = {false};

	    Thread thread = new Thread() {
			Socket preConnect = (Socket) DataManager.getInstance().getSocketMapData(port);
			public void run() {
				try {
					// 기존접속되어 있었다면 닫기
					if (preConnect != null) {
						if (preConnect.isConnected() && !preConnect.isClosed()) {
							preConnect.close();
						}
					}

					// 소켓접속주소
					// SocketAddress socketAddress = new InetSocketAddress(Constants.URL_DOMAIN, port);
					// 소켓생성
					Socket socket = new Socket(Constants.URL_DOMAIN, port);
					socket.setTcpNoDelay(true);
					socket.setKeepAlive(true);
					socket.setReuseAddress(true);

					//****** DatagramSocket //UTP 통신 소켓
					//수신시 timeout 설정 (실시간인 경우 예외)
					//if(port != 10201 && port != 10202) socket.setSoTimeout(DEFAULT_READ_TIMEOUT);
					//소켓연결 timeout 설정
					//socket.connect(socketAddress, DEFAULT_CONNECT_TIMEOUT);
					DataManager.getInstance().setSocketMapData(port, socket);

				} catch (SocketTimeoutException e) {
					//e.printStackTrace();
					errStr[0] = "SocatConnErr\n" + e.getMessage();
				} catch (Exception e) {
					//e.printStackTrace();
					errStr[0] = "SocatConnErr\n" + e.getMessage();
				}
			}
		};

		try {
			thread.start();
			thread.join();
			if (errStr[0]!="") {
				Handler mHandler = new Handler(Looper.getMainLooper());
				mHandler.postDelayed(new Runnable() {
					@Override
					public void run() {
						rtnBoolean[0] = false;
					}
				}, 0);
			} else {
				thread = new Thread() {
					public void run() {
						//정상접속시 true;
						Socket sc = (Socket) DataManager.getInstance().getSocketMapData(port);
						if (sc.isConnected() && !sc.isClosed()) {
							rtnBoolean[0] = true;
						}
					}
				};
				thread.start();
				thread.join();
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return rtnBoolean[0];
	}

	/**
	 * 소켓통신 - 서버 전송
	 */
	public static boolean send(final int port, final String inStr) {
		final String[] errStr = {""};
		if (!DataManager.getInstance().getSocketMap().containsKey(port)) {
			return false;
		}

		final Socket socket = (Socket) DataManager.getInstance().getSocketMapData(port);
		if (!socket.isConnected() || socket.isClosed())
			return false;

		Thread thread = new Thread() {
			public void run() {
				try {
					PrintWriter out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), HttpUtils.CHARSET_TYPE)), true);
					out.println(inStr);
				} catch (Exception e) {
					Log.d("Socat Send Error", e.getMessage());
					errStr[0] = "SocatSendErr\n" + e.getMessage();
				}
			}
		};

		try {
			thread.start();
			thread.join();
			if (!TextUtils.isEmpty(errStr[0])) {
				return false;
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
			return false;
		}

		return true;
	}

	/**
	 * 소켓통신 - 서버 수신
	 */
	public static String recv(final int port) {
		final String[] rtnStr = {""};
		if (!DataManager.getInstance().getSocketMap().containsKey(port)) {
			return "";
		}

		final Socket socket = (Socket) DataManager.getInstance().getSocketMapData(port);
		if (!socket.isConnected() || socket.isClosed())
			return "";

		Thread thread = new Thread() {
			public void run() {
				try {
					BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream(), "UTF-8"));
					rtnStr[0] = in.readLine();
				} catch (Exception e) {
					Log.d("Socat Recv Error", e.getMessage());
					rtnStr[0] = "SocatRecvErr\n"+e.getMessage();
				}
			}
		};

		try {
			thread.start();
			thread.join();
			if(TextUtils.isEmpty(rtnStr[0])) {
				//BaseActivityCustomToast(rtnStr[0]);
			}
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		return rtnStr[0];
	}

	/**
	 * 소켓통신 - 서버 수신 (실시간)
	 */
	public static void recvRT(final int port) {
		//해당 포트에 대한 소켓존재여부, 접속여부 체크
		if( !DataManager.getInstance().getSocketMap().containsKey(port) ) {
			return;
		}

		final Socket socket = (Socket) DataManager.getInstance().getSocketMapData(port);
		if(!socket.isConnected() || socket.isClosed())
			return;

		// 사용변수 셋
		final Handler mHandler = new Handler(Looper.getMainLooper());
		final byte[]  byteD    = new byte[10000];
		//응답용 송신 set
		PrintWriter out = null;
		try {
			out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream(), "UTF-8")), true);
		} catch (Exception e) {
			e.printStackTrace();
			return;
		}

		final PrintWriter fout = out;
		// 반복스레드 생성
		final ScheduledThreadPoolExecutor exec = new ScheduledThreadPoolExecutor(1);
		exec.scheduleAtFixedRate(new Runnable() {
			List    list                      = new ArrayList();
			String  rt_rvData, sdfNow, ErrMsg = ""             ;
			int     size = 0 , successYn = -1                  ; //successYn 수신정상여부 정상1 실패0 미동작-1
			public void run() {
				try {
					successYn = -1; //정상수신여부 초기화
					ErrMsg    = ""; //에러메세지 초기화
					size      = socket.getInputStream().read(byteD);
					if (size == -1) {
						Socket preConnect = (Socket) DataManager.getInstance().getSocketMapData(port);
						if (preConnect.isConnected() && !preConnect.isClosed()) {
							preConnect.close();
						}

						//서버가 끊어지면 tread 닫기
						exec.shutdown();
					}

					if (size > 0) {
						rt_rvData = new String(byteD, 0, size, HttpUtils.CHARSET_TYPE);
						//////////////////////////////////////////////////////////////////////
						// 실시간 수신알림 표시
						mHandler.postDelayed(new Runnable() {
							@Override
							public void run() {
								if( rt_rvData.length() > 0 ) {
									//BaseActivityCustomToast(rt_rvData);
									//rt_rvData = "";
								}
							}
						}, 100);

						Map map = new ArrayMap();
						// 현재시간가져오기
						sdfNow = new SimpleDateFormat("HH:mm:ss", Locale.getDefault()).format(new Date(System.currentTimeMillis()));
						// 기존에 전역데이터가 있었던 경우 가져오기
						if (!"".equals(DataManager.getInstance().getData("NoticeList")) ) {
							list = (ArrayList) DataManager.getInstance().getData("NoticeList");
						}

						// 전역변수에 값 추가
						map.put("time" ,sdfNow   );
						map.put("cont" ,rt_rvData);
						map.put("newYn","Y"      );
						list.add(map);
						DataManager.getInstance().setData("NoticeList", list);

						///////////////////////////////////////////////////////////////
						//정상동작하면 1
						successYn = 1;
					}
				} catch (Exception e) {
					successYn = 0; //실패 0
					ErrMsg    = e.getMessage();
					//Log.d("Socat Recv Error", e.getMessage());
				}
				//수신정상여부 전달
				/*if (successYn != -1) {
					Map map = BaseActivitySvrSendMap("302");
					map.put("SUCCESS_YN", successYn);
					map.put("FAIL_MSG"  , ErrMsg   );
					fout.println(JsonUtils.mapToJson(map));
				}*/
			}
		}, 0, 1, TimeUnit.SECONDS);
	}

	public static void closeAll() {
		//종료전 메세지 전달
		/*Map svrSendMsg = BaseActivitySvrSendMap("999" );
		svrSendMsg.put("Socket_Number", port   );
		svrSendMsg.put("ClientMessage", "close");
		SvrSocket.send(10001, JsonUtils.mapToJson(svrSendMsg).toString());

		Socket socket;
		try {
			if ("ALL".equals(port)) {
				//종료전 소켓 닫기(초기화)
				Object x[] = DataManager.getInstance().getSocketMap().keySet().toArray();
				for (int i = 0; i < DataManager.getInstance().getSocketMap().size(); i++) {
					socket = (Socket) DataManager.getInstance().getSocketMapData((Integer) x[i]);
					socket.close();
				}
				DataManager.getInstance().clearSocketMap();
			} else {
				socket = (Socket) DataManager.getInstance().socketMap.get(port);
				socket.close();
				DataManager.getInstance().setSocketMapData(port, "");
			}
	    } catch (IOException e) {
			e.printStackTrace();
		}*/
	}
}
