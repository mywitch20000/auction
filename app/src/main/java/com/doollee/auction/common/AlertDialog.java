package com.doollee.auction.common;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.text.Html;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.doollee.auction.R;
import com.doollee.auction.ui.BaseDialog;

/**
 *
 * Class: AlertDialog
 * Created by wizard on 2020.09.21
 *
 * Description: 얼럿 팝업
 */

public class AlertDialog extends BaseDialog implements View.OnClickListener {
    private Context mContext;

    public String msg;

    public String mLinkText = "";
    public String mNBtText = "";
    public String mPBtText = "";
    public String mSubText = "";

    public View.OnClickListener mLinkListener;
    public View.OnClickListener mNListener;
    public View.OnClickListener mPListener;
    private OnDismissListener   onDismissListener;

    public AlertDialog(Context context) {
        super(context);
        mContext = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.dialog_alert);

        setDialogWidth();

        initView();
    }

    /**
     * 화면 초기화
     *
     * @retuen
     */
    private void initView() {
        TextView textMsg = (TextView) findViewById(R.id.text_msg);
        TextView textSubmag = (TextView) findViewById(R.id.text_submsg);
        setCancelable(false);

        if (msg == null || "".equals(msg)) {
            msg = mContext.getString(R.string.alram);
        }

        if (msg.contains("font color"))
            textMsg.setText(Html.fromHtml(msg));
        else
            textMsg.setText(msg);

        if (!TextUtils.isEmpty(mSubText)) {
            textSubmag.setText(mSubText);
            textSubmag.setVisibility(View.VISIBLE);
        }

        Button mBtNegative = (Button) findViewById(R.id.btn_negative);
        Button mBtPositive = (Button) findViewById(R.id.btn_positive);
        mBtNegative.setOnClickListener(this);
        mBtPositive.setOnClickListener(this);

        mBtNegative.setTextColor(mContext.getResources().getColor(R.color.colorFFFFFF));
        mBtPositive.setTextColor(mContext.getResources().getColor(R.color.colorFFFFFF));

        if (mNBtText != null && !"".equals(mNBtText)) {
            mBtNegative.setText(mNBtText);
        }

        if (mPBtText != null && !"".equals(mPBtText)) {
            mBtPositive.setText(mPBtText);
        }

        if (mNListener == null) {
            mBtNegative.setVisibility(View.GONE);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_btnok);
        } else {
            mBtNegative.setBackgroundResource(R.drawable.selector_radius_left_btncancel);
            mBtPositive.setBackgroundResource(R.drawable.selector_radius_right_btnok);
        }

        if (mLinkListener != null) {
            LinearLayout llLink = (LinearLayout) findViewById(R.id.ll_alertlink);
            llLink.setVisibility(View.VISIBLE);
            TextView tvAlertLink = (TextView) findViewById(R.id.tv_linkmsg);
            tvAlertLink.setText(mLinkText);
            llLink.setOnClickListener(this);
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.btn_negative:
                if (mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    if (onDismissListener != null)
                        onDismissListener.onDismiss();
                    dismiss();
                }

                if (mNListener != null) {
                    mNListener.onClick(view);
                }
                break;

            case R.id.btn_positive:
                if (mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    if (onDismissListener != null)
                        onDismissListener.onDismiss();
                    dismiss();
                }

                if (mPListener != null) {
                    mPListener.onClick(view);
                }
                break;

            case R.id.ll_alertlink:
                if (mContext instanceof Activity && !((Activity)mContext).isFinishing()) {
                    if (onDismissListener != null)
                        onDismissListener.onDismiss();
                    dismiss();
                }

                if (mLinkListener != null) {
                    mLinkListener.onClick(view);
                }
                break;

            default:
                break;
        }
    }

    @Override
    public void dismiss() {
        super.dismiss();
        if (onDismissListener != null)
            onDismissListener.onDismiss();
    }

    public void setOnDismissListener(OnDismissListener listener) {
        onDismissListener = listener;
    }

    public interface OnDismissListener {
        void onDismiss();
    }
}
